#!/usr/bin/env bash

set -e # Exit script immediately on first error.
set -x # Print commands and their arguments as they are executed.

apt-get -y update
apt-get -y install ant
apt-get -y install openjdk-6-jdk
apt-get -y install build-essential
apt-get -y install expect

mkdir /opt/android_tools
cd /opt/android_tools

wget http://dl.google.com/android/android-sdk_r23.0.2-linux.tgz >/dev/null 2>&1
wget http://dl.google.com/android/ndk/android-ndk-r10c-linux-x86.bin >/dev/null 2>&1

tar xvzf android-sdk_r23.0.2-linux.tgz
chmod a+x android-ndk-r10c-linux-x86.bin
./android-ndk-r10c-linux-x86.bin

rm -f android-sdk_r23.0.2-linux.tgz
rm -f android-ndk-r10c-linux-x86.bin

chmod -R 777 /opt/android_tools

cd /opt/android_tools/android-sdk-linux/tools
#./android update sdk --no-ui -t 1,2,5,7,17,35,38,43,53

# Accept all license requests for the SDK installs
expect -c '
set timeout -1   ;
spawn ./android update sdk --no-ui -a --filter tools,platform-tools,build-tools-21.1.2,android-21,android-19,android-8,addon-google_apis-google-21,addon-google_apis_x86-google-19,addon-google_apis-google-8;
expect { 
    "Do you accept the license" { exp_send "y\r" ; exp_continue }
    eof
}
'

# Setup environment variables
echo "export ANDROID_SDK_ROOT_LOCAL=/opt/android_tools/android-sdk-linux" >> /home/vagrant/.profile
echo "export NDK_ROOT=/opt/android_tools/android-ndk-r10c" >> /home/vagrant/.profile

# Add android to path
echo "export PATH=${PATH}:/opt/android_tools/android-sdk-linux/tools:/opt/android_tools/android-sdk-linux/platform-tools" >> /home/vagrant/.profile
