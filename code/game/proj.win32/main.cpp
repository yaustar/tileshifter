#include "main.h"
#include "AppDelegate.h"
#include "CCEGLView.h"

//Include Visual Leak Detector ---------------------------------------
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)	//Win32 Only
	#if _DEBUG									//Debug Mode Only - Windows Specific Debug Macro
		#include "vld.h"
	#endif //_DEBUG
#endif //CC_PLATFORM_WIN32
//---------------------------------------------------------------------

USING_NS_CC;

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // create the application instance
    AppDelegate app;
    CCEGLView* eglView = CCEGLView::sharedOpenGLView();
    eglView->setViewName("tileshifter");
    eglView->setFrameSize(360, 640);
    return CCApplication::sharedApplication()->run();
}
