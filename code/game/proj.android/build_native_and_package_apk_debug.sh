#!/usr/bin/env bash
set -e # Exit script immediately on first error.
set -x # Print commands and their arguments as they are executed.
bash build_native.sh
android update project -p . -t "Google Inc.:Google APIs:8"
android update project -p ../../cocos2d-x-2.2/cocos2dx/platform/android/java/ -t "Google Inc.:Google APIs:8"
ant debug
