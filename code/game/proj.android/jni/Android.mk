LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AllGameTile.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/BaseGameTile.cpp \
                   ../../Classes/CharacterObject.cpp \
                   ../../Classes/CoinObject.cpp \
                   ../../Classes/CollisionManager.cpp \
                   ../../Classes/EventListener.cpp \
                   ../../Classes/EventManager.cpp \
                   ../../Classes/EventMessage.cpp \
				   ../../Classes/FastLabelTTF.cpp \
				   ../../Classes/FastLabelTTFCache.cpp \
				   ../../Classes/FastLabelTTFFactory.cpp \
				   ../../Classes/FastLabelTTFManager.cpp \
				   ../../Classes/GameActivationLayer.cpp \
                   ../../Classes/GameCoreData.cpp \
                   ../../Classes/GameOverLayer.cpp \
                   ../../Classes/GameScene.cpp \
                   ../../Classes/GameTileFactory.cpp \
                   ../../Classes/GameTileManager.cpp \
                   ../../Classes/GameTimer.cpp \
				   ../../Classes/HasteFileText.cpp \
				   ../../Classes/HasteFileXML.cpp \
				   ../../Classes/HighscoreManager.cpp \
                   ../../Classes/HighscoreScene.cpp \
                   ../../Classes/MainMenuScene.cpp \
				   ../../Classes/MenuButtonOptions.cpp \
				   ../../Classes/MenuButtonStandard.cpp \
				   ../../Classes/OptionsScene.cpp \
				   ../../Classes/OptionSelectorBar.cpp \
                   ../../Classes/RandomNumberGenerator.cpp \
                   ../../Classes/SceneFactory.cpp \
                   ../../Classes/SceneManager.cpp \
				   ../../Classes/SoundManager.cpp \
                   ../../Classes/StraightGameTile.cpp \
				   ../../Classes/StringExt.cpp \
                   ../../Classes/TitleScene.cpp \
                   ../../Classes/TurnGameTile.cpp \


LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

LOCAL_CFLAGS := -std=gnu++11

include $(BUILD_SHARED_LIBRARY)

$(call import-module,cocos2dx)
$(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
$(call import-module,extensions)
$(call import-module,external/Box2D)
$(call import-module,external/chipmunk)
