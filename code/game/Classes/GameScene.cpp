#include "GameScene.h"

#include "SceneManager.h"
#include "GameObjectTags.h"

#include "GameTileManager.h"
#include "CharacterObject.h"
#include "CoinObject.h"
#include "GameTimer.h"

//Game Over Layer ---
#include "GameOverLayer.h"

#include "CollisionManager.h"
#include "GameCoreData.h"

//Game Activiation Layer (3-2-1-Go)
#include "GameActivationLayer.h"

//Event Includes -----
#include "EventListener.h"
#include "EventGameTimerExpired.h"
#include "EventPauseGame.h"

const int GAME_ACTIVATION_SEQ_TAG = 138420;

USING_NS_CC;

CCScene* GameScene::createScene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    GameScene *layer = GameScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }


	//Set whether to pause the game or not from the start (if 3-2-1-Go sequence is used, do not set to false!)
	EventPauseGame::create( true );


    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
										"CloseNormal.png",
                                        "CloseSelected.png",
                                        this,
                                        menu_selector(GameScene::menuCloseCallback));
    
	pCloseItem->setPosition(ccp(origin.x + visibleSize.width - pCloseItem->getContentSize().width/2 ,
                                origin.y + pCloseItem->getContentSize().height/2));


    // create menu, it's an autorelease object
    CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
    pMenu->setPosition(CCPointZero);
    this->addChild(pMenu, 1);



	//Add Bg Colour
	CCLayerColor* pBg = CCLayerColor::create( cocos2d::ccc4(200, 200, 200, 255) );
	this->addChild( pBg );


	//Core System Objects ---------------------------------
	//Core Game Setup -------------------------------------
	GameCoreData::resetScore();

	//Core Game Objects -----------------------------------
	GameTileManager* pGameTileManager = GameTileManager::create();
	pGameTileManager->setTag( GAME_OBJ_TAG_TILE_MANAGER );
	this->addChild( pGameTileManager );

	CharacterObject* pCharacter = CharacterObject::create( pGameTileManager->selectCharacterTile(), false );
	pCharacter->setTag( GAME_OBJ_TAG_CHARACTER );
	this->addChild( pCharacter );

	CharacterObject* pGhost = CharacterObject::create( pGameTileManager->selectGhostTile(), true );
	pGhost->setTag( GAME_OBJ_TAG_GHOST );
	this->addChild( pGhost );

	CoinObject* pCoinObject = CoinObject::create( pCharacter->getActiveCharacterTile() );
	pCoinObject->setTag( GAME_OBJ_TAG_COIN );
	this->addChild( pCoinObject );

	CollisionManager* pCollisionManager = CollisionManager::create( pCharacter, pGhost, pCoinObject );
	this->addChild( pCollisionManager );

	GameTimer* pGameTimer = GameTimer::create();
	this->addChild( pGameTimer );

	//Perform 3-2-1-Go Sequence (expects game to be paused)
	CCCallFunc* pActivationSeqCallback = CCCallFunc::create(this, callfunc_selector(GameScene::callBackActivationSequenceComplete) );
	GameActivationLayer* pActivationSeq = GameActivationLayer::create( pActivationSeqCallback );
	pActivationSeq->setTag( GAME_ACTIVATION_SEQ_TAG );
	this->addChild( pActivationSeq );

	//Game Scene Event Listeners --------------------------
	EventListener* pEventListener = EventListener::create(this, callfuncO_selector(GameScene::callBackGameTimerExpired), TS_EVENT_ID_GAME_TIMER_EXPIRED);
	this->addChild( pEventListener );

	pEventListener = EventListener::create(this, callfuncO_selector(GameScene::callBackGameOver), TS_EVENT_ID_GAME_OVER);
	this->addChild( pEventListener );

    return true;
}


void GameScene::menuCloseCallback(CCObject* pSender)
{
	transitionToScores();
}

void GameScene::menuPauseCallback(CCObject* pSender)
{
	//Reverse the current pause status!
	EventPauseGame::create( !GameCoreData::isGamePaused() );
}

void GameScene::callBackGameOver(CCObject* pSender)
{
	 transitionToScores();
}

 void GameScene::callBackGameTimerExpired(CCObject* pEventObject)
 {
	 transitionToScores();
 }

 void GameScene::callBackActivationSequenceComplete()
 {
	//Remove activation sequence instance 
	removeChildByTag( GAME_ACTIVATION_SEQ_TAG );

	//Collect info.
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	//Add a "pause" icon to pause the progress. it's an autorelease object
    CCMenuItemImage *pPauseItem = CCMenuItemImage::create(
										GameCoreData::getResourcesName(RESOURCE_NAME_MENU_OPTIONS_STAND).c_str(),
                                        GameCoreData::getResourcesName(RESOURCE_NAME_MENU_OPTIONS_SELECT).c_str(),
                                        this,
										menu_selector(GameScene::menuPauseCallback));

	pPauseItem->setPosition( ccp(visibleSize.width*0.5f,
                                origin.y + pPauseItem->getContentSize().height*0.5f) );


	// create menu, it's an autorelease object 
    CCMenu* pMenu = CCMenu::create(pPauseItem, NULL);
    pMenu->setPosition(CCPointZero);
	pMenu->setOpacity( 0.0f );
    this->addChild(pMenu, 1);

	pMenu->runAction( CCFadeIn::create(1.0f) );
 }


 void GameScene::transitionToScores()
 {
	 //Produce a render texture of the current level
	 CCRenderTexture* pRenderTexture = CCRenderTexture::create( CCDirector::sharedDirector()->getVisibleSize().width, CCDirector::sharedDirector()->getVisibleSize().height );
	 
	 pRenderTexture->begin();
	 this->visit();
	 pRenderTexture->end();

	 //Remove all objects from the game scene
	 removeAllChildrenWithCleanup( true );

	 //Generate a sprite from the render texture
	 CCSprite* pGameSprite = CCSprite::createWithTexture( pRenderTexture->getSprite()->getTexture() );
	 pGameSprite->setScaleY(-1); //Sprite will be upside down without this being set.

	 //Create game over layer and pass it the render texture sprite
	 GameOverLayer* pGameOverLayer = GameOverLayer::create();
	 pGameOverLayer->activateLayer( pGameSprite );

	 //Add Game Over Layer to the game scene.
	 addChild( pGameOverLayer );
 }