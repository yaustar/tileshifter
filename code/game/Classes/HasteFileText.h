#ifndef HASTE_FILE_TEXT
#define HASTE_FILE_TEXT

#include "IHasteFile.h"
#include <string>

class HasteFileText : public IHasteFile
{
public:

	HasteFileText();
	virtual ~HasteFileText();

	virtual bool VFileExist(const char* fileName, bool bLoadFromWriteMemory = false);

	virtual void VLoadFile(const char* fileName, bool bLoadFromWriteMemory = false);
	virtual void VSaveFile(const char* fileName);

	void AssignBuffer(const std::string &assignBuffer);

protected:

	//Methods ----
	bool LoadFileFromReadOnly(const char* fileName);
	bool LoadFileFromWrite(const char* fileName);
	void VRemoveFileBuffer();

	//Variables ----
	std::string m_fileBuffer;
};
#endif //HASTE_FILE_TEXT