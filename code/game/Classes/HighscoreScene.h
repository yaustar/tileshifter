#ifndef HIGHSCORE_SCENE_H
#define HIGHSCORE_SCENE_H

#include "cocos2d.h"

class HighscoreScene : public cocos2d::CCLayer
{
public:

	HighscoreScene();
	~HighscoreScene();

	 // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

	virtual void onExit();

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* createScene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(HighscoreScene);

	//Wait for touch events
	//virtual void ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Starts

private:
	//Private Methods ----
	void retryCallback();
	void mainMenuCallback();
};

#endif //HIGHSCORE_SCENE_H