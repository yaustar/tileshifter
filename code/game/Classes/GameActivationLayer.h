#ifndef GAME_ACTIVATION_LAYER_H
#define GAME_ACTIVATION_LAYER_H

#include "cocos2d.h"
#include "ExtraDeclarations.h"
#include "GameCoreData.h" //Can't use forward declaration for 'CoreResourceNameType' due to android error.


//Game Activiation Layer = (3-2-1-Go Sequence!)
class GameActivationLayer : public cocos2d::CCLayer, public cocos2d::CCTextFieldDelegate
{
public:
	GameActivationLayer(cocos2d::CCCallFunc* pSeqCallBack);
	~GameActivationLayer();

	CREATE_FUNC_ONE_ARGU( GameActivationLayer, cocos2d::CCCallFunc*, pSeqCallBack );

	virtual bool init(); //Overloaded Init.
	virtual void onExit();


private:

	//Private Methods -------
	void generateSequenceSprite(const cocos2d::CCPoint &pos, CoreResourceNameType resourceType, cocos2d::CCCallFunc* pSeqCallBack, float duration, const int spriteTag);

	void threeSeqComplete();
	void twoSeqComplete();
	void oneSeqComplete();
	void goSeqComplete();

	//Private Variables -----
	cocos2d::CCCallFunc* m_pSeqCallBack;
};
#endif //GAME_ACTIVATION_LAYER_H