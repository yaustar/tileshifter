#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include "cocos2d.h"

#include "EventIdList.h"
#include "MemberLinkedList.h"

#include "EventMessageDeclarations.h"
#include "EventListenerDeclarations.h"

#include <vector>

class EventManager : public cocos2d::CCLayer
{
public:
	EventManager();
	~EventManager();

	CREATE_FUNC( EventManager );

	//Produce a singleton!
	static EventManager* sharedEventManager(bool bPerformNullTest = true);

	//Add Event Messages
	void QueueEvent(EventMemberLinkedNode* pLinkedListNode);

	//Add/Remove Event Listeners
	void registerListener(EventListenerLinkedNode* pLinkedListNode, TileshifterEventId eventId);
	void unRegisterListener(EventListenerLinkedNode* pLinkedListNode, TileshifterEventId eventId);

	//Overloaded from CCLayer, it's run at the end of each update!
	void update(float delta);
	void sceneTransitionComplete();


private:

	//Private Declarations -------

	//Declare the LISTENERS member linked list type
	typedef MemberLinkedList<EVENT_LISTENER_OBJECT_TYPE> EventListenerMemberLinkedList;

	//Declare the LISTENERS array of member linked lists
	typedef std::vector< EventListenerMemberLinkedList > EventListenerList;
	
	//Declare the MESSAGES member linked list type
	typedef MemberLinkedList<EVENT_MESSAGE_OBJECT_TYPE> EventMessageMemberLinkedList;


	//Private Variables -----
	EventListenerList m_eventListenerList;
	EventMessageMemberLinkedList m_eventQueue;


	//Static Private Variables ---- 
	static EventManager* ms_sharedInstance;
};

#endif //EVENT_MANAGER_H