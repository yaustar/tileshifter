#ifndef FAST_LABEL_TTF_FACTORY_H
#define FAST_LABEL_TTF_FACTORY_H

#include <string>
#include "FastLabelTTFTypes.h"

namespace FastLabelTTFFactory
{
	void GetFontInfo(std::string &outFontName, float &outFontSize, FastLabelTTFTypes fontType );
};

#endif //FAST_LABEL_TTF_FACTORY_H