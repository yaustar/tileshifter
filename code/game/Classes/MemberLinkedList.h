#ifndef MEMBER_LINKED_LIST_H
#define MEMBER_LINKED_LIST_H

#include "MemberLinkedListNode.h" //Has Friend Access!

//Template EXPECTS a POINTER type!!!!
template <class T>
class MemberLinkedList
{
public:
	
	//------------------------------------------------------------------
	MemberLinkedList() :
		m_fakeHead			( NULL ),
		m_fakeTail			( NULL ),
		m_listSize			( 0 ),
		m_bRetainObject		( true )
	{
		m_fakeHead.setNext( &m_fakeTail );
		m_fakeTail.setPrev( &m_fakeHead );
	}

	//Copy Constructor -------------------------------------------------
	//push_back on 'std::vector' forces a copy that we mus thandle here
	MemberLinkedList(const MemberLinkedList &other) :
		m_fakeHead			( NULL ),
		m_fakeTail			( NULL ),
		m_listSize			( other.m_listSize ),
		m_bRetainObject		( other.m_bRetainObject )
	{
		//If the member head and tail will be pointing at eachother, they need to point at our members, not the default of using the 'other' 
		if( m_listSize == 0 )
		{
			m_fakeHead.setNext( &m_fakeTail );
			m_fakeTail.setPrev( &m_fakeHead );
		}
		else
		{
			//The member head and tail point to actual pointers, update our local ones to match this
			m_fakeHead.setNext( other.m_fakeHead.m_pNextNode );
			m_fakeTail.setPrev( other.m_fakeTail.m_pPrevNode );
		}
	}

	//------------------------------------------------------------------
	~MemberLinkedList()
	{
		erase();
	}

	//------------------------------------------------------------------
	void retainNodeObject(bool bRetainObject)
	{
		m_bRetainObject = bRetainObject;
	}

	//------------------------------------------------------------------
	void insert(MemberLinkedListNode<T>* pPosNode, MemberLinkedListNode<T>* pInsertNode)
	{
		//Insert into the position slot (basically in front of it!)
		pPosNode->insertPrev( pInsertNode );

		UpdatePushCall( pInsertNode );
	}

	//------------------------------------------------------------------
	void push_front(MemberLinkedListNode<T>* pFrontNode)
	{
		insert( m_fakeHead.getNext(), pFrontNode );
	}

	//------------------------------------------------------------------
	void push_back(MemberLinkedListNode<T>* pBackNode)
	{
		insert( &m_fakeTail, pBackNode );
	}

	//------------------------------------------------------------------
	void erase()
	{
		while( m_listSize != 0 )
		{
			pop_back();
		}
	}

	//------------------------------------------------------------------
	void remove(MemberLinkedListNode<T>* pRemoveNode)
	{
		if( m_listSize == 0 )
		{
			return;
		}

		pRemoveNode->removeNode();

		UpdatePopCall( pRemoveNode );
	}

	//------------------------------------------------------------------
	void pop_front()
	{
		remove( m_fakeHead.getNext() );		
	}

	//------------------------------------------------------------------
	void pop_back()
	{
		remove( m_fakeTail.getPrev() );	
	}

	//------------------------------------------------------------------
	MemberLinkedListNode<T>* front()
	{
		if( m_listSize != 0 )
		{
			return m_fakeHead.getNext();
		}

		return NULL;
	}

	//------------------------------------------------------------------
	MemberLinkedListNode<T>* back()
	{
		if( m_listSize != 0 )
		{
			return m_fakeTail.getPrev();
		}

		return NULL;
	}

	//------------------------------------------------------------------
	MemberLinkedListNode<T>* accessNext(MemberLinkedListNode<T>* pCurrNode)
	{
		MemberLinkedListNode<T>* pNextNode = pCurrNode->getNext();

		if( pNextNode != &m_fakeTail )
		{
			return pNextNode;
		}

		return NULL;
	}

	//------------------------------------------------------------------
	MemberLinkedListNode<T>* accessPrev(MemberLinkedListNode<T>* pCurrNode)
	{
		MemberLinkedListNode<T>* pPrevNode = pCurrNode->getPrev();

		if( pPrevNode != &m_fakeHead )
		{
			return pPrevNode;
		}

		return NULL;
	}

	//------------------------------------------------------------------
	unsigned int getSize()
	{
		return m_listSize;
	}

private:

	//------------------------------------------------------------------
	//Private Methods 
	//------------------------------------------------------------------
	void UpdatePushCall(MemberLinkedListNode<T>* pPushNode)
	{
		m_listSize++;

		if( m_bRetainObject )
		{
			//We need to retain the actual object here (doing so inside the node will cause it to delete the object that's created it, VERY BAD!)
			pPushNode->GetLinkedObject()->retain();
		}
	}

	//------------------------------------------------------------------
	void UpdatePopCall(MemberLinkedListNode<T>* pPopNode)
	{
		m_listSize--;

		if( m_bRetainObject )
		{
			//Release the object as it's no longer contained within our list!
			pPopNode->GetLinkedObject()->release();
		}
	}

	//------------------------------------------------------------------
	//Private Variables 
	//------------------------------------------------------------------
	MemberLinkedListNode<T> m_fakeHead;
	MemberLinkedListNode<T> m_fakeTail;

	unsigned int m_listSize;
	bool m_bRetainObject;
};

#endif //MEMBER_LINKED_LIST_H