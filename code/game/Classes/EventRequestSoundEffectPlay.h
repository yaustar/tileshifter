#ifndef EVENT_REQUEST_SOUND_EFFECT_PLAY_H
#define EVENT_REQUEST_SOUND_EFFECT_PLAY_H

#include "EventMessage.h"

#include "SoundTypes.h"
#include "ExtraDeclarations.h"

class EventRequestSoundEffectPlay : public EventMessage
{
public:
	EventRequestSoundEffectPlay(SoundType soundEffect) :
		EventMessage	( TS_EVENT_ID_SOUND_EFFECT_PLAY ),
		m_soundEffect	( soundEffect )
	{

	}

	CREATE_FUNC_ONE_ARGU( EventRequestSoundEffectPlay, SoundType, soundEffect );

	SoundType GetSoundEffect()
	{
		return m_soundEffect;
	}

private:

	SoundType m_soundEffect;
};
#endif //EVENT_REQUEST_SOUND_EFFECT_PLAY_H