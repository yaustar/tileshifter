#ifndef HASTE_FILE_XML
#define HASTE_FILE_XML

#include "HasteFileText.h"

//Declarations --------
enum XMLDataType
{
	XML_NONE = 0,
	XML_STRING,
	XML_INT,
	XML_FLOAT
};

union XMLData
{
	std::string*	pS; //Pointer to a string
	int				i; //Signed int
	unsigned int	uI; //Unsigned int
	float			f; //floating point
};

//Core class --------
class HasteFileXML : public HasteFileText
{
public:
	HasteFileXML();
	~HasteFileXML();

	void VLoadFile(const char* fileName, bool bLoadFromWriteMemory = false);

	//XML Controller Methods ---------
	bool NodeContainsData();
	const XMLData& GetNodeData();
	XMLDataType GetNodeDataType();

	void ResetNodeAccess();
	void MoveToParent();

	bool FindAndAccessNode(const char* nodeName, bool bApplyToAccessor = true);
	bool FindAndAccessNodeSibling(const char* nodeName);

private:

	static const unsigned int NODE_NAME_SIZE = 32;

	struct NodeXML
	{
		NodeXML() :
			pParent		( NULL ),
			pSibling	( NULL ),
			pChild		( NULL ),

			dataType	( XML_NONE )
		{
		}

		NodeXML* pParent;
		NodeXML* pSibling;
		NodeXML* pChild;
		
		char nodeName[NODE_NAME_SIZE];
		XMLData data;
		XMLDataType dataType;
	};

	//Private Methods ----
	size_t PassXmlHeaderInfo();
	bool SkipToXmlInfo(size_t &inOutCurrPos);

	bool FindNextNode(size_t &inOutCurrPos);
	void CollectNodeData(size_t &inOutCurrPos);

	void RemoveWhiteSpace(size_t &inOutCurrPos);
	bool DetectNodeEnd(size_t &inOutCurrPos);

	NodeXML* IterateToEndSibling(NodeXML* startNode);

	//Private Variables ----
	NodeXML* m_pHeadNode;
	NodeXML* m_pActiveNode; //The 'selected' node
};

#endif //HASTE_FILE_XML