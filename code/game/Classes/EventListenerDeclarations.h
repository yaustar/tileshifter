#ifndef EVENT_LISTENER_DECLARATIONS_H
#define EVENT_LISTENER_DECLARATIONS_H

#include "MemberLinkedListNode.h"

//Forward Include -----
class EventListener;

//Variable Macro -----
#define EVENT_LISTENER_OBJECT_TYPE EventListener*

//Member Linked Node TypeDef ----
typedef MemberLinkedListNode<EVENT_LISTENER_OBJECT_TYPE> EventListenerLinkedNode;



#endif //EVENT_LISTENER_DECLARATIONS_H