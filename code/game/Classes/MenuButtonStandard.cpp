#include "MenuButtonStandard.h"

#include "SceneManager.h"
#include "GameCoreData.h"

#include "FastLabelTTFTypes.h"
#include "FastLabelTTF.h"

const int MENU_BUTTON_TAG = 24345; //Random value
const int MENU_TAG = MENU_BUTTON_TAG+1;
const int MENU_TEXT_TAG = MENU_TAG+1;

USING_NS_CC;

//---------------------------------------
MenuButtonStandard::MenuButtonStandard(cocos2d::CCCallFunc* pPressCallback) :
	m_pPressCallback	( pPressCallback )
{
	m_pPressCallback->retain();
}
//---------------------------------------
bool
MenuButtonStandard::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
	//Generate the main menu button
	CCMenuItemImage* pMenuButton = CCMenuItemImage::create(		getStandardSpriteName().c_str(),
																getSelectedSpriteName().c_str(),
																this,
																menu_selector(MenuButtonStandard::menuButtonCallback)
															);

	//Setup menu button
	pMenuButton->setPosition( CCPointZero );
	pMenuButton->setTag( MENU_BUTTON_TAG );
	
	//Create menu, ataching the menu button to it
    CCMenu* pMenu = CCMenu::create(pMenuButton, NULL);
    pMenu->setPosition(CCPointZero);
	pMenu->setTag( MENU_TAG );
    this->addChild(pMenu, 1);
	
    return true;
}
//---------------------------------------
void
MenuButtonStandard::onExit()
{
	m_pPressCallback->release();
}
//---------------------------------------
void
MenuButtonStandard::setString(const char* pName)
{
	FastLabelTTF* pMenuText = static_cast<FastLabelTTF*>( getChildByTag(MENU_TEXT_TAG) );

	if( pMenuText )
	{
		pMenuText->setString( pName );
	}
	else
	{
		FastLabelTTF* pMenuText = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_45, CCSize(1.0f, 1.0f), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter );
		pMenuText->setString( pName );
		pMenuText->setPosition( getChildByTag(MENU_TAG)->getChildByTag(MENU_BUTTON_TAG)->getPosition() );
		pMenuText->setTag( MENU_TEXT_TAG );
		this->addChild( pMenuText, 2 );
	}
}
//---------------------------------------
CCSize
MenuButtonStandard::getSize()
{
	return getChildByTag(MENU_TAG)->getChildByTag(MENU_BUTTON_TAG)->getContentSize();
}
//---------------------------------------
std::string
MenuButtonStandard::getStandardSpriteName()
{
	return GameCoreData::getResourcesName(RESOURCE_NAME_MENU_BUTTON_STAND);
}
//---------------------------------------
std::string
MenuButtonStandard::getSelectedSpriteName()
{
	return GameCoreData::getResourcesName(RESOURCE_NAME_MENU_BUTTON_SELECT);
}
//---------------------------------------
void
MenuButtonStandard::menuButtonCallback(CCObject* pSender)
{
	m_pPressCallback->execute();
}
//---------------------------------------
//---------------------------------------