#ifndef EVENT_MESSAGE_DECLARATIONS_H
#define EVENT_MESSAGE_DECLARATIONS_H

#include "MemberLinkedListNode.h"

//Forward Include -----
class EventMessage;

//Variable Macro -----
#define EVENT_MESSAGE_OBJECT_TYPE EventMessage*

//Member Linked Node TypeDef ----
typedef MemberLinkedListNode<EVENT_MESSAGE_OBJECT_TYPE> EventMemberLinkedNode;



#endif //EVENT_MESSAGE_DECLARATIONS_H