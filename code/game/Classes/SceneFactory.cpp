#include "SceneFactory.h"

#include "TitleScene.h"
#include "MainMenuScene.h"
#include "GameScene.h"
#include "OptionsScene.h"
#include "HighscoreScene.h"

USING_NS_CC;

//----------------------------------------------------
void sceneFactoryAssertion()
{
	CCAssert(false, "Scene Factory has failed!");
}
//-------------------------------------------
CCScene*
SceneFactory::getNewScene(SceneType sceneType)
{
	CCScene* pScene = NULL;

	switch (sceneType)
	{
	case SCENE_TITLE:
		pScene = TitleScene::createScene();
		break;

	case SCENE_MAIN_MENU:
		pScene = MainMenuScene::createScene();
		break;

	case SCENE_GAME:
		pScene = GameScene::createScene();
		break;

	case SCENE_OPTIONS:
		pScene = OptionsScene::createScene();
		break;

	case SCENE_HIGHSCORES:
		pScene = HighscoreScene::createScene();
		break;

	default:
		sceneFactoryAssertion();
		break;
	}

	return pScene;
}
//-------------------------------------------
//-------------------------------------------
//-------------------------------------------