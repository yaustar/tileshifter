#ifndef BASE_GAME_TILE_H
#define BASE_GAME_TILE_H

#include "cocos2d.h"
#include "TileType.h"

//Define Unique 'Create Function' Macro

#define CREATE_FUNC_TILE(__TYPE__) \
static __TYPE__* create(TileType tileID, const cocos2d::CCPoint &pos) \
{ \
    __TYPE__ *pRet = new __TYPE__(tileID, pos); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = NULL; \
        return NULL; \
    } \
}


class BaseGameTile : public cocos2d::CCLayer
{
public:

	struct TileNeighbourStruct
	{
		TileNeighbourStruct() 
		{
			for(int i = 0; i < TILE_NUM_DIRECTIONS; i++)
			{
				pTileList[i] = NULL;
			}
		}

		BaseGameTile* pTileList[TILE_NUM_DIRECTIONS];
	};

	//Construct -----
	BaseGameTile(TileType tileID, const cocos2d::CCPoint &pos);

	//Destruct ----- Bug Fix, on scene transition the static variables need cleaning up (as they will soon point to dead memory!)
	virtual ~BaseGameTile();

	//'create' method (for CCLayer) SHOULD be provided by ALL derived forms (abstract classes can't init themselves).
	virtual bool init(); //Overloaded Init. Requires Super INIT


	//Override Touch Methods ----------------
	virtual void ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Starts
    virtual void ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Moves
    virtual void ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Ends


	//Public Virtual Methods -------------------
	//Returns tiles (selected) edge position - pure virtual, CCPoint = new instance, otherwise it will return a local variable
	virtual const cocos2d::CCPoint getEdgeOffset(TileDirection &inOutCurrDir) const=0;

	//Returns cenrte position of neighbour node (if we can travel along it) (not pure virtual, but derived forms may still provide there own!)
	virtual bool getNextCentrePosition(TileDirection currDir, const BaseGameTile* &pOutActiveTile, cocos2d::CCPoint& outValidCentrePos, cocos2d::CCPoint& outInstantJumpOffset) const; 


	//Public Methods -------------------------

	//Assigns the current tiles neighbours to the values found within the neighbour list
	void initTileNeighbours(const TileNeighbourStruct& neighbourList); //Two private ones also exist!
	
	//Return a random direction that is valid for the current tile
	TileDirection getRandomTileDirection() const;

	//Pause Game Listener
	void pauseGameCallback(CCObject* pObject);

	//Public Accessors ---------------------
	TileType getTileTypeID();
	const cocos2d::CCPoint& getPosition() const;

	typedef std::vector<TileDirection> TileDirectionList;
	const TileDirectionList& getValidTileDirectionList() const;

	//Get selected tiles neighbours to the given direction (required when accessing other tiles, that aren't the tiles own)
	const BaseGameTile* getConstantNeighbourTile(TileDirection tileDirection) const;

	//Get the tile from the current tile that is the furthest away in the passed direction - CONSTANT VERSION - DUP_LICATED CODE
	const BaseGameTile* getConstantCornerTile(TileDirection tileDirection) const;

	//Get a tiles 'half' offset of the current direction (based on its size)
	float getTilesHalfOffset(TileDirection currDir) const;

	//Get the tiles Opacity
	float getTilesOpacity() const;

	//Get Is Tile Part Of Moving Chain (or the mover itself aswell!)
	bool isTileMoving() const;

	const cocos2d::CCPoint& getConstPos() const;

	//Get End Slide Position - Returns the position that the tile will end at once the sliding tile animation completes
	const cocos2d::CCPoint& getEndSlidePosition() const;

	const float getTileSlideSpeed() const;
protected:

	//Protected Methods -----------------

	//Generic call to access a 'chain' of tiles (along the horizontal or vertical, depends which we are locked to) and move them all by velocity
	void moveTileChain(const cocos2d::CCPoint &velocity);

	//Completes above 'move tile chain' by accessing the tiles of a specific direction 
	void moveTileChain(TileDirection tileDirection, const cocos2d::CCPoint &velocity);

	//Creates the tile sprite (called within Load Tile)
	void createTileSprite(TileType tileID, cocos2d::CCSprite* &pOutSprite);
	
	//Set tiles opacity
	void setTileOpacity(float opacity);

	//Get selected tiles neighbours to the given direction (required when accessing other tiles, that aren't the tiles own)
	BaseGameTile* getNeighbourTile(TileDirection tileDirection) const;


	//Protected Variables (for derived forms only) ---------
	cocos2d::CCSprite*	m_pSprite; 
	TileType			m_tileID;

	TileDirectionList m_validTileDirList;


	//Protected Static Variables (for derived forms only) -------
	static const cocos2d::CCPoint ms_vectorDir[TILE_NUM_DIRECTIONS];
	static const TileDirection ms_invertTileDirection[TILE_NUM_DIRECTIONS];

private:

	//Private Structs -------------------
	struct AxisAppliers
	{
		cocos2d::CCPoint axisApplierInverted;

		float	spritePos;
		float	gridBoundMin;
		float	gridBoundMax;
		float	tileSize;
		float	tileSizeHalf; 
	};


	//Private Methods -------------------

	//Swap a tiles neighbors touch events to true or false
	void swapNeighbourTouchEvents(const BaseGameTile* pTileCaller, bool bSwapEventsOn, bool bFireInAllDiections, bool bIsStartTile = false);
	void swapNeighbourTouchEventsChainCaller(TileDirection tileDir, const BaseGameTile* pTileCaller, bool bSwapEventsOn, bool bFireInAllDiections);

	//Invert Y axis (touch is different coords on 'Y' axis!)
	cocos2d::CCPoint invertYAxis(const cocos2d::CCPoint &convertPos);

	//When ready, produce the established direction for the tiles (allowing tile movement code to run) 
	bool touchesMovedDeclareDirection(cocos2d::CCTouch* pCurrentTouch);

	//Test for 'safe zone' overlap (to determine a direction change)
	bool touchesMovedComputeDirection(const cocos2d::CCPoint& touchLen);

	//move current tile by passed velocity (handles excess sprite!)
	void moveTileByVelocity(const cocos2d::CCPoint& velocity);
	
	//Called within 'move tile by velocity' to handle the excess tile transitions (transparency, creation etc.)
	void wrapTileTransition(float boundAxis, float spriteAxis, const cocos2d::CCPoint& axisApplier);

	//Check for an existing excess sprite, create/remove it as requested
	void createExcessSprite();
	void removeExcessSprite();

	//Return the tiles sprite
	cocos2d::CCSprite* getSprite();

	void setSprite(cocos2d::CCSprite* pSprite);

	//Get the tile from the current tile that is the furthest away in the passed direction
	BaseGameTile* getCornerTile(TileDirection tileDirection);

	//Get the tile from the current tile that is the furthest away in the passed direction (requires additional info. 
	// due to lacking 'null' zones marking start and end (what we are likely adding right now
	BaseGameTile* getCornerTile(TileMoveTypeDirection tileMoveTypeDir, int currPoint);

	//Get selected tiles neighbours to the given direction by a specifc offset amount
	BaseGameTile* getNeighbourTile(cocos2d::CCPoint neighbourOffset);

	//Assigns the current tiles neighbours to the values found within the neighbour list (specifically restrcited to a direction type or direction)
	void initTileNeighbours(const TileNeighbourStruct& neighbourList, TileMoveType applyMovementDirection); //Public one exists! ^^ 
	void initTileNeighbours(BaseGameTile* pTile, TileDirection applyMovementDirection);

	//Initialise the axis applier for the 'systems' 'end move' function call. (Produces the data for the calling Base Tile)
	bool produceThisTilesAxisApplier(AxisAppliers& outAxisApplier); //Returns FALSE if one couldn't be applied!

	//Calculate the tiles new position point
	void calculateTilesPositionPoint(const AxisAppliers& axisApplier);

	//Apply 'locked/aligned' positions to the tile sprite
	void applyEndTouchTilePositions(const AxisAppliers& axisApplier);

	//Swap the excess sprite with the passed objects sprite
	bool swapSpriteWithExcessSprite(BaseGameTile* pTile);

	//Reset tile flags (called in 'End Touch)
	void endTouchResetTileFlags();

	void endTouchApplyTilePositionsChains();

	//Slides the tile (over time) into a final position (calls the 'end touch reset tile flags function once action complete).
	void endTochSlideTileToPos(const cocos2d::CCPoint& position);

	//Slide tile CHAIN (linked tiles)
	void endTouchSlideTileChainPos(TileDirection tileDirection, const cocos2d::CCPoint& velocity);

	//The actual sliding code
	void slideTile( cocos2d::CCSprite* moveSprite, const cocos2d::CCPoint& position, bool resetFlagOnCompletion = false );

	//Relinks chains of tiles and caches the old points/positions
	typedef std::vector<TileNeighbourStruct> TileNeighbourList;
	void endTouchChainAndCacheExistingTiles(TileNeighbourList &outTilesByPoint);

	//Unlinks chains (denoting the start and end tiles/nodes), and updates the remaining neighbours to the correct ones
	void endTouchUnchainAndUpdateTileNeighbours(const TileNeighbourList& neighboursByPoint, int tilePosPoint);

	//initialise the protected variable
	void initTileDirectionList();

	//Set Is Tile Moving! (or the active mover itself?)
	void setTileChainIsMoving(bool setTo);
	void setTileChainIsMoving(TileDirection tileDirection, bool setTo);


	//Private Static Variables ------------
	static TileMoveType			ms_tileMove; //Stores active movement direction
	static cocos2d::CCPoint		ms_axisApplier;
	static float				ms_axisGridNodeSize;

	static cocos2d::CCSprite*	ms_pExcessTileSprite;
	static BaseGameTile*		ms_pExcessTileLocation;

	static TileDirection		ms_tileDir[TILE_NUM_TYPE_DIRECTIONS]; //Stores the Left or Bottom Tile direction based on horizontal or vertical movement being active
	static int					ms_tilePositionPoint;

	static bool					ms_bIsStaticVarDefault; //Denotes if all static variables are set to default (bug fix), issue invovled old sprite data beign reused (using dead memory)

	//Private Variables ------------
	cocos2d::CCPoint	m_endSlidePosition;
	const float			TILE_SLIDE_SPEED;

	BaseGameTile*		m_pNeighbourTiles[TILE_NUM_DIRECTIONS];
	bool				m_bIsMovedTile;
	bool				m_bIsClickedTile;


	//Make the tiles manager class a friend (granting access to private methods),
	//felt this was better than moving methods to public (allowing potential for any class to take a lot of control)
	friend class GameTileManager;
};

#endif //BASE_GAME_TILE_H