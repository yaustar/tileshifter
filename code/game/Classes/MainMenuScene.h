#ifndef MAIN_MENU_SCENE_H
#define MAIN_MENU_SCENE_H

#include "cocos2d.h"

class MainMenuScene : public cocos2d::CCLayer
{
public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

	virtual void onExit();

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* createScene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(MainMenuScene);

private:
	//Private Methods ---
	void menuPlayModeCallback();
	void menuQuitCallback();
	void menuOptionsCallback();
};

#endif //MAIN_MENU_SCENE_H