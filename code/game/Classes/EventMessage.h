#ifndef EVENT_MESSAGE_H
#define EVENT_MESSAGE_H

#include "cocos2d.h"

#include "EventIdList.h"
#include "EventMessageDeclarations.h"

class EventMessage : public cocos2d::CCLayer
{
public:
	EventMessage(TileshifterEventId eventId);
	virtual ~EventMessage();

	//Cocos CCLayer requires objects to use 'create' to auto delete memory; however, derived forms need to provide their own create method, but
	//static methods can not be made pure virtual, so this method should provide an assertion if called (i.e. if it hasn't been overridden)!
	static EventMessage* create();

	TileshifterEventId getEventId();

private:

	//Private Methods -----
	void QueueEvent();

	//Private Variables -----
	EventMemberLinkedNode m_eventListNode;
	TileshifterEventId m_eventId;
};

//Provides a macro that casts the generic CCObject into the required EventMessage
#define CREATE_EVENT_MESSAGE_INST( eventClassName, ccObjectInst) eventClassName* pEvent = static_cast<eventClassName*>( ccObjectInst );

#endif //EVENT_MESSAGE_H