#include "StraightGameTile.h"

#include "GameCoreData.h"
#include "RandomNumberGenerator.h"

USING_NS_CC;
//--------------------------------------------------------------
StraightGameTile::StraightGameTile(TileType tileID, const CCPoint& pos) :
BaseGameTile( tileID, pos )
{

}
//--------------------------------------------------------------
//Public Virtual Methods
//--------------------------------------------------------------
const CCPoint
StraightGameTile::getEdgeOffset(TileDirection &inOutCurrDir) const
{
	return ms_vectorDir[inOutCurrDir] * getTilesHalfOffset( inOutCurrDir );
}
//------------------------------------------------------------------