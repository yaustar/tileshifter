#include "StringExt.h"

//Provides 'out_of_range' exception! (Required for ANDROID build)
#include <stdexcept>

//-----------------------------------------
char
StringExt::CollectCharacter(const std::string& string, size_t pos)
{
	char rtnChar;

	try
	{
		rtnChar = string.at( pos );
	}

	catch (const std::out_of_range&) //(can create an instance within the argument list if desired to get more info!)
	{
		return -1;
	}

	return rtnChar;
}
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------