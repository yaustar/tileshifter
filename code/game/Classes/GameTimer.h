#ifndef GAME_TIMER_H
#define GAME_TIMER_H

#include "cocos2d.h"

//Class allows the game level to complete after a period of time.
//http://stackoverflow.com/questions/23623945/how-to-create-timer-in-my-games-in-cocos2dx-using-c was helpful

class FastLabelTTF;
class GameTimer : public cocos2d::CCLayer
{
public:

	GameTimer();
	~GameTimer();

	CREATE_FUNC( GameTimer );

	virtual bool init(); //Overloaded Init.
	virtual void onExit();

	void updateTimer(float elapsedTime);
	void pauseGameCallback(CCObject* pObject);

private:

	void updateOutputString();

	const float START_TIMER;
	const float UPDATE_TIMER_INTERVAL;
	float m_currTime;

	FastLabelTTF* m_pTimerOutput;

	cocos2d::CCSprite* m_pPulseSprite;

	float m_pulseVelocity;
	float m_pulsePerc;
	bool m_bPulseActive;
};
#endif //GAME_TIMER_H