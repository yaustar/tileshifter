#ifndef GAME_OVER_LAYER_H
#define GAME_OVER_LAYER_H

#include "cocos2d.h"

class GameOverLayer : public cocos2d::CCLayer, public cocos2d::CCTextFieldDelegate
{
public:
	GameOverLayer();
	~GameOverLayer();

	CREATE_FUNC( GameOverLayer );

	virtual bool init(); //Overloaded Init.
	virtual void onExit();

	virtual void ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Starts

	//Delegate (callback) from text field
	virtual bool onTextFieldInsertText(cocos2d::CCTextFieldTTF* pSender, const char* text, int nLen);

	void activateLayer(cocos2d::CCSprite* pBgSprite);
	
private:

	//Private Methods -------
	bool removeWarningMessage();

	void moveToHighscoresCallback();
	void retryCallback();
	void mainMenuCallback();

	//Private Variables -----
	cocos2d::CCSprite* m_pBgSprite;	
};
#endif //GAME_OVER_LAYER_H