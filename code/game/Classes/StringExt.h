#ifndef STRING_EXT_H
#define STRING_EXT_H

#include <sstream>

namespace StringExt
{
	char CollectCharacter(const std::string& string, size_t pos);

	template<class fromType, class toType>
	const toType& TypeConversion(const fromType& from, toType &outTo)
	{
		std::stringstream ss;
		ss << from;
		ss >> outTo;

		return outTo;
	}
};
#endif //STRING_EXT_H