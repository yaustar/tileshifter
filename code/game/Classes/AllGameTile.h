#ifndef ALL_GAME_TILE_H
#define ALL_GAME_TILE_H

#include "BaseGameTile.h"

class AllGameTile : public BaseGameTile
{
public:

	AllGameTile(TileType tileID, const cocos2d::CCPoint& pos);

	//Implement the Layers 'create' function - Calls default constructor (and the base classes init!)
	CREATE_FUNC_TILE(AllGameTile);


	//Public Virtual Methods - Derived from base! --------------------
	virtual const cocos2d::CCPoint getEdgeOffset(TileDirection &inOutCurrDir) const;



};
#endif //ALL_GAME_TILE_H