#ifndef __GameScene_SCENE_H__
#define __GameScene_SCENE_H__

#include "cocos2d.h"

class GameScene : public cocos2d::CCLayer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* createScene();
    
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
	void menuPauseCallback(CCObject* pSender);

	// event callbacks
	void callBackGameOver(CCObject* pEventObject);
	void callBackGameTimerExpired(CCObject* pEventObject);
    void callBackActivationSequenceComplete();

    // implement the "static node()" method manually
    CREATE_FUNC(GameScene);

private:
	void transitionToScores();

};
#endif // __GameScene_SCENE_H__
