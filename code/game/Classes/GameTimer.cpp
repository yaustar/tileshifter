#include "GameTimer.h"
#include "FastLabelTTF.h"
#include "GameCoreData.h"

#include <sstream>

#include "EventGameTimerExpired.h"
#include "HasteFileXML.h"

#include "EventListener.h"
#include "EventPauseGame.h"

USING_NS_CC;

//Constants --------------------------------
const int MIN_PER_HOUR = 60;
const int SEC_PER_MIN = 60;

const char* DEBUG_OPTIONS_FILENAME = "Scripts\\DebugOptions.xml";

const cocos2d::ccColor3B DEFAULT_TIMER_COLOUR = ccc3( 255, 255, 255 );
const cocos2d::ccColor3B PULSE_TIMER_COLOUR = ccc3( 255, 0, 0 );

const int TAG_PULSE_ACTION = 54534;
const float PULSE_TRANSITION_TIME = 0.5f;

//------------------------------------------
GameTimer::GameTimer() :
	START_TIMER				( 60.0f ),
	UPDATE_TIMER_INTERVAL	( 0.0f ), 
	m_currTime				( START_TIMER ),
	m_pTimerOutput			( NULL ),

	m_pPulseSprite			( NULL ),
	m_pulseVelocity			( 1.0f ),
	m_pulsePerc				( 0.0f ),
	m_bPulseActive			( false )
{
	//Provide a debug timer if desired! (windows only, file directory will need testing for other platforms!)
	if( CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )
	{
		HasteFileXML xmlFile;

		//Check to see if file exists!
		if( xmlFile.VFileExist(DEBUG_OPTIONS_FILENAME) )
		{
			//Load the file
			xmlFile.VLoadFile( DEBUG_OPTIONS_FILENAME );
			
			//Access parent node
			if( xmlFile.FindAndAccessNode("DebugOptions") )
			{
				//Look for Game Timer node (may not exist!)
				if( xmlFile.FindAndAccessNode("GameTimer") )
				{
					//Update the current timer to the value requested!
					m_currTime = xmlFile.GetNodeData().f;
				}
			}
		}
	}
}
//------------------------------------------
GameTimer::~GameTimer()
{
}
//------------------------------------------
bool
GameTimer::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	//Listen for game pause event
	EventListener* pPauseGameListener = EventListener::create( this, callfuncO_selector(GameTimer::pauseGameCallback), TS_EVENT_ID_PAUSE_GAME );
	this->addChild( pPauseGameListener );

	const CCSize &visibleSize = CCDirector::sharedDirector()->getVisibleSize();

	//Create timer output
	m_pTimerOutput = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_30, visibleSize, kCCTextAlignmentRight, kCCVerticalTextAlignmentTop ); 
	m_pTimerOutput->setPosition( ccp(visibleSize.width*0.49f, visibleSize.height*0.49f) ); //1% from right corner
	m_pTimerOutput->setColor( DEFAULT_TIMER_COLOUR );
	updateOutputString();

	
	//Init pulse sprite
	m_pPulseSprite = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_TIMER_PULSE).c_str() );
	m_pPulseSprite->setColor( PULSE_TIMER_COLOUR );

	m_pPulseSprite->setPosition( ccp(visibleSize.width*0.945f, visibleSize.height*0.975f) );
	
	m_pPulseSprite->setOpacity( 0 );
	m_pPulseSprite->setScale( 0.55f );
	this->addChild( m_pPulseSprite );

	
	//Add output to scene, schduele the updater (timer)
	this->addChild( m_pTimerOutput );
	this->schedule( schedule_selector(GameTimer::updateTimer), UPDATE_TIMER_INTERVAL );

	return true;
}
//------------------------------------------
void
GameTimer::onExit()
{
	//Auto-release ---
	m_pTimerOutput = NULL;
	m_pPulseSprite = NULL;

	removeAllChildren();
}
//------------------------------------------
void
GameTimer::updateTimer(float elapsedTime)
{
	//Deduct current time by elapsed time
	m_currTime -= elapsedTime;

	if( m_currTime <= 0.0f )
	{
		this->unschedule( schedule_selector(GameTimer::updateTimer) );
		m_currTime = 0.0f;

		//Create the Game Timer expired event!
		EventGameTimerExpired::create();
	}

	updateOutputString();


	//Check if pulse is required
	if( !m_bPulseActive && m_currTime <= 10.0f )
	{
		if( m_pPulseSprite->numberOfRunningActions() == 0 )
		{
			float diff = m_currTime - 9.5f;

			m_pTimerOutput->runAction( CCTintTo::create(diff, PULSE_TIMER_COLOUR.r, PULSE_TIMER_COLOUR.g, PULSE_TIMER_COLOUR.b) );
			m_pPulseSprite->runAction( CCFadeIn::create(diff) );
		}

		if( m_pPulseSprite->getOpacity() == 255 )
		{
			m_bPulseActive = true;

			//Timer value Pulse (force to red)
			CCRepeatForever* pInfinitePulse = CCRepeatForever::create( CCSequence::createWithTwoActions(CCFadeOut::create(PULSE_TRANSITION_TIME), CCFadeIn::create(PULSE_TRANSITION_TIME)) );
			pInfinitePulse->setTag( TAG_PULSE_ACTION );

			m_pTimerOutput->runAction( pInfinitePulse );
		

			//Timer BG Sprite Pulse
			pInfinitePulse = CCRepeatForever::create( CCSequence::createWithTwoActions(CCFadeOut::create(PULSE_TRANSITION_TIME), CCFadeIn::create(PULSE_TRANSITION_TIME)) );
			pInfinitePulse->setTag( TAG_PULSE_ACTION );

			m_pPulseSprite->runAction( pInfinitePulse );
		}
	}
	else if( m_bPulseActive && m_currTime > 10.0f )
	{
		m_bPulseActive = false;

		m_pTimerOutput->stopActionByTag( TAG_PULSE_ACTION );
		m_pTimerOutput->runAction( CCFadeIn::create(PULSE_TRANSITION_TIME) );
		m_pTimerOutput->runAction( CCTintTo::create(PULSE_TRANSITION_TIME, 255, 255, 255) );

		m_pPulseSprite->stopActionByTag( TAG_PULSE_ACTION );;
	}
}
//------------------------------------------
void
GameTimer::pauseGameCallback(CCObject* pObject)
{
	EventPauseGame* pPauseGameEvent = static_cast<EventPauseGame*>( pObject );

	if( pPauseGameEvent->activatePause() )
	{
		this->pauseSchedulerAndActions();
		m_pPulseSprite->pauseSchedulerAndActions();
		m_pTimerOutput->pauseSchedulerAndActions();
	}
	else
	{
		this->resumeSchedulerAndActions();
		m_pPulseSprite->resumeSchedulerAndActions();
		m_pTimerOutput->resumeSchedulerAndActions();
	}
}
//------------------------------------------
void
GameTimer::updateOutputString()
{
	//http://www.cplusplus.com/forum/beginner/14357/ Minutes && Seconds format

	//Round up the current time!
	int currTime = static_cast<int>( ceilf(m_currTime) );

	int mins = ( currTime / SEC_PER_MIN) % MIN_PER_HOUR;
	int secs = currTime % SEC_PER_MIN;

	if( m_pTimerOutput )
	{
		std::stringstream ss;

		if( mins < 10 )
		{
			ss << "0";
		}

		ss << mins;
		ss << " : ";

		if( secs < 10 )
		{
			ss << "0";
		}

		ss << secs;

		m_pTimerOutput->setString( ss.str().c_str() );
	}
}
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------