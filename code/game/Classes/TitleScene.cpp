#include "TitleScene.h"

#include "SceneTypes.h"
#include "SceneManager.h"

#include "GameCoreData.h"

USING_NS_CC;

//---------------------------------------
CCScene* TitleScene::createScene()
{
    CCScene *scene = CCScene::create();
    TitleScene *layer = TitleScene::create();

    scene->addChild(layer);
    return scene;
}
//---------------------------------------
bool TitleScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
	this->setTouchEnabled(true);

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    // add "HelloWorld" splash screen"
	CCSprite* pSprite = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_LOGO).c_str() );
    pSprite->setPosition(ccp(visibleSize.width*0.5f + origin.x, visibleSize.height*0.5f + origin.y));
    this->addChild(pSprite, 0);


	this->schedule( schedule_selector(TitleScene::timerExpiredCallback), 1.0f );

    return true;
}
//---------------------------------------
void TitleScene::onExit()
{
	this->setTouchEnabled(false);
}
//---------------------------------------
void
TitleScene::ccTouchesEnded(CCSet* pTouches, CCEvent* pEvent)
{
	this->unschedule( schedule_selector(TitleScene::timerExpiredCallback) );

	//We simply want to swap the scene here
	SceneManager::swapScene( SCENE_MAIN_MENU );
}
//---------------------------------------
void
TitleScene::timerExpiredCallback(float elapsedTime)
{
	this->unschedule( schedule_selector(TitleScene::timerExpiredCallback) );
	SceneManager::swapScene( SCENE_MAIN_MENU );
}
//---------------------------------------
//---------------------------------------