#ifndef HIGHSCORE_MANAGER_H
#define HIGHSCORE_MANAGER_H

#include "cocos2d.h"
#include "SceneTypes.h"

struct HighscoreData
{
	HighscoreData(const char* initials, int score);

	std::string m_initals;
	int m_score;
};

class HasteFileXML;
class HighscoreManager : public cocos2d::CCLayer
{
public:
	HighscoreManager();
	~HighscoreManager();

	//Collect a singleton!
	static HighscoreManager* sharedHighscoreManager(bool bPerformNullTest = true);

	CREATE_FUNC( HighscoreManager );

	virtual bool init(); //Overloaded Init.
	virtual void onExit();

	void sceneTransitionComplete(SceneType sceneType);

	void resetLatestHighscoreId();

	std::string getHighScore(int index);
	const std::string& getHighScoreName(int index);

	std::string getHighestScore();
	int getLatestHighscoreId();

	int getNumActiveScores();

	bool isScoreHigh(int score);
	bool assignHighScore(const char* initials, int score);

private:

	typedef std::vector<HighscoreData> Highscores;

	//Private Methods ----
	void ParseXmlScore(HasteFileXML* pXmlFile);

	//Private Variables ----
	Highscores m_scores;

	int m_latestHighscoreId;

	//Static Private Variables ---- 
	static HighscoreManager* ms_sharedInstance;
};
#endif //HIGHSCORE_MANAGER_H