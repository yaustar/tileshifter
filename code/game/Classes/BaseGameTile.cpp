#include "BaseGameTile.h"

#include "GameCoreData.h"
#include "RandomNumberGenerator.h"

#include "EventListener.h"
#include "EventPauseGame.h"

//Use the 'cocos2d' namespace (only do this within the .cpp!)
USING_NS_CC; 

//Constants ----------------------------------------------
const	float			TOUCH_MOVE_AMOUNT		(5.0f);

//Static Variables --------------------------------------
//REMINDER: DEFAULT VALUES ARE DUPLICATED !!!
const CCPoint		BaseGameTile::ms_vectorDir[TILE_NUM_DIRECTIONS]				= { ccp(-1,0), ccp(1,0), ccp(0,1), ccp(0,-1) };
const TileDirection	BaseGameTile::ms_invertTileDirection[TILE_NUM_DIRECTIONS]	= { TILE_RIGHT, TILE_LEFT, TILE_BOTTOM, TILE_TOP };

TileMoveType		BaseGameTile::ms_tileMove									( TILE_MOVE_NULL );
CCPoint				BaseGameTile::ms_axisApplier								( ccp(0,0) );
float				BaseGameTile::ms_axisGridNodeSize							( 0.0f );

CCSprite*			BaseGameTile::ms_pExcessTileSprite							( NULL );
BaseGameTile*		BaseGameTile::ms_pExcessTileLocation						( NULL );

TileDirection		BaseGameTile::ms_tileDir[TILE_NUM_TYPE_DIRECTIONS]			= { TILE_LEFT, TILE_RIGHT };
int					BaseGameTile::ms_tilePositionPoint							( 0 );
bool				BaseGameTile::ms_bIsStaticVarDefault						( true );

//----------------------------------------------------
BaseGameTile::BaseGameTile(TileType tileID, const CCPoint &pos) :
	m_pSprite			( NULL ),
	m_tileID			( tileID ),
	TILE_SLIDE_SPEED	( 0.1f ),
	m_bIsMovedTile		( false ),
	m_bIsClickedTile	( false )
{
	for(int i = 0; i < TILE_NUM_DIRECTIONS; i++)
	{
		m_pNeighbourTiles[i] = NULL;
	}

	//Init the sprite
	createTileSprite( tileID, m_pSprite);
	m_pSprite->setPosition( pos );
	this->addChild(m_pSprite);

	m_endSlidePosition = m_pSprite->getPosition();

	//We are inside the creation process, static data can not be considered defaulted anymore
	ms_bIsStaticVarDefault = false;
}
//----------------------------------------------------
BaseGameTile::~BaseGameTile()
{
	//If the static data is NOT set to default (prevents multiple instances resetting the same data multiple times)
	if( !ms_bIsStaticVarDefault )
	{
		//CONSTANTS ------
		//ms_vectorDir[TILE_NUM_DIRECTIONS]			= { ccp(-1,0), ccp(1,0), ccp(0,1), ccp(0,-1) }; 
		//ms_invertTileDirection[TILE_NUM_DIRECTIONS]	= { TILE_RIGHT, TILE_LEFT, TILE_BOTTOM, TILE_TOP };

		ms_tileMove									= TILE_MOVE_NULL;
		ms_axisApplier								= ccp(0,0);
		ms_axisGridNodeSize							= 0.0f;

		removeExcessSprite();
		//ms_pExcessTileSprite						= NULL;
		//ms_pExcessTileLocation						= NULL;

		ms_tileDir[0]								= TILE_LEFT;
		ms_tileDir[1]								= TILE_RIGHT;

		ms_tilePositionPoint						= 0;
		ms_bIsStaticVarDefault = true;
	}
}
//----------------------------------------------------
bool
BaseGameTile::init()
{
	//--------------------------------------------------------
    // Super init first
    if ( !CCLayer::init() )
    {
        return false;
    };

	//We enable tile touch events 'manually' (handled within tile manager) in the next step!
	//Allows us to force tiles to recieve the touch event before the character object (which is a MUST!)
	//this->setTouchEnabled(true);
	
	//Listen for game pause event
	EventListener* pPauseGameListener = EventListener::create( this, callfuncO_selector(BaseGameTile::pauseGameCallback), TS_EVENT_ID_PAUSE_GAME );
	this->addChild( pPauseGameListener );

	return true;
}
//----------------------------------------------------
void
BaseGameTile::ccTouchesBegan(CCSet* pTouches, CCEvent* pEvent)
{
	if(!m_bIsClickedTile)
	{
		//Get First Touch - Do NOT handle multiple touches. 
		CCTouch* pCurrentTouch	( static_cast<CCTouch*>(pTouches->anyObject()) );

		 //Cannot make variables into const references? returned value will be removed from the stack??
		const CCPoint touchPoint	( pCurrentTouch->getLocation() ); //Cache the screen position player has touched
		const CCRect tileBound		( m_pSprite->boundingBox() );

		//Test for tile hit with pointer
		if( touchPoint.x >= tileBound.getMinX() && touchPoint.x <= tileBound.getMaxX() ) 
		{
			if( touchPoint.y >= tileBound.getMinY() && touchPoint.y <= tileBound.getMaxY() ) 
			{
				//We need to remove touch events from all other tiles (preventing other tiles calling 'move' and 'end', best for performance)
				swapNeighbourTouchEvents(this, false, true, true); 

				//Same as above (only would be less optimal without the above), specifically needed to prevent 'touch events' outside the tile ranges moving tiles.
				m_bIsClickedTile = true;

				m_pSprite->setColor( ccc3(255, 0, 255) );
			}
		}
	}
}
//----------------------------------------------------
void
BaseGameTile::ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent)
{
	if(m_bIsClickedTile)
	{
		//Get First Touch - Do NOT handle multiple touches. 
		CCTouch* pCurrentTouch ( static_cast<CCTouch*>(pTouches->anyObject()) );

		if( touchesMovedDeclareDirection(pCurrentTouch) ) //Work out an active direction (unless one already exists), skip if one wasn't allowed
		{
			//Calculate velocity based on current and previous 'touch pointer' positions. 
			CCPoint velocity = invertYAxis( pCurrentTouch->getLocationInView() ) - invertYAxis( pCurrentTouch->getPreviousLocationInView() );
			
			//The axis applier removes any data in the velocity (for the opposite axis movement that we don't want).
			velocity.x *= ms_axisApplier.x;
			velocity.y *= ms_axisApplier.y;

			if(velocity.equals( ccp(0.0f, 0.0f) ))
			{
				//The movement detected was for the other movement that we're locked out from!
				return;
			}

			moveTileChain(velocity); //Move neighbouring tiles (along the correct chain) by velocity
			moveTileByVelocity(velocity); //Move active tile by velocity
		}
	}
}
//----------------------------------------------------
void
BaseGameTile::ccTouchesEnded(CCSet* pTouches, CCEvent* pEvent)
{
	if(m_bIsClickedTile)
	{
		AxisAppliers aa; //Provides axis specific (i.e. only on horizontal or vertical based on which is currently selected!
		if(produceThisTilesAxisApplier(aa))
		{
			//We already know ALL other tile will NOT respond it touch events, so make sure this ONE also no longer responds!
			this->setTouchEnabled( false );

			//Calculate tile position 'point' (stored within static member)
			calculateTilesPositionPoint(aa); 

			//Store neighbour information based on OLD physical positions (also rechains ALL tile neighbours)
			TileNeighbourList neighboursByPoint ( ms_axisGridNodeSize ); 
			endTouchChainAndCacheExistingTiles( neighboursByPoint );

			//Apply new neighbours as required.
			endTouchUnchainAndUpdateTileNeighbours( neighboursByPoint, ms_tilePositionPoint );

			//Move tiles position to lock into grid
			applyEndTouchTilePositions(aa); 
		}

		else //There was no movement from the click
		{
			endTouchResetTileFlags();
		}
	}
}
//----------------------------------------------------
bool 
BaseGameTile::getNextCentrePosition(TileDirection currDir, const BaseGameTile* &pOutActiveTile, cocos2d::CCPoint& outValidCentrePos, cocos2d::CCPoint& outInstantJumpOffset) const
{
	const BaseGameTile* pCurrNextTile = getNeighbourTile( currDir );
	TileDirection invertedCurrDir = ms_invertTileDirection[currDir];

	if( pCurrNextTile == NULL )
	{
		pCurrNextTile = getConstantCornerTile( invertedCurrDir );
	}

	{//SCOPED - Work out the tile offset incase an INSTANT jump is needed (for when a character hits the edges)
		float tileOffset;

		if( currDir == TILE_LEFT || TILE_RIGHT )
		{
			tileOffset = GameCoreData::getTileData().tileSizeHalf.width;
		}
		else
		{
			tileOffset = GameCoreData::getTileData().tileSizeHalf.height;
		}

		outInstantJumpOffset = (ms_vectorDir[invertedCurrDir] * tileOffset);    //pCurrNextTile->getPosition() + (ms_vectorDir[invertedCurrDir] * tileOffset);
	}

	const TileDirectionList& tileDirList = pCurrNextTile->getValidTileDirectionList();

	for(unsigned int i = 0; i < tileDirList.size(); i++)
	{
		if( tileDirList[i] == invertedCurrDir )
		{
			outValidCentrePos = pCurrNextTile->getPosition();
			pOutActiveTile = pCurrNextTile;
			return true;
		}
	}
	

	return false;
}
//----------------------------------------------------
//Public Methods
//----------------------------------------------------
void
BaseGameTile::initTileNeighbours(const TileNeighbourStruct& neighbourList)
{
	for(int i = 0; i < TILE_NUM_DIRECTIONS; i++)
	{
		m_pNeighbourTiles[i] = neighbourList.pTileList[i];
	}
}
//----------------------------------------------------
TileDirection
BaseGameTile::getRandomTileDirection() const
{
	return	m_validTileDirList[RandomNumberGenerator::getNumber(0, m_validTileDirList.size()-1)];
}
//----------------------------------------------------
void
BaseGameTile::pauseGameCallback(CCObject* pObject)
{
	EventPauseGame* pPauseGameEvent = static_cast<EventPauseGame*>( pObject );

	if( pPauseGameEvent->activatePause() )
	{
		this->pauseSchedulerAndActions();
		m_pSprite->pauseSchedulerAndActions();
		this->setTouchEnabled( false );
	}
	else
	{
		this->resumeSchedulerAndActions();
		m_pSprite->resumeSchedulerAndActions();
		this->setTouchEnabled( true );
	}
}
//----------------------------------------------------
//Public Accessor Methods
//----------------------------------------------------
TileType
BaseGameTile::getTileTypeID()
{
	return m_tileID;
}
//----------------------------------------------------
const CCPoint&
BaseGameTile::getPosition() const
{
	return m_pSprite->getPosition();
}
//----------------------------------------------------
//Protected Methods
//----------------------------------------------------
void
BaseGameTile::moveTileChain(const cocos2d::CCPoint &velocity)
{
	for(int i = 0; i < TILE_NUM_TYPE_DIRECTIONS; i++)
	{
		//The directions tiles are moved in (i.e. left && right || up and down)
		if(m_pNeighbourTiles[ms_tileDir[i]])
		{
			m_pNeighbourTiles[ms_tileDir[i]]->moveTileChain(ms_tileDir[i], velocity);
		}
	}
}
//----------------------------------------------------
void
BaseGameTile::moveTileChain(TileDirection tileDirection, const cocos2d::CCPoint &velocity)
{
	//Set position
	moveTileByVelocity(velocity);

	if(m_pNeighbourTiles[tileDirection])
	{
		m_pNeighbourTiles[tileDirection]->moveTileChain(tileDirection, velocity); 
	}
}
//----------------------------------------------------
void
BaseGameTile::createTileSprite(TileType tileID, CCSprite* &pOutSprite)
{
	const CoreTileData& tileData ( GameCoreData::getTileData() );

	CCRect tileRect( tileData.tileSize.width * tileID, 0.0f, tileData.tileSize.width, tileData.tileSize.height );
	pOutSprite = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_TILE).c_str(), tileRect );

	//init the tile info. 
	initTileDirectionList();
}
//----------------------------------------------------
void
BaseGameTile::setTileOpacity(float opacity)
{
	m_pSprite->setOpacity( opacity );
}
//----------------------------------------------------
//Private Methods
//----------------------------------------------------
void
BaseGameTile::swapNeighbourTouchEvents(const BaseGameTile* pTileCaller, bool bSwapEventsOn, bool bFireInAllDiections, bool bIsStartTile)
{
	//Switch on / off touch events for neighboring tile nodes, through recursion. 
	
	//If we are firing from the starting tile.
	if(!bIsStartTile)
	{
		//Enable/Disable Touch Events On This Layer!
		this->setTouchEnabled( bSwapEventsOn );
	}

	if(bFireInAllDiections) //Used to restrict directions (prevents multiple nodes being assigned the on/off value twice!
	{
		swapNeighbourTouchEventsChainCaller(TILE_LEFT, pTileCaller, bSwapEventsOn, true);
		swapNeighbourTouchEventsChainCaller(TILE_RIGHT, pTileCaller, bSwapEventsOn, true);
	}

	swapNeighbourTouchEventsChainCaller(TILE_TOP, pTileCaller, bSwapEventsOn, false);
	swapNeighbourTouchEventsChainCaller(TILE_BOTTOM, pTileCaller, bSwapEventsOn, false);
}
//----------------------------------------------------
void 
BaseGameTile::swapNeighbourTouchEventsChainCaller(TileDirection tileDir, const BaseGameTile* pTileCaller, bool bSwapEventsOn, bool bFireInAllDiections)
{
	if( m_pNeighbourTiles[tileDir] )
	{
		if(m_pNeighbourTiles[tileDir] != pTileCaller)
		{
			m_pNeighbourTiles[tileDir]->swapNeighbourTouchEvents(this, bSwapEventsOn, bFireInAllDiections);
		}
	}
}
//----------------------------------------------------
CCPoint
BaseGameTile::invertYAxis(const CCPoint &convertPos)
{
	return CCPoint(convertPos.x, CCDirector::sharedDirector()->getVisibleSize().height - convertPos.y);
}
//----------------------------------------------------
bool
BaseGameTile::touchesMovedDeclareDirection(CCTouch* pCurrentTouch)
{
	if(ms_tileMove != TILE_MOVE_NULL) 
	{
		//Movement Direction Already Exists
		return true;
	}
	else //Attempt to produce a movement direction
	{
		//Collect start and current touch locations
		CCPoint startTouchPos = invertYAxis( pCurrentTouch->getStartLocationInView() );
		CCPoint currTouchPos = invertYAxis( pCurrentTouch->getLocationInView() );

		//Calculate lengths of X and Y 
		CCPoint touchLen (startTouchPos.x - currTouchPos.x, startTouchPos.y - currTouchPos.y);

		//Force positive values
		touchLen.x = fabsf(touchLen.x);
		touchLen.y = fabsf(touchLen.y);

		//Compute direction (returns false if one cannot be evaluated yet) 
		
		if( touchesMovedComputeDirection(touchLen) )
		{
			//We now have a direction, so we can access the tile chain for the first time here! 
			//Update the file chains 'Is Moving' to true
			setTileChainIsMoving(true);
			return true;
		}
		
		return false;
	}
}
//----------------------------------------------------
bool 
BaseGameTile::touchesMovedComputeDirection(const CCPoint& touchLen)
{
	//If one has exceeded the safe zone
	if(touchLen.x > TOUCH_MOVE_AMOUNT || touchLen.y > TOUCH_MOVE_AMOUNT)
	{
		//If it is the X that exceeded the safe zone, set Horizontal direction
		if(touchLen.x > TOUCH_MOVE_AMOUNT)
		{
			ms_tileMove = TILE_MOVE_HOR; //Set flag
			ms_axisApplier = ccp( 1.0f, 0.0f );
			ms_axisGridNodeSize = GameCoreData::getTileData().gridNodeSize.width;

			ms_tileDir[TILE_LOW_DIR] = TILE_LEFT;
			ms_tileDir[TILE_HIGH_DIR] = TILE_RIGHT;
		}
		//else it is the Y that exceeded the safe zone, set Vertical direction
		else
		{
			ms_tileMove = TILE_MOVE_VER; //Set flag
			ms_axisApplier = ccp( 0.0f, 1.0f );
			ms_axisGridNodeSize = GameCoreData::getTileData().gridNodeSize.height;

			ms_tileDir[TILE_LOW_DIR] = TILE_BOTTOM;
			ms_tileDir[TILE_HIGH_DIR] = TILE_TOP;
		}

		return true;
	}
	
	return false;
}
//----------------------------------------------------
void
BaseGameTile::moveTileByVelocity(const cocos2d::CCPoint& velocity)
{
	const CoreTileData& tileData ( GameCoreData::getTileData() );

	m_pSprite->setPosition( m_pSprite->getPosition() + velocity );

	if( velocity.x != 0.0f )
	{
		if( m_pSprite->getPositionX() < tileData.gridBound.getMinX() )
		{
			wrapTileTransition( tileData.gridBound.getMinX(), m_pSprite->getPositionX(), CCPoint(1.0f, 0.0f) );//-velocity.normalize() );
		}
		else if( m_pSprite->getPositionX() > tileData.gridBound.getMaxX() - tileData.tileSize.width )
		{
			wrapTileTransition( tileData.gridBound.getMaxX() - tileData.tileSize.width, m_pSprite->getPositionX(), CCPoint(-1.0f, 0.0f) );
		}
		else
		{
			m_pSprite->setOpacity(255.0f);
		}
	}
	else
	{
		if( m_pSprite->getPositionY() < tileData.gridBound.getMinY() )
		{
			wrapTileTransition( tileData.gridBound.getMinY(), m_pSprite->getPositionY(), CCPoint(0.0f, 1.0f) );
		}
		else if( m_pSprite->getPositionY() > tileData.gridBound.getMaxY() - tileData.tileSize.height )
		{
			wrapTileTransition( tileData.gridBound.getMaxY() - tileData.tileSize.height, m_pSprite->getPositionY(), CCPoint(0.0f, -1.0f) );
		}
		else
		{
			m_pSprite->setOpacity(255.0f);
		}
	}
}
//----------------------------------------------------
void
BaseGameTile::wrapTileTransition(float boundAxis, float spriteAxis, const CCPoint& axisApplier)
{
	createExcessSprite(); //Create excess sprite (only if non-existent)

	const CoreTileData& tileData ( GameCoreData::getTileData() );

	CCPoint gridAxisApplier ( axisApplier.x * tileData.gridSize.width, axisApplier.y * tileData.gridSize.height );
	ms_pExcessTileSprite->setPosition( m_pSprite->getPosition() + gridAxisApplier );

	float diff ( fabsf(boundAxis - spriteAxis) );
	float opacityDiff = (255.0f / tileData.tileSize.width) * diff;

	m_pSprite->setOpacity( 255.0f - opacityDiff );
	ms_pExcessTileSprite->setOpacity( opacityDiff );	

	if(gridAxisApplier.x > 0.0f || gridAxisApplier.y > 0.0f)
	{
		float tileSize = axisApplier.x == 0.0f ? tileData.tileSize.height : tileData.tileSize.width; 

		if( spriteAxis <= (boundAxis - tileSize) )
		{
			m_pSprite->setOpacity( 255.0f );
			m_pSprite->setPosition( m_pSprite->getPosition() + gridAxisApplier );
			
			removeExcessSprite();
		}
	}
	else
	{
		float tileSize = axisApplier.x == 0.0f ? tileData.tileSize.height : tileData.tileSize.width; 

		if( spriteAxis >= (boundAxis + tileSize) )
		{
			m_pSprite->setOpacity( 255.0f );
			m_pSprite->setPosition( m_pSprite->getPosition() + gridAxisApplier );
			
			removeExcessSprite();
		}
	}
}
//----------------------------------------------------
void
BaseGameTile::createExcessSprite()
{
	//Bug where excess tile was incorrect during direction change (during tile move) is fixed here
	//Basically tests for a change in the tile ID, removign the current tile if it changes. 
	if( ms_pExcessTileLocation ) 
	{
		if( ms_pExcessTileLocation->getTileTypeID() != m_tileID )
		{
			removeExcessSprite();
		}
	}

	if( ms_pExcessTileSprite == NULL )
	{
		createTileSprite( m_tileID, ms_pExcessTileSprite );
		ms_pExcessTileSprite->setPositionY( m_pSprite->getPositionY() );
		this->addChild(ms_pExcessTileSprite);
		
		ms_pExcessTileLocation = this;
	}
}
//----------------------------------------------------
void
BaseGameTile::removeExcessSprite()
{
	if(ms_pExcessTileSprite)
	{
		ms_pExcessTileLocation->removeChild( ms_pExcessTileSprite );
		ms_pExcessTileLocation->setTileOpacity( 255.0f ); //The tile creating the excess tile is likely to be transparent (remove it here)

		ms_pExcessTileLocation->removeChild( ms_pExcessTileSprite );
		ms_pExcessTileLocation = NULL;
		ms_pExcessTileSprite = NULL;
	}
}
//----------------------------------------------------
CCSprite*
BaseGameTile::getSprite()
{
	return m_pSprite;
}
//----------------------------------------------------
void
BaseGameTile::setSprite(CCSprite* pSprite)
{
	m_pSprite = pSprite;
}
//----------------------------------------------------
BaseGameTile*
BaseGameTile::getCornerTile(TileDirection tileDirection)
{
	if(m_pNeighbourTiles[tileDirection])
	{
		return m_pNeighbourTiles[tileDirection]->getCornerTile( tileDirection );
	}
	else
	{
		return this;
	}
}
//----------------------------------------------------
BaseGameTile* 
BaseGameTile::getCornerTile(TileMoveTypeDirection tileMoveTypeDir, int currPoint)
{
	BaseGameTile* pTileAccess = this;

	if( tileMoveTypeDir == TILE_LOW_DIR )
	{
		while( currPoint != 0 )
		{
			currPoint--;
			pTileAccess = pTileAccess->getNeighbourTile( ms_tileDir[TILE_LOW_DIR] );
		};
	}
	else //Must be 'high'
	{
		while( currPoint != static_cast<int>((ms_axisGridNodeSize-1.0f)) )
		{
			currPoint++;
			pTileAccess = pTileAccess->getNeighbourTile( ms_tileDir[TILE_HIGH_DIR] );
		};
	}

	return pTileAccess;
}
//----------------------------------------------------
BaseGameTile*
BaseGameTile::getNeighbourTile(TileDirection tileDirection) const
{
	return m_pNeighbourTiles[tileDirection];
}
//----------------------------------------------------
const BaseGameTile::TileDirectionList&
BaseGameTile::getValidTileDirectionList() const
{
	return m_validTileDirList;
}
//----------------------------------------------------
const BaseGameTile* 
BaseGameTile::getConstantNeighbourTile(TileDirection tileDirection) const
{
	return getNeighbourTile( tileDirection );
}
//----------------------------------------------------
const BaseGameTile*
BaseGameTile::getConstantCornerTile(TileDirection tileDirection) const
{
	if(m_pNeighbourTiles[tileDirection])
	{
		return m_pNeighbourTiles[tileDirection]->getCornerTile( tileDirection );
	}
	else
	{
		return this;
	}
}
//----------------------------------------------------
float
BaseGameTile::getTilesHalfOffset(TileDirection currDir) const
{
	if( currDir == TILE_LEFT || TILE_RIGHT )
	{
		return GameCoreData::getTileData().tileSizeHalf.width;
	}
	else
	{
		return GameCoreData::getTileData().tileSizeHalf.height;
	}
}
//----------------------------------------------------
float 
BaseGameTile::getTilesOpacity() const
{
	return m_pSprite->getOpacity();
}
//----------------------------------------------------
void
BaseGameTile::setTileChainIsMoving(bool setTo)
{
	m_bIsMovedTile = setTo;

	for(int i = 0; i < TILE_NUM_TYPE_DIRECTIONS; i++)
	{
		//The directions tiles are moved in (i.e. left && right || up and down)
		if(m_pNeighbourTiles[ms_tileDir[i]])
		{
			m_pNeighbourTiles[ms_tileDir[i]]->setTileChainIsMoving(ms_tileDir[i], setTo);
		}
	}
}
//----------------------------------------------------
void
BaseGameTile::setTileChainIsMoving(TileDirection tileDirection, bool setTo)
{
	m_bIsMovedTile = setTo;

	if(m_pNeighbourTiles[tileDirection])
	{
		m_pNeighbourTiles[tileDirection]->setTileChainIsMoving(tileDirection, setTo); 
	}
}
//----------------------------------------------------
bool
BaseGameTile::isTileMoving() const
{
	return m_bIsMovedTile;
}
//----------------------------------------------------
const CCPoint&
BaseGameTile::getConstPos() const
{
	return m_pSprite->getPosition();
}
//----------------------------------------------------
const CCPoint&
BaseGameTile::getEndSlidePosition() const
{
	return m_endSlidePosition;
}
//----------------------------------------------------
const float
BaseGameTile::getTileSlideSpeed() const
{
	return TILE_SLIDE_SPEED;
}
//----------------------------------------------------
BaseGameTile*
BaseGameTile::getNeighbourTile(cocos2d::CCPoint neighbourOffset)
{
	BaseGameTile* pTileAccess = this;

	if( neighbourOffset.x > 0.0f )
	{
		while(neighbourOffset.x != 0.0f)
		{
			neighbourOffset.x--;
			pTileAccess = pTileAccess->getNeighbourTile( TILE_RIGHT );
		}
	}
	else
	{
		while(neighbourOffset.x != 0.0f)
		{
			neighbourOffset.x++;
			pTileAccess = pTileAccess->getNeighbourTile( TILE_LEFT );
		}
	}

	if( neighbourOffset.y > 0.0f )
	{
		while(neighbourOffset.y != 0.0f)
		{
			neighbourOffset.y--;
			pTileAccess = pTileAccess->getNeighbourTile( TILE_TOP );
		}
	}
	else
	{
		while(neighbourOffset.y != 0.0f)
		{
			neighbourOffset.y++;
			pTileAccess = pTileAccess->getNeighbourTile( TILE_BOTTOM );
		}
	}

	return pTileAccess;
}
//----------------------------------------------------
void
BaseGameTile::initTileNeighbours(const TileNeighbourStruct& neighbourList, TileMoveType applyMovementDirection)
{	
	TileDirection tileDir[TILE_NUM_TYPE_DIRECTIONS];

	if( applyMovementDirection == TILE_MOVE_HOR )
	{
		tileDir[TILE_LOW_DIR] = TILE_LEFT;
		tileDir[TILE_HIGH_DIR] = TILE_RIGHT;
	}
	else if(  applyMovementDirection == TILE_MOVE_VER )
	{
		tileDir[TILE_LOW_DIR] = TILE_BOTTOM;
		tileDir[TILE_HIGH_DIR] = TILE_TOP;
	}
	else
	{
		initTileNeighbours( neighbourList );
		return;
	}

	m_pNeighbourTiles[tileDir[TILE_LOW_DIR]] = neighbourList.pTileList[tileDir[TILE_LOW_DIR]];
	if( m_pNeighbourTiles[tileDir[TILE_LOW_DIR]] )
	{
		m_pNeighbourTiles[tileDir[TILE_LOW_DIR]]->initTileNeighbours( this, tileDir[TILE_HIGH_DIR] );
	}

	m_pNeighbourTiles[tileDir[TILE_HIGH_DIR]] = neighbourList.pTileList[tileDir[TILE_HIGH_DIR]];
	if( m_pNeighbourTiles[tileDir[TILE_HIGH_DIR]] )
	{
		m_pNeighbourTiles[tileDir[TILE_HIGH_DIR]]->initTileNeighbours( this, tileDir[TILE_LOW_DIR] );
	}
	
}
//----------------------------------------------------
void
BaseGameTile::initTileNeighbours(BaseGameTile* pTile, TileDirection applyMovementDirection)
{
	m_pNeighbourTiles[applyMovementDirection] = pTile;
}
//----------------------------------------------------
bool
BaseGameTile::produceThisTilesAxisApplier(AxisAppliers& outAxisApplier)
{
	const CoreTileData& tileData ( GameCoreData::getTileData() );

	if( ms_tileMove == TILE_MOVE_HOR )
	{
		outAxisApplier.axisApplierInverted = ccp( 0.0f, 1.0f );

		outAxisApplier.spritePos	= m_pSprite->getPositionX();
		outAxisApplier.gridBoundMin	= tileData.gridBound.getMinX();
		outAxisApplier.gridBoundMax	= tileData.gridBound.getMaxX();
		outAxisApplier.tileSize		= tileData.tileSize.width;
		outAxisApplier.tileSizeHalf	= tileData.tileSizeHalf.width; 
	}
	else if ( ms_tileMove == TILE_MOVE_VER )
	{
		outAxisApplier.axisApplierInverted = ccp( 1.0f, 0.0f );

		outAxisApplier.spritePos	= m_pSprite->getPositionY();
		outAxisApplier.gridBoundMin	= tileData.gridBound.getMinY();
		outAxisApplier.gridBoundMax	= tileData.gridBound.getMaxY();
		outAxisApplier.tileSize		= tileData.tileSize.height;
		outAxisApplier.tileSizeHalf	= tileData.tileSizeHalf.height;
	}
	else
	{
		//Must of been NO MOVEMENT!
		return false; //No changes should of happened!
	}

	return true;
}
//----------------------------------------------------
void
BaseGameTile::calculateTilesPositionPoint(const AxisAppliers& axisApplier)
{
	//Calculate the point in which the current tile should be set to (i.e. forcing the tile into alignment with the grid)
	if( axisApplier.spritePos > axisApplier.gridBoundMax - axisApplier.tileSizeHalf )
	{
		ms_tilePositionPoint = 0; //Should be pushed into the first point 
	}
	else if( axisApplier.spritePos < axisApplier.gridBoundMin - axisApplier.tileSizeHalf )
	{
		ms_tilePositionPoint = ms_axisGridNodeSize - 1.0f; //Push into the end point
	}
	else
	{
		ms_tilePositionPoint = ((axisApplier.spritePos - axisApplier.gridBoundMin) / axisApplier.tileSize) + 0.5f; //Calculate between the centre points
	}
}
//----------------------------------------------------
void
BaseGameTile::applyEndTouchTilePositions(const AxisAppliers& axisApplier)
{
	CoreTileData tileData = GameCoreData::getTileData();

	//The tiles neighbours have been updated to the new slots (despite their position not yet reflecting this), we take advantage of this here.
	BaseGameTile* pCornerTile	= getCornerTile(ms_tileDir[TILE_LOW_DIR]);
	
	//Provide a single float variable that contains the active axis (done so through the axis applier)
	float pos = ((pCornerTile->getSprite()->getPositionX() + tileData.tileSizeHalf.width) * ms_axisApplier.x) +
				((pCornerTile->getSprite()->getPositionY() + tileData.tileSizeHalf.height) * ms_axisApplier.y);

	float gridSize =	(tileData.gridBound.getMaxX() * ms_axisApplier.x) +
						(tileData.gridBound.getMaxY() * ms_axisApplier.y);

	
	if( pos >= gridSize )
	{
		//Swaps the SPRITES Completely as the active tile is set to the wrong side!
		if( !swapSpriteWithExcessSprite(pCornerTile) )
		{
			//If the swap couldn't occur! (then the excess tile doesn't exist!)
			//Perform manual offset.
			CCSprite* pCornerTileSprite = pCornerTile->getSprite();

			pCornerTileSprite->setPosition	( ccp(	pCornerTileSprite->getPositionX() - (tileData.gridSize.width * ms_axisApplier.x),
													pCornerTileSprite->getPositionY() - (tileData.gridSize.height * ms_axisApplier.y) )
											);
		}
	}
	else
	{
		pCornerTile	= getCornerTile(ms_tileDir[TILE_HIGH_DIR]);

		pos =	(pCornerTile->getSprite()->getPositionX() * ms_axisApplier.x) +
				(pCornerTile->getSprite()->getPositionY() * ms_axisApplier.y);

		gridSize = (tileData.gridBound.getMinX() * ms_axisApplier.x) +
					(tileData.gridBound.getMinY() * ms_axisApplier.y);
	
	
		if( pos < gridSize )
		{
			//Swaps the SPRITES Completely as the active tile is set to the wrong side!
			if( !swapSpriteWithExcessSprite(pCornerTile) )
			{
				//If the swap couldn't occur! (then the excess tile doesn't exist!)
				//Perform manual offset.
				CCSprite* pCornerTileSprite = pCornerTile->getSprite();

				pCornerTileSprite->setPosition	( ccp(	pCornerTileSprite->getPositionX() + (tileData.gridSize.width * ms_axisApplier.x),
														pCornerTileSprite->getPositionY() + (tileData.gridSize.height * ms_axisApplier.y) )
												);
			}
		}
	}

	//Apply the slide to tiles!
	endTochSlideTileToPos( (ccp( m_pSprite->getPositionX() * axisApplier.axisApplierInverted.x, m_pSprite->getPositionY() * axisApplier.axisApplierInverted.y )) +
																	( ms_axisApplier * (axisApplier.gridBoundMin + ( ms_tilePositionPoint * axisApplier.tileSize )) ));
}
//----------------------------------------------------
bool
BaseGameTile::swapSpriteWithExcessSprite(BaseGameTile* pTile)
{
	if( ms_pExcessTileSprite )
	{
		CCSprite* pExcessSprite = ms_pExcessTileSprite;
		ms_pExcessTileSprite = pTile->getSprite();
		pTile->setSprite( pExcessSprite );

		return true;
	}
	

	return false;

}
//----------------------------------------------------
void
BaseGameTile::endTouchResetTileFlags()
{
	m_bIsClickedTile = false;
	swapNeighbourTouchEvents(this, true, true, false); //Reactivate neighbours touch events (as we can click any of them again!) (Last argument is false, as it will reset THIS tiles touch to be active again!)

	ms_tileMove = TILE_MOVE_NULL;
	m_pSprite->setColor( ccc3(255, 255, 255) );

	removeExcessSprite();

	//Finally remove 'is moving tile'
	setTileChainIsMoving(false);
}
//----------------------------------------------------
void
BaseGameTile::endTochSlideTileToPos(const CCPoint& position)
{
	CCPoint velocity = position - m_pSprite->getPosition();

	if( ms_pExcessTileSprite )
	{
		slideTile( ms_pExcessTileSprite, ms_pExcessTileSprite->getPosition() + velocity ); //CALL FIRST!! - This call effects the current instances 'm_endSlidePosition'! (overirdden in next call)
	}
	slideTile( this->m_pSprite, position, true );
	

	//slide neighbouring/linked tiles!
	for(int i = 0; i < TILE_NUM_TYPE_DIRECTIONS; i++)
	{
		//The directions tiles are moved in (i.e. left && right || up and down)
		if(m_pNeighbourTiles[ms_tileDir[i]])
		{
			m_pNeighbourTiles[ms_tileDir[i]]->endTouchSlideTileChainPos( ms_tileDir[i], velocity );
		}
	}
}
//----------------------------------------------------
void
BaseGameTile::endTouchSlideTileChainPos(TileDirection tileDirection, const CCPoint& velocity)
{
	slideTile( this->m_pSprite, m_pSprite->getPosition() + velocity );

	if(m_pNeighbourTiles[tileDirection])
	{
		m_pNeighbourTiles[tileDirection]->endTouchSlideTileChainPos( tileDirection, velocity );
	}
}
//----------------------------------------------------
void
BaseGameTile::slideTile(CCSprite* moveSprite, const CCPoint& position, bool resetFlagOnCompletion)
{
	//Cache the position that we will stop at. (allows external systems to move themelseves in accordance!)
	m_endSlidePosition = position;

	//Move position of sprite
	CCMoveTo* pMoveTo = CCMoveTo::create(TILE_SLIDE_SPEED, position);

	if( resetFlagOnCompletion )
	{
		//Once move complete, reset tile flags!
		CCCallFunc* pResetTileFlagsCallBack = CCCallFunc::create( this, callfunc_selector(BaseGameTile::endTouchResetTileFlags) );

		//Push into sequence
		CCSequence* pSequence = CCSequence::createWithTwoActions(pMoveTo, pResetTileFlagsCallBack); //Once move complete call 'Centre' call back

		//Run sequence of events
		moveSprite->runAction( pSequence );
	}
	else
	{
		moveSprite->runAction( pMoveTo );
	}
}
//----------------------------------------------------
void
BaseGameTile::endTouchChainAndCacheExistingTiles(TileNeighbourList &outTilesByPoint)
{
	BaseGameTile* pTileAccess ( getCornerTile(ms_tileDir[TILE_LOW_DIR]) ); //Access lowest (first) tile

	{
		BaseGameTile* pLowestTile ( pTileAccess ); //Cache lowest tile
		BaseGameTile* pHighestTile ( getCornerTile(ms_tileDir[TILE_HIGH_DIR]) ); //Cache highest (last) tile

		//Link the first and last tile neighbours to point to each other (creating an endless chain between the 'active chain')
		pLowestTile->initTileNeighbours( pHighestTile, ms_tileDir[TILE_LOW_DIR] );
		pHighestTile->initTileNeighbours( pLowestTile, ms_tileDir[TILE_HIGH_DIR] );
	}

	for(unsigned int i = 0; i < outTilesByPoint.size(); i++)
	{
		for(int n = 0; n < TILE_NUM_DIRECTIONS; n++)
		{
			//Store every neighbour based on the old points/positions of the tiles
			outTilesByPoint[i].pTileList[n] = pTileAccess->getNeighbourTile( static_cast<TileDirection> (n) );
		}

		pTileAccess = pTileAccess->getNeighbourTile( ms_tileDir[TILE_HIGH_DIR] );
	}
}
//----------------------------------------------------
void
BaseGameTile::endTouchUnchainAndUpdateTileNeighbours(const TileNeighbourList& neighboursByPoint, int tilePosPoint)
{
	//Apply NULL (un-chaining the tiles) to the back of the NEW chain
	getCornerTile(TILE_HIGH_DIR, tilePosPoint)->initTileNeighbours( NULL, ms_tileDir[TILE_HIGH_DIR] );

	//Apply NULL (un-chaining the tiles) to the front of the NEW chain
	BaseGameTile* pTileAccess = getCornerTile(TILE_LOW_DIR, tilePosPoint); //Caches NEW first tile before applying!
	pTileAccess->initTileNeighbours( NULL, ms_tileDir[TILE_LOW_DIR] );

	for(unsigned int i = 0; i < neighboursByPoint.size(); i++)
	{
		//We only want to mod (to the old neighbours) the new tiles neighbours against on the opposite direction tiles 
		pTileAccess->initTileNeighbours( neighboursByPoint[i], ms_tileMove == TILE_MOVE_HOR ? TILE_MOVE_VER : TILE_MOVE_HOR );
		
		//Get next direction (we go left to right)
		pTileAccess = pTileAccess->getNeighbourTile( ms_tileDir[TILE_HIGH_DIR] );
	}
}
//----------------------------------------------------
void
BaseGameTile::initTileDirectionList()
{
	switch ( m_tileID )
	{
	case TILE_ALL:
		m_validTileDirList.push_back( TILE_LEFT );
		m_validTileDirList.push_back( TILE_RIGHT );
		m_validTileDirList.push_back( TILE_TOP );
		m_validTileDirList.push_back( TILE_BOTTOM );
		break;
	case TILE_LEFT_RIGHT:
		m_validTileDirList.push_back( TILE_LEFT );
		m_validTileDirList.push_back( TILE_RIGHT );
		break;
	case TILE_TOP_BOTTOM:
		m_validTileDirList.push_back( TILE_TOP );
		m_validTileDirList.push_back( TILE_BOTTOM );
		break;
	case TILE_RIGHT_BOTTOM:
		m_validTileDirList.push_back( TILE_RIGHT );
		m_validTileDirList.push_back( TILE_BOTTOM );
		break;
	case TILE_LEFT_TOP:
		m_validTileDirList.push_back( TILE_LEFT );
		m_validTileDirList.push_back( TILE_TOP );
		break;
	case TILE_RIGHT_TOP:
		m_validTileDirList.push_back( TILE_RIGHT );
		m_validTileDirList.push_back( TILE_TOP );
		break;
	case TILE_LEFT_BOTTOM:
		m_validTileDirList.push_back( TILE_LEFT );
		m_validTileDirList.push_back( TILE_BOTTOM );
		break;

	default:
		break;
	}
}