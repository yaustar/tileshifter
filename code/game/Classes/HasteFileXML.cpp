#include "HasteFileXML.h"

#include "cocos2d.h"
#include "StringExt.h"

//-----------------------------------------
HasteFileXML::HasteFileXML() :
	m_pHeadNode		( NULL ),
	m_pActiveNode	( NULL )
{

}
//-----------------------------------------
HasteFileXML::~HasteFileXML()
{
	m_pActiveNode = m_pHeadNode; //Access head of node tree 

	NodeXML* pPrevSibling = NULL; //Caches the previous sibling (allowing deleted sibling linkers to be cleared)

	//While we have an active node
	while( m_pActiveNode )
	{
		//If a sibling exists
		if( m_pActiveNode->pSibling )
		{
			pPrevSibling = m_pActiveNode; //Cache the active node (potentially for later), will soon become the previous sibling.
			m_pActiveNode = m_pActiveNode->pSibling; //Set active node to the next sibling
			continue; //Reset loop (will try and find another sibling, or further chilidren!)
		}

		//If no siblings exist (provided above), but a child does exist
		if( m_pActiveNode->pChild )
		{
			m_pActiveNode = m_pActiveNode->pChild; //Set active node as the child node
			continue; //Reset loop (will try and find another sibling, or further chilidren!)
		}

		//Once here we have reached an end of both a sibling and child tree node (we can go no further)

		//Cache the parent node (before we delete the node containing this required information)
		NodeXML* pParent = m_pActiveNode->pParent;
		const NodeXML* pCurrNode = m_pActiveNode; //MUST BE CONSTANT!! We delete the active node! We only maintain this as we wnat to compare the pointer address!!!

		//If we have used a dynamically allocated string
		if( m_pActiveNode->dataType == XML_STRING )
		{
			//Unallocate it.
			CC_SAFE_DELETE ( m_pActiveNode->data.pS );
		}

		//Unallocate the acitve node
		CC_SAFE_DELETE( m_pActiveNode );

		//If the previous sibling exists
		if( pPrevSibling && pCurrNode == pPrevSibling->pSibling )
		{
			pPrevSibling->pSibling = NULL; //Update it so that it no longer points to its deleted sibling.
			pPrevSibling = NULL; //Update the pointer to null (ready for a new prev sibling (if any)).
		}
		else
		{
			//No sibling exists, but if their is an active parent
			if( pParent )
			{
				//if( pParent->pChild->pSibling )
				//ADD TEST that doesn't skip siblings! when removing the child. 

				if( pParent->pChild == pCurrNode )
				{
					//Update the child of the parent to null (as we have just unallocated it!)
					pParent->pChild = NULL;
				}
				//else this is a child, but it's not the one referenced by the parent (it's a sibling of it!)
				else
				{
					//The link through the sibling still needs destroying!
					NodeXML* pCurrChild = pParent->pChild;

					while( pCurrChild->pSibling != pCurrNode )
					{
						pCurrChild = pCurrChild->pSibling;
					}

					pCurrChild->pSibling = NULL;
				}
			}
		}

		if( !pParent )
		{
			if( pCurrNode == m_pHeadNode )
			{
				m_pActiveNode = NULL;
				m_pHeadNode = NULL; //Clear the head node (stored in 'active node')
			}
			else
			{
				m_pActiveNode = m_pHeadNode;
				
				//while the sibling of the active node is not this node, 
				while( m_pActiveNode->pSibling && m_pActiveNode->pSibling != pCurrNode )
				{
					m_pActiveNode = m_pActiveNode->pSibling;
				}
				m_pActiveNode->pSibling = NULL; //we need to remove this link

				m_pActiveNode = m_pHeadNode;
			}
		}
		else
		{
			//Active node is set to the parent
			m_pActiveNode = pParent;
		}
	}
}
//-----------------------------------------
void
HasteFileXML::VLoadFile(const char* fileName, bool bLoadFromWriteMemory)
{
	HasteFileText::VLoadFile( fileName, bLoadFromWriteMemory ); //Load the text file (XML is text)

	size_t currPos = PassXmlHeaderInfo(); //Remove the XML definition/description!

	//While a node exists, find the next node
	while( FindNextNode(currPos) )
	{
		CollectNodeData( currPos ); //Cache the current nodes data 

		while ( DetectNodeEnd(currPos) ); //While we a re hitting 'node ends', keep detecting them (controls scene graph depth)
	}
	
	VRemoveFileBuffer(); //We have processed the XML information into data structures more useful, remove the string.

	//Reset/init active node to the start!
	m_pActiveNode = NULL;
}
//-----------------------------------------
bool
HasteFileXML::NodeContainsData()
{
	if( m_pActiveNode->dataType == XML_NONE )
	{
		return false;
	}

	return true;
}
//-----------------------------------------
const XMLData&
HasteFileXML::GetNodeData()
{
	return m_pActiveNode->data;
}
//-----------------------------------------
XMLDataType
HasteFileXML::GetNodeDataType()
{
	return m_pActiveNode->dataType;
}
//-----------------------------------------
void
HasteFileXML::ResetNodeAccess()
{
	m_pActiveNode = NULL;
}
//-----------------------------------------
void
HasteFileXML::MoveToParent()
{
	m_pActiveNode = m_pActiveNode->pParent;
}
//-----------------------------------------
bool
HasteFileXML::FindAndAccessNode(const char* nodeName, bool bApplyToAccessor)
{
	NodeXML* pSearchNode = NULL;
	
	if( m_pActiveNode )
	{
		pSearchNode = m_pActiveNode->pChild;
	}
	else
	{
		pSearchNode = m_pHeadNode;
	}

	//Test Search Node Exist!
	if( !pSearchNode )
	{
		return false; //No info. 
	}
	
	while( strcmp(pSearchNode->nodeName, nodeName) != 0 )
	{
		if( pSearchNode->pSibling )
		{
			pSearchNode = pSearchNode->pSibling;
			continue;
		}
		
		//We have reached the end of the search! No match was found! (maintain active node pointer where it started from!)
		return false;
	}

	if( bApplyToAccessor )
	{
		m_pActiveNode = pSearchNode;
	}

	return true;
}
//-----------------------------------------
bool
HasteFileXML::FindAndAccessNodeSibling(const char* nodeName)
{
	NodeXML* pSearchNode = NULL;
	
	if( m_pActiveNode )
	{
		pSearchNode = m_pActiveNode->pSibling;
	}
	else
	{
		m_pActiveNode = m_pHeadNode;
		pSearchNode = m_pHeadNode->pSibling;
	}

	if( !pSearchNode )
	{
		return false;
	}
	
	while( strcmp(pSearchNode->nodeName, nodeName) != 0 )
	{
		if( pSearchNode->pSibling )
		{
			pSearchNode = pSearchNode->pSibling;
			continue;
		}
		
		//We have reached the end of the search! No match was found! (maintain active node pointer where it started from!)
		return false;
	}

	m_pActiveNode = pSearchNode;
	return true;
}
//-----------------------------------------
size_t
HasteFileXML::PassXmlHeaderInfo()
{
	//If we have a valid header
	if( m_fileBuffer.find("<?") != std::string::npos )
	{
		//Find the end of it
		size_t pos = m_fileBuffer.find( "?>" );

		if( pos != std::string::npos )
		{
			pos += 2;
			return pos;
		}
	}

	return 0;
}
//-----------------------------------------
bool
HasteFileXML::FindNextNode(size_t &inOutCurrPos)
{
	inOutCurrPos = m_fileBuffer.find('<', inOutCurrPos);
	if( inOutCurrPos == std::string::npos )
	{
		return false; //String has no more nodes avaliable!
	};

	size_t clipStart = inOutCurrPos+1;

	inOutCurrPos = m_fileBuffer.find('>', inOutCurrPos);
	if( inOutCurrPos == std::string::npos )
	{
		CCAssert( false, "The current node has no 'finish' node to match it!" );
		return false;
	};

	size_t clipLen = inOutCurrPos - clipStart;

	if( clipLen >= NODE_NAME_SIZE ) //Use 'equals to' as we cater for a NULL Terminator.
	{
		CCAssert( false, "An XML node name is too large!" );
		clipLen = NODE_NAME_SIZE-1;
	}

	if( !m_pActiveNode )
	{
		m_pActiveNode = new NodeXML();

		//Alloc the XML node head
		if( !m_pHeadNode )
		{
			m_pHeadNode = m_pActiveNode;
		}
		else
		{
			//We are adding a node at the highest level (it will be a SIBLING of the head node)
			IterateToEndSibling( m_pHeadNode )->pSibling = m_pActiveNode;
		}
	}
	else
	{
		NodeXML* pCurrParent = m_pActiveNode;
		m_pActiveNode = new NodeXML();

		m_pActiveNode->pParent = pCurrParent; //Update new nodes parent

		if( !pCurrParent->pChild )
		{
			pCurrParent->pChild = m_pActiveNode; //Update parent nodes child
		}
		else
		{
			//A child already exists for the parent, traverse the 'old' childs siblings until we find an empty slot!
			NodeXML* pChild = pCurrParent->pChild;

			while( pChild->pSibling )
			{
				pChild = pChild->pSibling;
			}

			pChild->pSibling = m_pActiveNode;
		}
	}

	m_fileBuffer.copy( m_pActiveNode->nodeName, clipLen, clipStart );
	m_pActiveNode->nodeName[clipLen] = 0; //NULL TERMINATOR


	inOutCurrPos++;
	return true;
}
//-----------------------------------------
bool
HasteFileXML::SkipToXmlInfo(size_t &inOutCurrPos)
{
	char currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos );

	if( currChar == -1 )
	{
		return false; //Nothing left
	}
	
	while( currChar == '\n' || currChar == '\t' || currChar == '\r' )
	{
		inOutCurrPos++;

		currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos );

		if( currChar == -1 )
		{
			return false; //Nothing left
		}
	}

	return true;
}
//-----------------------------------------
void
HasteFileXML::CollectNodeData(size_t &inOutCurrPos)
{
	RemoveWhiteSpace( inOutCurrPos );

	char currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos );
	
	if( currChar != '<' )
	{
		//If we have found a special term header (do we want a float an int or a string?)
		if( currChar == '{')
		{
			inOutCurrPos++;

			currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos );

			//If we want an int
			if( currChar == 'i' )
			{
				m_pActiveNode->dataType = XML_INT;
			
			}

			//If we want a float
			else if( currChar == 'f' )
			{
				m_pActiveNode->dataType = XML_FLOAT;
			}

			//No other data types known (strings default to not usign this) 
			else
			{
				CCAssert( false, "XML Requests an unknown data type!" );
			}

			inOutCurrPos += 2;
		}
		else
		{
			m_pActiveNode->dataType = XML_STRING; //default is string
		}

		//Extract the string and convert to the desired type
		size_t currLen = m_fileBuffer.find( '<', inOutCurrPos ) - inOutCurrPos;
		std::string extratedString( m_fileBuffer.substr(inOutCurrPos, currLen) );

		switch ( m_pActiveNode->dataType )
		{
			case XML_STRING:
				m_pActiveNode->data.pS = new std::string( extratedString );
				break;

			case XML_INT:
				StringExt::TypeConversion<std::string, int>( extratedString, m_pActiveNode->data.i );
				break;

			case XML_FLOAT:
				StringExt::TypeConversion<std::string, float>( extratedString, m_pActiveNode->data.f );
				break;
                
            default: break;
		}

		inOutCurrPos += currLen;
	}
	else
	{
		//There is NO Data avaliable to store! Reflect this!
		m_pActiveNode->data.i = 0;
	}
}
//-----------------------------------------
void
HasteFileXML::RemoveWhiteSpace(size_t &inOutCurrPos)
{
	char currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos );

	while( currChar == '\n' || currChar == '\t' || currChar == '\r' || currChar == ' ' )
	{
		inOutCurrPos++;
		currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos );
	}
}
//-----------------------------------------
bool
HasteFileXML::DetectNodeEnd(size_t &inOutCurrPos)
{
	RemoveWhiteSpace( inOutCurrPos );

	char currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos );

	if( currChar == '<' )
	{
		currChar = StringExt::CollectCharacter( m_fileBuffer, inOutCurrPos+1 );

		if( currChar == '/' )
		{
			inOutCurrPos += 2; //Set this to the position shown above +1

			size_t currLen = m_fileBuffer.find( '>', inOutCurrPos ) - inOutCurrPos;

			if( m_fileBuffer.substr(inOutCurrPos, currLen).compare(m_pActiveNode->nodeName) != 0 )
			{
				CCAssert( false, "The wrong XML 'end' header has appeared!" );
				return false;
			}
			
			inOutCurrPos += currLen + 1; //Move position along. +1 to remove the '>' as well

			//Move active access pointer
			m_pActiveNode = m_pActiveNode->pParent;

			return true;
		}
	}

	return false;
}
//-----------------------------------------
HasteFileXML::NodeXML*
HasteFileXML::IterateToEndSibling(NodeXML* startNode)
{
	while( startNode->pSibling )
	{
		startNode = startNode->pSibling;
	}

	return startNode;
}
//-----------------------------------------