#include "GameTileFactory.h"

#include "BaseGameTile.h"

#include "AllGameTile.h"
#include "StraightGameTile.h"
#include "TurnGameTile.h"

//Use the 'cocos2d' namespace (only do this within the .cpp!)
USING_NS_CC; 
//----------------------------------------------------
void gameTileFactoryAssertion()
{
	CCAssert(false, "Game Tile Factory Doesn't Handle Requested Tile ID");
}
//----------------------------------------------------
BaseGameTile* createGameTileFromFactory(TileType tileID, const CCPoint& pos)
{
    BaseGameTile* pBaseGameTile;

	switch (tileID)
	{
	case TILE_ALL:
		pBaseGameTile = AllGameTile::create( tileID, pos );
		break;

	case TILE_LEFT_RIGHT:
	case TILE_TOP_BOTTOM:
		pBaseGameTile = StraightGameTile::create( tileID, pos );
		break;

	case TILE_RIGHT_BOTTOM:
	case TILE_LEFT_TOP:
	case TILE_RIGHT_TOP:
	case TILE_LEFT_BOTTOM:
		pBaseGameTile = TurnGameTile::create( tileID, pos );
		break;

	default:
		gameTileFactoryAssertion();
	}

	return pBaseGameTile;
}