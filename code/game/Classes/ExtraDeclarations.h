#ifndef EXTRA_DECLARATIONS_H
#define EXTRA_DECLARATIONS_H

//CREATE_FUNC macro with one argument!
#define CREATE_FUNC_ONE_ARGU(__TYPE__, _ARGU_0_TYPE_, _ARGU_0_) \
static __TYPE__* create(_ARGU_0_TYPE_ _ARGU_0_) \
{ \
    __TYPE__ *pRet = new __TYPE__(_ARGU_0_); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = NULL; \
        return NULL; \
    } \
}

//CREATE_FUNC macro with one argument!
#define CREATE_FUNC_TWO_ARGU(__TYPE__, _ARGU_0_TYPE_, _ARGU_0_, _ARGU_1_TYPE_, _ARGU_1_) \
static __TYPE__* create(_ARGU_0_TYPE_ _ARGU_0_, _ARGU_1_TYPE_ _ARGU_1_) \
{ \
    __TYPE__ *pRet = new __TYPE__(_ARGU_0_, _ARGU_1_); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = NULL; \
        return NULL; \
    } \
}

#endif //EXTRA_DECLARATIONS_H