#include "HasteFileText.h"

#include "cocos2d.h"

#include <fstream>

USING_NS_CC;

//-----------------------------------------
HasteFileText::HasteFileText() :
	m_fileBuffer	( )
{
}
//-----------------------------------------
HasteFileText::~HasteFileText()
{
	VRemoveFileBuffer();
}
//-----------------------------------------
bool
HasteFileText::VFileExist(const char* fileName, bool bLoadFromWriteMemory)
{
	if( !bLoadFromWriteMemory )
	{
		return LoadFileFromReadOnly( fileName );
	}
	else
	{
		return LoadFileFromWrite( fileName );
	}
}
//-----------------------------------------
void
HasteFileText::VLoadFile(const char* fileName, bool bLoadFromWriteMemory)
{
	bool bResult = false;

	if( !bLoadFromWriteMemory )
	{
		//Load from core/packaged/main memory (non-editable)
		bResult = LoadFileFromReadOnly( fileName );
	}
	else
	{
		//Load memory generated from previous games
		bResult = LoadFileFromWrite( fileName );
	}

	if( !bResult )
	{
		CCAssert( false, "Haste File Text Read Failed! Check Filename!" );
	}
}
//-----------------------------------------
void
HasteFileText::VSaveFile(const char* fileName)
{
	std::string filePath = CCFileUtils::sharedFileUtils()->getWritablePath() + fileName; //FILE PATH / SD CARD / FILE NAME
	FILE* pFile = fopen(filePath.c_str(), "w"); //Open file for writting

	if( pFile )
	{
		fwrite(m_fileBuffer.c_str(), m_fileBuffer.size(), 1, pFile); //Write buffer to file
		fclose(pFile); //close file
	}
	else
	{
		//Create Message Box
		std::stringstream ss;
		ss << "Write File " << fileName << " Failed!";

		CCAssert( false, ss.str().c_str() );
	}
}
//-----------------------------------------
void
HasteFileText::AssignBuffer(const std::string &assignBuffer)
{
	m_fileBuffer = assignBuffer;
}
//-----------------------------------------
bool
HasteFileText::LoadFileFromReadOnly(const char* fileName)
{
	unsigned long bufferLen = 0;
	unsigned char* pBuffer = CCFileUtils::sharedFileUtils()->getFileData( fileName, "r", &bufferLen );
	
	if( bufferLen > 0 )
	{
		m_fileBuffer.assign( (const char*)pBuffer, bufferLen );

		//Destroy dynamic/heap buffer
		if( pBuffer ) 
		{
			delete []pBuffer;
			pBuffer = NULL;
		}

		return true;
	}
	
	m_fileBuffer.assign("");
	return false;
}
//-----------------------------------------
bool
HasteFileText::LoadFileFromWrite(const char* fileName)
{
	std::string filePath = CCFileUtils::sharedFileUtils()->getWritablePath() + fileName; //FILE PATH / SD CARD / FILE NAME
	FILE* pFile = fopen(filePath.c_str(), "r"); //Open file for writting

	if( pFile )
	{
		//Read file
		//http://www.cplusplus.com/reference/cstdio/fread/ Original source for reading code
	
		//Obtain file size:
		fseek( pFile , 0 , SEEK_END );
		long bufferSize = ftell( pFile );
		rewind( pFile );

		//Allocate memory to contain the whole file:
		char* pBuffer = new char[bufferSize+1];//static_cast<char*>( malloc(sizeof(char)*bufferSize) );
		pBuffer[bufferSize] = 0;

		//Copy the file into the buffer:
		size_t result = fread( pBuffer, 1, bufferSize, pFile );

		//Assign buffer to member buffer
		m_fileBuffer.assign( pBuffer );

		//Clean up
		fclose( pFile );
		delete( pBuffer );

		return true;
	}

	return false;
}
//-----------------------------------------
void
HasteFileText::VRemoveFileBuffer()
{
	m_fileBuffer.clear();
}
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------
//-----------------------------------------