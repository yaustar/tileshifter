#ifndef FAST_LABEL_TTF_MANAGER_H
#define FAST_LABEL_TTF_MANAGER_H

#include "cocos2d.h"

#include "FastLabelTTFTypes.h"

class FastLabelTTFCache;
class FastLabelTTFManager : public cocos2d::CCLayer
{
public:
	FastLabelTTFManager();
	~FastLabelTTFManager();

	static FastLabelTTFManager* create();
	virtual bool init(); //Overloaded Init.

	//Singleton access
	static FastLabelTTFManager* sharedManager();
	
	//Public Methods ----- 
	const FastLabelTTFCache* getCharacterCache(FastLabelTTFTypes fontType);

private:

	//Private Structs -----
	struct CharCacheInfo
	{
		CharCacheInfo();

		FastLabelTTFCache* m_pCharCache;
		bool m_bUsed;
	};

	//Private Variables ----
	typedef std::vector<CharCacheInfo> CharCacheArray;

	CharCacheArray m_charCaches;
};
#endif //FAST_LABEL_TTF_MANAGER_H