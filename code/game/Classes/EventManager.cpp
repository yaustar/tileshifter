#include "EventManager.h"

#include "GameCoreData.h" //Needed for TS_UPDATE_PRIORITY_EVENT_MANAGER

#include "EventMessage.h"
#include "EventListener.h"

EventManager* EventManager::ms_sharedInstance = NULL;

//----------------------------------------------------
EventManager::EventManager() :
	m_eventListenerList		( TS_EVENT_ID_COUNT ),
	m_eventQueue			(  )
{
	if( ms_sharedInstance == NULL )
	{
		ms_sharedInstance = this;
	}
	else
	{
		CCAssert( false, "The Event Manager Singleton Has Been Created Twice!" );
	}

	for(EventListenerList::iterator iter = m_eventListenerList.begin(); iter != m_eventListenerList.end(); iter++)
	{
		(*iter).retainNodeObject( false ); //We don't want the listener to do this!
	}
}
//----------------------------------------------------
EventManager::~EventManager()
{
	if( ms_sharedInstance == this )
	{
		ms_sharedInstance = NULL;
	}
	else
	{
		CCAssert( false, "The Event Manager Singleton Has Been Created Twice!" );
	}
}
//----------------------------------------------------
EventManager*
EventManager::sharedEventManager(bool bPerformNullTest)
{
	if( bPerformNullTest )
	{
		if( ms_sharedInstance )
		{
			return ms_sharedInstance;
		}

		CCAssert( false, "The Event Manager Singleton Has NOT Been Created!" );
	}

	return ms_sharedInstance; //This may return null
}
//----------------------------------------------------
void
EventManager::QueueEvent(EventMemberLinkedNode* pLinkedListNode)
{
	m_eventQueue.push_back( pLinkedListNode );
}
//----------------------------------------------------
void
EventManager::registerListener(EventListenerLinkedNode* pLinkedListNode, TileshifterEventId eventId)
{
	m_eventListenerList[eventId].push_back( pLinkedListNode );
}
//----------------------------------------------------
void
EventManager::unRegisterListener(EventListenerLinkedNode* pLinkedListNode, TileshifterEventId eventId)
{
	EventListenerLinkedNode* pEventListenerNode = m_eventListenerList[eventId].front();

	for(unsigned int i = 0; i < m_eventListenerList[eventId].getSize(); i++)
	{
		if( pEventListenerNode == pLinkedListNode )
		{
			m_eventListenerList[eventId].remove( pEventListenerNode );
			pEventListenerNode = NULL;
			break;
		}

		pEventListenerNode = m_eventListenerList[eventId].accessNext( pEventListenerNode );
	}

	if( pEventListenerNode )
	{
		CCAssert( false, "The Event Listener Has Failed To Unregister! We Can NOT Find It!" );
	}
}
//----------------------------------------------------
void
EventManager::update(float delta)
{
	EventMemberLinkedNode* pCurrNode = m_eventQueue.front();
	
	while( pCurrNode != NULL )
	{
		EventMessage* pEventMessage = pCurrNode->GetLinkedObject();

		EventListenerMemberLinkedList &eventListenerList = m_eventListenerList[pEventMessage->getEventId()];
		EventListenerLinkedNode* pListenerListNode = eventListenerList.front();

		while( pListenerListNode != NULL )
		{
			pListenerListNode->GetLinkedObject()->runListenerCallback( pEventMessage );
			pListenerListNode = eventListenerList.accessNext( pListenerListNode );
		}

		m_eventQueue.pop_front();
		pCurrNode = m_eventQueue.front();
	}
}
//----------------------------------------------------
void
EventManager::sceneTransitionComplete()
{
	//The 'update methods schdule gets removed on switching states (this is added to the active state), so reactivate here!
	this->scheduleUpdateWithPriority( TS_UPDATE_PRIORITY_EVENT_MANAGER );

	//Clear the event queue, prevents events from a different state occruing.
	//while( m_eventQueue.getSize() != 0 )
	//{
	//	m_eventQueue.pop_back();
	//}
}
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------