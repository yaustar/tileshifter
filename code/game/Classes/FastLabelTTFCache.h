#ifndef FAST_LABEL_TTF_CACHE_H
#define FAST_LABEL_TTF_CACHE_H

#include "cocos2d.h"

class FastLabelTTFCache : public cocos2d::CCLayer
{
public:
	FastLabelTTFCache(const std::string &fontName, float fontSize);
	~FastLabelTTFCache();

	static FastLabelTTFCache* create(const std::string &fontName, float fontSize);
	virtual bool init(); //Overloaded Init.

	//Public Methods ----- 
	cocos2d::CCTexture2D* getCharacterTexture(char requestedChar) const;

private:

	//Private Methods -----
	void generateCharacterTextures(const std::string &fontName, float fontSize);

	//Private Variables ----
	typedef std::vector<cocos2d::CCTexture2D*> TextureArray;
	TextureArray m_charTextures;
};
#endif //FAST_LABEL_TTF_CACHE_H