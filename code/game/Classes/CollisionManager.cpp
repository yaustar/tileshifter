#include "CollisionManager.h"

#include "CharacterObject.h"
#include "CoinObject.h"

USING_NS_CC;

//----------------------------------------------------
CollisionManager::CollisionManager(CharacterObject* pCharacter, CharacterObject* pGhost, CoinObject* pCoin) :
	m_pCharacter	( pCharacter ),
	m_pGhost		( pGhost),
	m_pCoin			( pCoin )
{

}
//----------------------------------------------------
bool
CollisionManager::init()
{
	// Super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

	this->schedule( schedule_selector(CollisionManager::onCollisionCallBack), 0.0f );

	return true;
}
//----------------------------------------------------
void
CollisionManager::onExit()
{
}
//----------------------------------------------------
CollisionManager*
CollisionManager::create(CharacterObject* pCharacter, CharacterObject* pGhost, CoinObject* pCoin)
{
	CollisionManager *pRet = new CollisionManager(pCharacter, pGhost, pCoin);
	if (pRet && pRet->init())
	{
		pRet->autorelease();
	}
	else
	{
		delete pRet;
		pRet = NULL;
	}
    
    return pRet;
}
//----------------------------------------------------
void
CollisionManager::onCollisionCallBack(float delta)
{
	//Coin Vs. Character 
	if( m_pCoin->testForCollisionWithCharacter(m_pCharacter, true) )
	{
		m_pCharacter->onCollisionWithCoin();
	}

	//Coin Vs. Ghost 
	if( m_pCoin->testForCollisionWithCharacter(m_pGhost, false) )
	{
		//We don't want the ghost to react in any way (the commented code woudl speed it up)
		//m_pGhost->onCollisionWithCoin();
	}

	//Character Vs. Ghost
	if( m_pCharacter->testForGhostCollision(m_pGhost) )
	{

	}
}
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------