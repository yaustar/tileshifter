#ifndef EVENT_GAME_TIMER_EXPIRED_H
#define EVENT_GAME_TIMER_EXPIRED_H

#include "EventMessage.h"

class EventGameTimerExpired : public EventMessage
{
public:
	EventGameTimerExpired() :
		EventMessage	( TS_EVENT_ID_GAME_TIMER_EXPIRED )
	{

	}

	CREATE_FUNC( EventGameTimerExpired );

private:
};
#endif //EVENT_GAME_TIMER_EXPIRED_H