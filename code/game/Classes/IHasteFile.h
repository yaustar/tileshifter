#ifndef I_HASTE_FILE_H
#define I_HASTE_FILE_H


class IHasteFile
{
public:
	IHasteFile() {}
	virtual ~IHasteFile() {};

	virtual bool VFileExist(const char* fileName, bool bLoadFromWriteMemory) =0;

	virtual void VLoadFile(const char* fileName, bool bLoadFromWriteMemory) =0;
	virtual void VSaveFile(const char* fileName) =0;

protected:
	virtual void VRemoveFileBuffer() =0;
};
#endif //I_HASTE_FILE_H