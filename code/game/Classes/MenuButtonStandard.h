#ifndef MENU_BUTTON_STANDARD_H
#define MENU_BUTTON_STANDARD_H

#include "cocos2d.h"
#include "ExtraDeclarations.h"

class MenuButtonStandard : public cocos2d::CCLayer
{
public:
	MenuButtonStandard(cocos2d::CCCallFunc* pPressCallback);

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
	virtual void onExit();
    
    // implement the "static node()" method manually
    CREATE_FUNC_ONE_ARGU(MenuButtonStandard, cocos2d::CCCallFunc*, pPressCallback);

	void setString(const char* pName);

	cocos2d::CCSize getSize();

protected:
	virtual std::string getStandardSpriteName();
	virtual std::string getSelectedSpriteName();

private:

	//Private Methods ---
	void menuButtonCallback(CCObject* pSender);
	

	//Private Variables ----
	cocos2d::CCCallFunc* m_pPressCallback;
	
};
#endif //MENU_BUTTON_STANDARD_H