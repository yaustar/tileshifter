#include "EventMessage.h"

#include "EventManager.h"
//----------------------------------------------------
EventMessage::EventMessage(TileshifterEventId eventId) :
	m_eventListNode		( this ),
	m_eventId			( eventId )
{
	QueueEvent();
}
//----------------------------------------------------
EventMessage::~EventMessage()
{
}
//----------------------------------------------------
EventMessage*
EventMessage::create()
{
	CCAssert( false, "The Event Message Has NOT Provided A Valid Create Method!" );
	return NULL;
}
//----------------------------------------------------
TileshifterEventId
EventMessage::getEventId()
{
	return m_eventId;
}
//----------------------------------------------------
void
EventMessage::QueueEvent()
{
	EventManager::sharedEventManager()->QueueEvent( &m_eventListNode );
}
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------