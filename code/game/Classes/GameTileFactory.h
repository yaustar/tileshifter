#ifndef GAME_TILE_FACTORY
#define GAME_TILE_FACTORY

#include "cocos2d.h"

#include "TileType.h"
class BaseGameTile;

BaseGameTile* createGameTileFromFactory(TileType tileID, const cocos2d::CCPoint& pos );

#endif //GAME_TILE_FACTORY