#include "SoundManager.h"

//Audio --
#include "SimpleAudioEngine.h"

//Event System --
#include "EventRequestSoundEffectPlay.h"
#include "EventListener.h"

//File Parser ---
#include "HasteFileXML.h"


USING_NS_CC;

SoundManager* SoundManager::ms_sharedInstance = NULL;

//------------------------------------------
SoundManager::SoundManager() :
	m_soundFilenames	( SOUND_TYPE_COUNT, "Sound/" ),
	m_bAllowSoundFx		( true ),
	m_bAllowMusic		( false )
{
	if( ms_sharedInstance == NULL )
	{
		ms_sharedInstance = this;
	}
	else
	{
		CCAssert( false, "The Event Manager Singleton Has Been Created Twice!" );
	}
}
//------------------------------------------
SoundManager::~SoundManager()
{
}
//------------------------------------------
SoundManager*
SoundManager::sharedSoundManager(bool bPerformNullTest)
{
	if( bPerformNullTest )
	{
		if( ms_sharedInstance )
		{
			return ms_sharedInstance;
		}

		CCAssert( false, "The Sound Manager Singleton Has NOT Been Created!" );
	}

	return ms_sharedInstance; //This may return null
}
//------------------------------------------
bool
SoundManager::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	//Parse XML file
	HasteFileXML file;
	file.VLoadFile("Sound/SoundList.xml");

	if( file.FindAndAccessNode("soundList") )
	{
		if( file.FindAndAccessNode("soundDecl") )
		{
			do
			{
				if( file.FindAndAccessNode("fileName") )
				{
					std::string* pFilename = file.GetNodeData().pS;

					while( file.FindAndAccessNodeSibling("commandName") )
					{
						ApplySoundCommand( file.GetNodeData().pS, pFilename );
					}

					file.MoveToParent();
				}

			} while( file.FindAndAccessNodeSibling("soundDecl") );
		}
	}

	//Pre load sound effects!
	CocosDenshion::SimpleAudioEngine* pAudioEngine = CocosDenshion::SimpleAudioEngine::sharedEngine();

	for(int i = 0; i < SOUND_MUSIC_BG; i++)
	{
		pAudioEngine->preloadEffect( m_soundFilenames[i].c_str() );
	}

	//Generate listener callbacks
	this->addChild( EventListener::create(this, callfuncO_selector(SoundManager::callBackPlayEffect), TS_EVENT_ID_SOUND_EFFECT_PLAY) );
	
	return true;
}
//------------------------------------------
void
SoundManager::onExit()
{
	
}
//------------------------------------------
void
SoundManager::allowSoundFxPlayBack(bool bAllowSoundFx)
{
	m_bAllowSoundFx = bAllowSoundFx;
}
//------------------------------------------
void
SoundManager::allowMusicPlayBack(bool bAllowMusic)
{
	m_bAllowMusic = bAllowMusic;
}
//------------------------------------------
void
SoundManager::sceneTransitionComplete(SceneType sceneType)
{
	SoundType musicType = SOUND_TYPE_NONE;

    if( m_bAllowMusic )
	{
		switch( sceneType )
		{
		case SCENE_GAME:
			musicType = SOUND_MUSIC_BG;
			break;
		default: break;
		}
	}

	CocosDenshion::SimpleAudioEngine* pAudioEngine = CocosDenshion::SimpleAudioEngine::sharedEngine();

	if( musicType != SOUND_TYPE_NONE )
	{
		pAudioEngine->playBackgroundMusic( m_soundFilenames[musicType].c_str(), true );
	}
	else
	{
		pAudioEngine->stopBackgroundMusic();
	}
}
//------------------------------------------
void
SoundManager::callBackPlayEffect(CCObject* pEventObject)
{
	if( m_bAllowSoundFx )
	{
		//Play sound effect
		EventRequestSoundEffectPlay* pEvent = static_cast<EventRequestSoundEffectPlay*>( pEventObject );
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect( m_soundFilenames[pEvent->GetSoundEffect()].c_str() );
	}
}
//------------------------------------------
bool
SoundManager::isSoundFxAllowed()
{
	return m_bAllowSoundFx;
}
//------------------------------------------
bool
SoundManager::isMusicAllowed()
{
	return m_bAllowMusic;
}
//------------------------------------------
void
SoundManager::ApplySoundCommand(const std::string* pCommandName, const std::string* pFilename)
{
	if( pCommandName->compare("CollectCoin") == 0 )
	{
		ApplySoundCommand( pFilename, SOUND_EFFECT_COLLECT_COIN );
	}
	else if( pCommandName->compare("NegativeCollectCoin") == 0 )
	{
		ApplySoundCommand( pFilename, SOUND_EFFECT_NEG_COLLECT_COIN );
	}
	else if( pCommandName->compare("PressKey") == 0 )
	{
		//ApplySoundCommand( pFilename, SOUND_EFFECT_COLLECT_COIN );
	}
	else if( pCommandName->compare("MusicBg") == 0 )
	{
		ApplySoundCommand( pFilename, SOUND_MUSIC_BG );
	}
	else
	{
		CCAssert( false, "Command Name Not Handled!" );
	}
}
//------------------------------------------
void
SoundManager::ApplySoundCommand(const std::string* pFilename, SoundType soundType)
{
	if( m_soundFilenames[soundType].compare("Sound/") == 0 )
	{
		m_soundFilenames[soundType].append( *pFilename );
	}
	else
	{
		CCAssert( false, "Sound Manager Already Allocated This Command!" );
	}
}
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------