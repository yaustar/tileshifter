#ifndef GAME_CORE_DATA_H
#define GAME_CORE_DATA_H

#include "cocos2d.h"

enum CoreResourceNameType
{
	RESOURCE_NAME_LOGO,
	RESOURCE_NAME_TITLE,
	RESOURCE_NAME_TILE,
	RESOURCE_NAME_CHARACTER,
	RESOURCE_NAME_GHOST,
	RESOURCE_NAME_COIN,
	RESOURCE_NAME_TIMER_PULSE,
	RESOURCE_NAME_MENU_BUTTON_STAND,
	RESOURCE_NAME_MENU_BUTTON_SELECT,
	RESOURCE_NAME_MENU_SELECTOR_LEFT,
	RESOURCE_NAME_MENU_SELECTOR_CENTER,
	RESOURCE_NAME_MENU_SELECTOR_RIGHT,
	RESOURCE_NAME_MENU_OPTIONS_STAND,
	RESOURCE_NAME_MENU_OPTIONS_SELECT,
	RESOURCE_NAME_GO_SEQ_FINAL,
	RESOURCE_NAME_GO_SEQ_3,
	RESOURCE_NAME_GO_SEQ_2,
	RESOURCE_NAME_GO_SEQ_1,

	//Auto Gen ---
	RESOURCE_NAME_COUNT
};

struct CoreTileData
{
	CoreTileData() { };

	cocos2d::CCSize tileSize;
	cocos2d::CCSize tileSizeHalf;

	cocos2d::CCSize gridNodeSize;
	cocos2d::CCSize gridSize;

	cocos2d::CCRect gridBound;
};

struct CoreScoreData
{
	CoreScoreData( unsigned int scoreIncrementer ) : 
	score				( 0 ),
	SCORE_INCREMENTER	( scoreIncrementer )
	{ };

	unsigned int		score;
	const unsigned int	SCORE_INCREMENTER;
};

enum CoreUpdatePriorityData
{
	TS_UPDATE_PRIORITY_EVENT_MANAGER = 1000
};

typedef std::vector<std::string> ResourceNames;

class GameCoreData
{
public:
	static void init();

	static const std::string& getResourcesName(CoreResourceNameType resourceType);
	static const CoreTileData& getTileData();
	static const CoreScoreData& getScoreData();

	static void incrementScore(float scoreMultiplier);
	static void resetScore();

	static void setGamePaused(bool setTo);
	static bool isGamePaused();

private:
	
	//Private Methods -----
	static void assignResourceDirectory(const char* dirName);
	static void assignResourceNames();

	static cocos2d::CCSize generateDesignResolution(cocos2d::CCSize physicalScreenRes);

	//Private Variables ------
	static ResourceNames			ms_coreResourceNames;
	static CoreTileData				ms_coreTileData;
	static CoreScoreData			ms_coreScoreData;
	//static CoreUpdatePriorityData	ms_coreUpdatePriority;
	static bool						ms_bGamePaused;
};
#endif //GAME_CORE_DATA_H