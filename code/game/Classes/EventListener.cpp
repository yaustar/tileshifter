#include "EventListener.h"

#include "EventManager.h"

USING_NS_CC;

//----------------------------------------------------
EventListener::EventListener(CCObject* pCallBackObject, SEL_CallFuncO callBackMethod, TileshifterEventId eventID) :
	m_callBackMethod	( callBackMethod ),
	m_pCallBackObject	( pCallBackObject ),
	
	m_eventListNode		( this ),
	m_eventId			( eventID )
{
	//Register the listener with the manager!
	EventManager::sharedEventManager()->registerListener( &m_eventListNode, eventID );
}
//----------------------------------------------------
EventListener*
EventListener::create(CCObject* pCallBackObject, SEL_CallFuncO callBackMethod, TileshifterEventId eventID)
{
	//Copied from 'CREATE_FUNC', modified to cater for additional parameters.
	 EventListener *pRet = new EventListener(pCallBackObject, callBackMethod, eventID); 
    if (pRet && pRet->init()) 
    { 
        pRet->autorelease(); 
        return pRet; 
    } 
    else 
    { 
        delete pRet; 
        pRet = NULL; 
        return NULL; 
    }
}
//----------------------------------------------------
bool
EventListener::init()
{
	//--------------------------------------------------------
    // Super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

	return true;
}
//----------------------------------------------------
EventListener::~EventListener()
{
	//Do not perform the NULL test, the Event Manager may destroy this object during its destruction, so it may legitimately not exist!
	EventManager* pEventManager = EventManager::sharedEventManager( false ); 
	if( pEventManager )
	{
		pEventManager->unRegisterListener( &m_eventListNode, m_eventId );
	}
}
//----------------------------------------------------
void
EventListener::runListenerCallback(CCObject* pEventObject)
{
	//Run the callback (passing the event message as an object still!)
	runAction(CCCallFuncO::create(m_pCallBackObject, m_callBackMethod, pEventObject));
}
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------