#ifndef COLLISION_MANAGER_H
#define COLLISION_MANAGER_H

#include "cocos2d.h"

class CharacterObject;
class CoinObject;
class CollisionManager : public cocos2d::CCLayer
{
public:

	CollisionManager(CharacterObject* pCharacter = NULL, CharacterObject* pGhost = NULL, CoinObject* pCoin = NULL);

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
	virtual void onExit();

	static CollisionManager* create(CharacterObject* pCharacter, CharacterObject* pGhost, CoinObject* pCoin);

private:

	void onCollisionCallBack(float delta);

	CharacterObject* m_pCharacter;
	CharacterObject* m_pGhost;
	CoinObject* m_pCoin;
};
#endif //COLLISION_MANAGER_H

