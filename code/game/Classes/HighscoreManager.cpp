#include "HighscoreManager.h"

#include <sstream>
#include "HasteFileXML.h"

USING_NS_CC;

const int NUM_HIGH_SCORES = 10;
const char* DEBUG_SCORE_WRITE_FILENAME = "SaveData-Scores.xml";
const char* DEBUG_SCORE_FILENAME = "Data/Scores.xml";

HighscoreManager* HighscoreManager::ms_sharedInstance = NULL;

//------------------------------------------
//Highscore Data Struct
//------------------------------------------
HighscoreData::HighscoreData(const char* initials, int score) :
	m_initals	( initials ),
	m_score		( score )
{
}
//------------------------------------------
//Highscore Manager
//------------------------------------------
HighscoreManager::HighscoreManager() :
	m_scores			( ),
	m_latestHighscoreId	( -1 )
{
	m_scores.reserve( NUM_HIGH_SCORES );

	if( ms_sharedInstance == NULL )
	{
		ms_sharedInstance = this;
	}
	else
	{
		CCAssert( false, "The Event Manager Singleton Has Been Created Twice!" );
	}
}
//------------------------------------------
HighscoreManager::~HighscoreManager()
{
	//CONSIDER REFACTORING! -- Instead extend the 'Haste File XML' to automatically output its internal data? (May require its elements be updated with the new info)
	HasteFileXML xmlFile;
	std::string xmlBuffer;

	xmlBuffer.assign( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n" );
	xmlBuffer.append( "<Highscores>" );

	for(Highscores::iterator iter = m_scores.begin(); iter != m_scores.end(); iter++)
	{
		xmlBuffer.append( "<Score>" );

			xmlBuffer.append( "<Name>" );
				xmlBuffer.append( iter->m_initals );
			xmlBuffer.append( "</Name>" );

			xmlBuffer.append( "<Value>" );
				xmlBuffer.append( "{i}" );
				std::stringstream ss;
				ss << iter->m_score;
				xmlBuffer.append( ss.str() );
			xmlBuffer.append( "</Value>" );

		xmlBuffer.append( "</Score>" );
	}



	xmlBuffer.append( "</Highscores>" );

	xmlFile.AssignBuffer( xmlBuffer );
	xmlFile.VSaveFile( DEBUG_SCORE_WRITE_FILENAME );
}
//------------------------------------------
HighscoreManager*
HighscoreManager::sharedHighscoreManager(bool bPerformNullTest)
{
	if( bPerformNullTest )
	{
		if( ms_sharedInstance )
		{
			return ms_sharedInstance;
		}

		CCAssert( false, "The Sound Manager Singleton Has NOT Been Created!" );
	}

	return ms_sharedInstance; //This may return null
}
//------------------------------------------
bool
HighscoreManager::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	HasteFileXML xmlFile;

	if( xmlFile.VFileExist(DEBUG_SCORE_WRITE_FILENAME, true) )
	{
		xmlFile.VLoadFile( DEBUG_SCORE_WRITE_FILENAME, true );
	}
	else
	{
		xmlFile.VLoadFile( DEBUG_SCORE_FILENAME, false );
	}

	if( xmlFile.FindAndAccessNode("Highscores") )
	{
		if( xmlFile.FindAndAccessNode("Score") )
		{
			ParseXmlScore( &xmlFile );
		}

		while( xmlFile.FindAndAccessNodeSibling("Score") )
		{
			ParseXmlScore( &xmlFile );
		}
	}

	return true;
}
//------------------------------------------
void
HighscoreManager::onExit()
{
	
}
//------------------------------------------
void
HighscoreManager::sceneTransitionComplete(SceneType sceneType)
{
}
//------------------------------------------
void
HighscoreManager::resetLatestHighscoreId()
{
	m_latestHighscoreId = -1;
}
//------------------------------------------
std::string
HighscoreManager::getHighScore(int index)
{
	std::stringstream ss;
	ss << m_scores[index].m_score;
	return ss.str();
}
//------------------------------------------
const std::string&
HighscoreManager::getHighScoreName(int index)
{
	return m_scores[index].m_initals;
}
//------------------------------------------
std::string
HighscoreManager::getHighestScore()
{
	if( !m_scores.empty() )
	{
		std::stringstream ss;
		ss << m_scores[0].m_score;
		return ss.str();
	}
	else
	{
		return "0";
	}
}
//------------------------------------------
int
HighscoreManager::getLatestHighscoreId()
{
	return m_latestHighscoreId;
}
//------------------------------------------
int
HighscoreManager::getNumActiveScores()
{
	return static_cast<int>( m_scores.size() );
}
//------------------------------------------
bool
HighscoreManager::isScoreHigh(int score)
{
	if( score > 0.0f )
	{
		if( m_scores.size() >= m_scores.capacity() )
		{
			for(Highscores::iterator iter = m_scores.begin(); iter != m_scores.end(); iter++)
			{
				if( score > (*iter).m_score )
				{
					return true;
				}
			}
		}
		else
		{
			return true;
		}
	}

	return false;
}
//------------------------------------------
bool
HighscoreManager::assignHighScore(const char* initials, int score)
{
	{
		int counter = 0;

		for(Highscores::iterator iter = m_scores.begin(); iter != m_scores.end(); iter++)
		{
			if( score > (*iter).m_score )
			{
				m_latestHighscoreId = counter;

				if( m_scores.size() >= m_scores.capacity() )
				{
					//Remove bottom one	
					m_scores.pop_back();
				}
				
				//Add new one
				m_scores.insert( iter, HighscoreData(initials, score) );

				return true;
			}

			counter++;
		}
	}
	
	if( m_scores.size() < m_scores.capacity() )
	{
		m_latestHighscoreId = static_cast<int>( m_scores.size() );
		m_scores.push_back( HighscoreData(initials, score) );
		
		return true;
	}
	
	return false;
}
//------------------------------------------
void
HighscoreManager::ParseXmlScore(HasteFileXML* pXmlFile)
{
	pXmlFile->FindAndAccessNode("Name");
	std::string* pName = pXmlFile->GetNodeData().pS;

	pXmlFile->FindAndAccessNodeSibling("Value");
	pXmlFile->GetNodeData().i;

	m_scores.push_back( HighscoreData(pName->c_str(), pXmlFile->GetNodeData().i) );

	//Reset the XML accessor
	pXmlFile->MoveToParent();
}
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------