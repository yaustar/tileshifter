#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include "cocos2d.h"

#include <vector>
#include <string>

#include "SoundTypes.h"
#include "SceneTypes.h"

class SoundManager : public cocos2d::CCLayer
{
public:
	SoundManager();
	~SoundManager();

	//Collect a singleton!
	static SoundManager* sharedSoundManager(bool bPerformNullTest = true);

	CREATE_FUNC( SoundManager );

	virtual bool init(); //Overloaded Init.
	virtual void onExit();

	void allowSoundFxPlayBack(bool bAllowSoundFx);
	void allowMusicPlayBack(bool bAllowMusic);

	void sceneTransitionComplete(SceneType sceneType);

	void callBackPlayEffect(CCObject* pEventObject);

	bool isSoundFxAllowed();
	bool isMusicAllowed();

private:

	//Private Methods ----
	void ApplySoundCommand(const std::string* pCommandName, const std::string* pFilename);
	void ApplySoundCommand(const std::string* pFilename, SoundType soundType);

	//Private Variables ----
	typedef std::vector<std::string> SoundArray;
	SoundArray m_soundFilenames;

	bool m_bAllowSoundFx;
	bool m_bAllowMusic;

	//Static Private Variables ---- 
	static SoundManager* ms_sharedInstance;
};
#endif //SOUND_MANAGER_H