#ifndef EVENT_GAME_OVER_H
#define EVENT_GAME_OVER_H

#include "EventMessage.h"


class EventGameOver : public EventMessage
{
public:
	EventGameOver() :
		EventMessage( TS_EVENT_ID_GAME_OVER )
	{

	}

	CREATE_FUNC( EventGameOver );

private:

};
#endif //EVENT_GAME_OVER_H