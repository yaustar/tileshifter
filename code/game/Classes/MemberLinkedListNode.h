#ifndef MEMBER_LINKED_LIST_NODE__H
#define MEMBER_LINKED_LIST_NODE__H

//This class is designed to provide a linked list interface that can be contained within a class or struct.
//The advantage is that we get the benfits of a linked list while minimising fragmentation drawbacks 

#include "cocos2d.h"

//Template EXPECTS a POINTER type!!!!
template <class T>
class MemberLinkedListNode : public cocos2d::CCLayer
{
public:

	//------------------------------------------------------------------
	MemberLinkedListNode(T pMemberObject) :
	m_pNextNode		( NULL ),
	m_pPrevNode		( NULL ),
	m_pLinkedObject	( pMemberObject )
	{

	}

	//------------------------------------------------------------------
	~MemberLinkedListNode()
	{
		removeNode();

		//Only NULL the object here! (this is the only time is guarnteed to be done with!)
		m_pLinkedObject = NULL;		
	}

	//------------------------------------------------------------------
	static MemberLinkedListNode* create(T pMemberObject)
	{
		//Copied from 'CREATE_FUNC', modified to cater for additional parameters.
		 MemberLinkedListNode *pRet = new MemberLinkedListNode(pMemberObject); 
		if (pRet && pRet->init()) 
		{ 
			pRet->autorelease(); 
			return pRet; 
		} 
		else 
		{ 
			delete pRet; 
			pRet = NULL; 
			return NULL; 
		}
	}

	//------------------------------------------------------------------
	virtual bool init() //Overloaded Init.
	{
		//--------------------------------------------------------
		// Super init first
		if ( !CCLayer::init() )
		{
			return false;
		}

		return true;
	}

	//------------------------------------------------------------------
	T GetLinkedObject()
	{
		//Return the stored object (could be an event message/event listener etc.)
		return m_pLinkedObject;
	}


private:
	//------------------------------------------------------------------
	//Private Methods 
	//------------------------------------------------------------------
	void removeNode()
	{
		//Cache the next node as we may change it before the prev node can be updated!
		MemberLinkedListNode* pCacheNext = m_pNextNode;

		if( m_pNextNode )
		{
			m_pNextNode->setPrev( m_pPrevNode );
			m_pNextNode = NULL;
		}

		if( m_pPrevNode )
		{
			m_pPrevNode->setNext( pCacheNext );
			m_pPrevNode = NULL;
		}

		//Do NOT 'NULL' the pointer to the object here, we've not destroyed it, just wish to remove the linked list!
	}

	//------------------------------------------------------------------
	void insertNext(MemberLinkedListNode* pInsertNode)
	{
		if( pInsertNode->getPrev() != NULL )
		{
			CCAssert( false, "Member Linked List Node 'insertNext' is already inserted!" );
			return;
		}
	
		if( m_pNextNode )
		{
			if( pInsertNode->getNext() != NULL )
			{
				CCAssert( false, "Member Linked List Node 'insertNext' is already inserted!" );
				return;
			}

			m_pNextNode->setPrev( pInsertNode );
			pInsertNode->setNext( m_pNextNode );
		}
	
		setNext( pInsertNode );
		pInsertNode->setPrev( this );
	}

	//------------------------------------------------------------------
	void insertPrev(MemberLinkedListNode* pInsertNode)
	{
		//Reverse the call, allows us to reuse existing code.
		MemberLinkedListNode* pThisPrevNode = this->getPrev();
		if( pThisPrevNode )
		{
			pThisPrevNode->insertNext( pInsertNode );
		}
		else
		{
			//Should be safe to reverse the method call entirely
			pInsertNode->insertNext( this );
		}
	}

	//------------------------------------------------------------------
	void setNext(MemberLinkedListNode* pNextNode)
	{
		m_pNextNode = pNextNode;
	}

	//------------------------------------------------------------------
	void setPrev(MemberLinkedListNode* pPrevNode)
	{
		m_pPrevNode = pPrevNode;
	}

	//------------------------------------------------------------------
	MemberLinkedListNode* getNext()
	{
		return m_pNextNode;
	}

	//------------------------------------------------------------------
	MemberLinkedListNode* getPrev()
	{
		return m_pPrevNode;
	}

	//------------------------------------------------------------------
	//Private Variables 
	//------------------------------------------------------------------
	MemberLinkedListNode* m_pNextNode;
	MemberLinkedListNode* m_pPrevNode;

	T m_pLinkedObject;

	//------------------------------------------------------------------
	//Friend Classes
	//------------------------------------------------------------------
	template <class U>
	friend class MemberLinkedList;
};
#endif //MEMBER_LINKED_LIST_NODE__H