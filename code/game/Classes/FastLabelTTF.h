#ifndef FAST_LABEL_TTF_H
#define FAST_LABEL_TTF_H

#include "cocos2d.h"

#include "FastLabelTTFTypes.h"

class FastLabelTTFCache;
class FastLabelTTF : public cocos2d::CCSprite //This will be an EMPTY sprite that provides sprite children!
{
public:
	FastLabelTTF(FastLabelTTFTypes fontType, const cocos2d::CCSize &dimensions, cocos2d::CCTextAlignment horTextAlignment = cocos2d::kCCTextAlignmentCenter, cocos2d::CCVerticalTextAlignment verTextAlignment = cocos2d::kCCVerticalTextAlignmentCenter);
	~FastLabelTTF();

	static FastLabelTTF* create(FastLabelTTFTypes fontType, const cocos2d::CCSize &dimensions, cocos2d::CCTextAlignment horTextAlignment = cocos2d::kCCTextAlignmentCenter, cocos2d::CCVerticalTextAlignment verTextAlignment = cocos2d::kCCVerticalTextAlignmentCenter);
	virtual bool init(); //Overloaded Init.

	//Overload, allowing children to apply opacity (allows fade actions to work)
	virtual void setOpacity(GLubyte opacity);

	//Public Methods ----- 
	void setString(const char* string);
	void setColor(const cocos2d::ccColor3B &colour);

	const cocos2d::CCSize& getSize();

private:

	//Private Methods -----
	void applyTextAlignment(cocos2d::CCRect &textRect);
	float textAlignmentLeftOrBottomPosition(float currPos, float textBoxDimension, float textRectMin);
	float textAlignmentCentrePosition(float currPos, float textRectMid);
	float textAlignmentRightOrTopPosition(float currPos, float textBoxDimension, float textRectMax);

	//Private Variables ----
	//cocos2d::ccColor3B m_colour;
	cocos2d::CCSize m_textBoxDimensions;
	
	cocos2d::CCSize m_stringSize;

	cocos2d::CCTextAlignment m_horTextAlign;
	cocos2d::CCVerticalTextAlignment m_verTextAlign;

	const FastLabelTTFCache* m_pCharacterCache;
	
};
#endif //FAST_LABEL_TTF_H