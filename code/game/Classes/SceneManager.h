#ifndef SCENE_MANAGER_H_
#define SCENE_MANAGER_H_

#include "cocos2d.h"
#include "SceneTypes.h"

namespace SceneManager
{
	cocos2d::CCScene* initScene();

	void swapScene(SceneType sceneType);
}
#endif //SCENE_MANAGER_H_