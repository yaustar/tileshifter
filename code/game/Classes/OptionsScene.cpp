#include "OptionsScene.h"

#include "SceneManager.h"

#include "FastLabelTTFTypes.h"
#include "FastLabelTTF.h"

#include "OptionSelectorBar.h"
#include "MenuButtonStandard.h"

#include "SoundManager.h"

USING_NS_CC;

//---------------------------------------
CCScene*
OptionsScene::createScene()
{
    CCScene *scene = CCScene::create();
    OptionsScene *layer = OptionsScene::create();

    scene->addChild(layer);
    return scene;
}
//---------------------------------------
bool
OptionsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
	//Allow touch control
	this->setTouchEnabled(true);

	//Cache the screen size to aid button placement
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();


	OptionSelectorBar* pSoundFxBar = OptionSelectorBar::create( CCCallFunc::create(this, callfunc_selector(OptionsScene::soundFxOnCallback)), CCCallFunc::create(this, callfunc_selector(OptionsScene::soundFxOffCallback)) );
	pSoundFxBar->setPosition( ccp(visibleSize.width*0.75f, visibleSize.height*0.7) );
	pSoundFxBar->setSwitch( SoundManager::sharedSoundManager()->isSoundFxAllowed() );
	this->addChild( pSoundFxBar );


	OptionSelectorBar* pMusicBar = OptionSelectorBar::create( CCCallFunc::create(this, callfunc_selector(OptionsScene::musicOnCallback)), CCCallFunc::create(this, callfunc_selector(OptionsScene::musicOffCallback)) );
	pMusicBar->setPosition( ccp(visibleSize.width*0.75f, visibleSize.height*0.6) );
	pMusicBar->setSwitch( SoundManager::sharedSoundManager()->isMusicAllowed() );
	this->addChild( pMusicBar );


	FastLabelTTF* pSoundFxText = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_45, CCSize(1.0f, pSoundFxBar->getContentSize().height), kCCTextAlignmentLeft, kCCVerticalTextAlignmentCenter );
	pSoundFxText->setString( "Sound Fx:" );
	pSoundFxText->setPosition( ccp( visibleSize.width*0.1f, pSoundFxBar->getPositionY()) );
	this->addChild( pSoundFxText );


	FastLabelTTF* pMusicFxText = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_45, CCSize(1.0f, pMusicBar->getContentSize().height), kCCTextAlignmentLeft, kCCVerticalTextAlignmentCenter );
	pMusicFxText->setString( "Music:" );
	pMusicFxText->setPosition( ccp( visibleSize.width*0.1f, pMusicBar->getPositionY()) );
	this->addChild( pMusicFxText );


	FastLabelTTF* pOnOffText = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_45, CCSize(1.0f, pMusicBar->getContentSize().height), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter );
	pOnOffText->setString( "On : Off" );
	pOnOffText->setPosition( ccp( pSoundFxBar->getPositionX(), pSoundFxBar->getPositionY() + visibleSize.height*0.075) );
	this->addChild( pOnOffText );


	//Generate main menu button
	MenuButtonStandard* pMainMenuButton = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(OptionsScene::mainMenuCallback) ));
	pMainMenuButton->setPosition( visibleSize.width - (pMainMenuButton->getSize().width*0.5f), (pMainMenuButton->getSize().height*0.5f) );
	pMainMenuButton->setString("Main Menu");
	this->addChild( pMainMenuButton );


    return true;
}
//---------------------------------------
void
OptionsScene::soundFxOnCallback()
{
	SoundManager::sharedSoundManager()->allowSoundFxPlayBack( true );
}
//---------------------------------------
void
OptionsScene::soundFxOffCallback()
{
	SoundManager::sharedSoundManager()->allowSoundFxPlayBack( false );
}
//---------------------------------------
void
OptionsScene::musicOnCallback()
{
	SoundManager::sharedSoundManager()->allowMusicPlayBack( true );
}
//---------------------------------------
void
OptionsScene::musicOffCallback()
{
	SoundManager::sharedSoundManager()->allowMusicPlayBack( false );
}
//---------------------------------------
void
OptionsScene::mainMenuCallback()
{
	SceneManager::swapScene( SCENE_MAIN_MENU );
}