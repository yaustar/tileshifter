#include "GameActivationLayer.h"

#include "GameCoreData.h"
#include "EventPauseGame.h"

USING_NS_CC;

const int ACTIVE_SEQ_NUM_TAG = 9456894;

//----------------------------------------------------
GameActivationLayer::GameActivationLayer(CCCallFunc* pSeqCallBack) :
	m_pSeqCallBack	( pSeqCallBack )
{
	m_pSeqCallBack->retain();
}
//----------------------------------------------------
GameActivationLayer::~GameActivationLayer()
{
}
//----------------------------------------------------
bool
GameActivationLayer::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

	CCCallFunc* pCallBack = CCCallFunc::create( this, callfunc_selector(GameActivationLayer::threeSeqComplete) );
	generateSequenceSprite( visibleSize*0.5f, RESOURCE_NAME_GO_SEQ_3, pCallBack, 1.5f, ACTIVE_SEQ_NUM_TAG );

	return true;
}
//----------------------------------------------------
void
GameActivationLayer::onExit()
{
	m_pSeqCallBack->release();
}
//----------------------------------------------------
void
GameActivationLayer::generateSequenceSprite(const CCPoint &pos, CoreResourceNameType resourceType, CCCallFunc* pSeqCallBack, float duration, const int spriteTag )
{
	CCSprite* pSprite = CCSprite::create( GameCoreData::getResourcesName(resourceType).c_str() );
	pSprite->setPosition( pos );
	pSprite->setTag( spriteTag );
	pSprite->setScale( 0.0f );
	addChild( pSprite );

	CCScaleTo* pScaleTo = CCScaleTo::create(duration, 3.0f, 3.0f);

	if( pSeqCallBack )
	{
		pSprite->runAction( CCSequence::createWithTwoActions(pScaleTo, pSeqCallBack) );
	}
	else
	{
		pSprite->runAction( pScaleTo );
	}

	pSprite->runAction( CCFadeOut::create(duration) );

	if( spriteTag != ACTIVE_SEQ_NUM_TAG )
	{
		//Remove the old tag!
		removeChildByTag( spriteTag-1 );
	}
}
//----------------------------------------------------
void
GameActivationLayer::threeSeqComplete()
{
	CCSprite* pThree = static_cast<CCSprite*>( getChildByTag(ACTIVE_SEQ_NUM_TAG) );

	CCCallFunc* pCallBack = CCCallFunc::create( this, callfunc_selector(GameActivationLayer::twoSeqComplete) );
	generateSequenceSprite( pThree->getPosition(), RESOURCE_NAME_GO_SEQ_2, pCallBack, 1.0f, ACTIVE_SEQ_NUM_TAG+1 );
}
//----------------------------------------------------
void
GameActivationLayer::twoSeqComplete()
{
	CCSprite* pTwo = static_cast<CCSprite*>( getChildByTag(ACTIVE_SEQ_NUM_TAG+1) );

	CCCallFunc* pCallBack = CCCallFunc::create( this, callfunc_selector(GameActivationLayer::oneSeqComplete) );
	generateSequenceSprite( pTwo->getPosition(), RESOURCE_NAME_GO_SEQ_1, pCallBack, 1.0f, ACTIVE_SEQ_NUM_TAG+2 );
}
//----------------------------------------------------
void
GameActivationLayer::oneSeqComplete()
{
	CCSprite* pOne = static_cast<CCSprite*>( getChildByTag(ACTIVE_SEQ_NUM_TAG+2) );

	CCCallFunc* pCallBack = CCCallFunc::create( this, callfunc_selector(GameActivationLayer::goSeqComplete) );
	generateSequenceSprite( pOne->getPosition(), RESOURCE_NAME_GO_SEQ_FINAL, pCallBack, 1.0f, ACTIVE_SEQ_NUM_TAG+3 );
}
//----------------------------------------------------
void
GameActivationLayer::goSeqComplete()
{
	removeChildByTag( ACTIVE_SEQ_NUM_TAG+3 );

	//Remove the game pause!
	EventPauseGame::create( false );
	//CCCallFunc* pCallBack = CCCallFunc::create( this, callfunc_selector(GameActivationLayer::goSeqComplete) );
	///generateSequenceSprite( pOne->getPosition(), RESOURCE_NAME_GO_SEQ_2, pCallBack, 1.0f, ACTIVE_SEQ_NUM_TAG+4 );

	//Sequence complete, inform the object that created this object
	runAction( m_pSeqCallBack );
}
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------