#include "FastLabelTTFManager.h"

USING_NS_CC;


#include "FastLabelTTFCache.h"
#include "FastLabelTTFFactory.h"

//Singleton Declaration ----- 
static FastLabelTTFManager *s_pSingleton = NULL;

//Struct Declaration ------
FastLabelTTFManager::CharCacheInfo::CharCacheInfo() :
	m_pCharCache	( NULL ),
	m_bUsed			( false )
{
};

FastLabelTTFManager::FastLabelTTFManager() :
	m_charCaches	( FAST_LABEL_TTF_COUNT )
{
}
//------------------------------------------
FastLabelTTFManager::~FastLabelTTFManager()
{
	//Singleton has destroyed itself.
	s_pSingleton = NULL;

	//Check for unused caches, they're a waste of memory!
	for(int i = 0; i < FAST_LABEL_TTF_COUNT; i++)
	{
		if( !m_charCaches[i].m_bUsed )
		{
			CCLog( "Unused FastLabelTTF Cache Detected! ID: %d", i );
		}
	}
}
//------------------------------------------
FastLabelTTFManager*
FastLabelTTFManager::create()
{
	if( !s_pSingleton )
	{
		//Copied from 'CREATE_FUNC', modified to cater for additional parameters.
		s_pSingleton = new FastLabelTTFManager( ); 
		if (s_pSingleton && s_pSingleton->init()) 
		{ 
			s_pSingleton->autorelease(); 
			return s_pSingleton; 
		} 
		else
		{ 
			delete s_pSingleton; 
			s_pSingleton = NULL; 
			return NULL; 
		}
	}
	return s_pSingleton;
}
//------------------------------------------
bool
FastLabelTTFManager::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	//Init the array of caches ---
	for( int i = 0; i < FAST_LABEL_TTF_COUNT; i++)
	{
		std::string fontName;
		float fontSize;

		FastLabelTTFFactory::GetFontInfo( fontName, fontSize, static_cast<FastLabelTTFTypes>(i) );
		m_charCaches[i].m_pCharCache = FastLabelTTFCache::create( fontName, fontSize );

		addChild( m_charCaches[i].m_pCharCache );
	}
	
	return true;
}
//------------------------------------------
FastLabelTTFManager*
FastLabelTTFManager::sharedManager()
{
	if( !s_pSingleton )
	{
		create();
	}

	return s_pSingleton;
}
//------------------------------------------
const FastLabelTTFCache*
FastLabelTTFManager::getCharacterCache(FastLabelTTFTypes fontType)
{
	//The cache has been requested, note this.
	m_charCaches[fontType].m_bUsed = true;

	return m_charCaches[fontType].m_pCharCache;
}
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------