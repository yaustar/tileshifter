#include "GameTileManager.h"

#include "BaseGameTile.h"
#include "GameTileFactory.h"

#include "RandomNumberGenerator.h"
#include "GameCoreData.h"

#include <vector>
#include <algorithm>

#include "EventListener.h"
#include "EventPauseGame.h"

//Use the 'cocos2d' namespace (only do this within the .cpp!)
USING_NS_CC; 

//----------------------------------------------------
GameTileManager::GameTileManager() :
m_pFirstGameTile ( NULL )
{
	
}

//----------------------------------------------------
bool
GameTileManager::init()
{
	//--------------------------------------------------------
    // Super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

	//Enable touch events for every tile (performed in next program 'step', given tiles priority of touch events)
	this->scheduleOnce( schedule_selector(GameTileManager::delayTileTouchEventActivationCallBack), 0.0f );

	//Listen for game pause event
	EventListener* pPauseGameListener = EventListener::create( this, callfuncO_selector(GameTileManager::pauseGameCallback), TS_EVENT_ID_PAUSE_GAME );
	this->addChild( pPauseGameListener );

	//Cache tile data access
	const CoreTileData &coreData ( GameCoreData::getTileData() );
	
	//Create local/temp 2D list of Base tile pointers (resized to the requested tile node size)
	std::vector<std::vector<BaseGameTile*> > tileList ( coreData.gridNodeSize.width ); //Gap between '> >' is a must! eclipse confuses it with a binary operator! 

	for(unsigned int i = 0; i < tileList.size(); i++)
	{
		tileList[i].resize(coreData.gridNodeSize.height);
	}
    
    // Do a preset of tiles as long as we are assuming we are using a 5x5 board
    const int TOTAL_PRESET_TILE_SIZE = 25;
    std::vector<TileType> shuffledTileTypeList;
    
    int boardSize = coreData.gridNodeSize.width * coreData.gridNodeSize.height;
    shuffledTileTypeList.reserve(boardSize);
    
    if (TOTAL_PRESET_TILE_SIZE == boardSize)
    {
        // Load the shuffled tile type list with preset counts
        for(int i = 0; i < 4; ++i)
        {
            shuffledTileTypeList.push_back(TILE_LEFT_BOTTOM);
            shuffledTileTypeList.push_back(TILE_LEFT_TOP);
            shuffledTileTypeList.push_back(TILE_RIGHT_BOTTOM);
            shuffledTileTypeList.push_back(TILE_RIGHT_TOP);
        }
        
		//Generate nine 'TILE_ALL' (removed 'TILE_LEFT_RIGHT' and 'TILE_TOP_BOTTOM')
		for(int i = 0; i < 9; ++i)
        {
            shuffledTileTypeList.push_back(TILE_ALL);
        }
        
        CCAssert(shuffledTileTypeList.size() == TOTAL_PRESET_TILE_SIZE, "Preset shuffled tile type list is not the correct size for the board");
        
        // Fix up the error if the list was the wrong size
        if (shuffledTileTypeList.size() > TOTAL_PRESET_TILE_SIZE)
        {
            for(unsigned int i = 0; i < shuffledTileTypeList.size()-TOTAL_PRESET_TILE_SIZE; ++i)
            {
                shuffledTileTypeList.pop_back();
            }
        }
        
        if (shuffledTileTypeList.size() < TOTAL_PRESET_TILE_SIZE)
        {
            for(unsigned int i = 0; i < TOTAL_PRESET_TILE_SIZE-shuffledTileTypeList.size(); ++i)
            {
                shuffledTileTypeList.push_back(static_cast<TileType>(RandomNumberGenerator::getNumber(0, TILE_NUM_TYPES-1)));
            }
        }
        
        auto randomFunc = [](int i) -> int {return static_cast<TileType>(RandomNumberGenerator::getNumber(i-1));};
        std::random_shuffle(shuffledTileTypeList.begin(), shuffledTileTypeList.end(), randomFunc);
    }
    else
    {
        for(int w = 0; w < coreData.gridNodeSize.width; w++)
        {
            for(int h = 0; h < coreData.gridNodeSize.height; h++)
            {
				TileType tileType = static_cast<TileType>( RandomNumberGenerator::getNumber(0, TILE_NUM_TYPES-1) );

				//Remove the 'LEFT_RIGHT' and 'TOP_BOTTOM'
				if( tileType == TILE_LEFT_RIGHT || tileType == TILE_TOP_BOTTOM )
				{
					tileType = TILE_ALL;
				}

                shuffledTileTypeList.push_back( tileType );
            }
        }
    }
    
    for(unsigned int i = 0; i<shuffledTileTypeList.size(); ++i)
    {
        std::cerr <<"(" << i << " " << shuffledTileTypeList[i] << ") ";
    }
    
    for(int w = 0; w < coreData.gridNodeSize.width; w++)
    {
        for(int h = 0; h < coreData.gridNodeSize.height; h++)
        {
            int shuffleTileIndex = (w * coreData.gridNodeSize.height) + h;
            CCPoint currPos (coreData.gridBound.getMinX() + (w * coreData.tileSize.height), coreData.gridBound.getMinY() + (h * coreData.tileSize.height));
            tileList[w][h] = createGameTileFromFactory(shuffledTileTypeList[shuffleTileIndex], currPos);
            this->addChild( tileList[w][h] );
        }
    }

	//With all tiles created, we won't be maintaining a list of tiles in the manager, init starting tile neighbours so they can link to eachother.
	for(int w = 0; w < coreData.gridNodeSize.width; w++)
	{
		for(int h = 0; h < coreData.gridNodeSize.height; h++)
		{
			BaseGameTile::TileNeighbourStruct tileNeighbours;

			if(w > 0)
			{
				tileNeighbours.pTileList[TILE_LEFT] = tileList[w-1][h];
			}
			if(w < (coreData.gridNodeSize.width-1))
			{
				tileNeighbours.pTileList[TILE_RIGHT] = tileList[w+1][h];
			}
			if(h < (coreData.gridNodeSize.height-1))
			{
				tileNeighbours.pTileList[TILE_TOP] = tileList[w][h+1];
			}
			if(h > 0)
			{
				tileNeighbours.pTileList[TILE_BOTTOM] = tileList[w][h-1];
			}

			tileList[w][h]->initTileNeighbours(tileNeighbours);
		}
	}

	//Init access to first game tile.
	m_pFirstGameTile = tileList.front().front();

	return true;
}
//----------------------------------------------------
//Public Methods
//----------------------------------------------------
const BaseGameTile*
GameTileManager::selectCharacterTile()
{
	updateFirstGameTileAccessPointer();
	return m_pFirstGameTile->getNeighbourTile( CCPoint(1.0f, 1.0f) );
}
//----------------------------------------------------
const BaseGameTile*
GameTileManager::selectGhostTile()
{
	const CCSize& gridNodeSize = GameCoreData::getTileData().gridNodeSize;

	updateFirstGameTileAccessPointer();
	return m_pFirstGameTile->getNeighbourTile( CCPoint(gridNodeSize.width-2.0f, gridNodeSize.height-2.0f) );
}
//----------------------------------------------------
void
GameTileManager::getFirstTile(const BaseGameTile* &OutFirstTile) const
{
	getFirstGameTileAccessPointer( OutFirstTile );
}
//------------------------------------------
void
GameTileManager::pauseGameCallback(CCObject* pObject)
{
	EventPauseGame* pPauseGameEvent = static_cast<EventPauseGame*>( pObject );

	if( pPauseGameEvent->activatePause() )
	{
		this->pauseSchedulerAndActions();
	}
	else
	{
		this->resumeSchedulerAndActions();
	}
}
//----------------------------------------------------
//Private Methods
//----------------------------------------------------
void
GameTileManager::delayTileTouchEventActivationCallBack(float delta)
{
	m_pFirstGameTile->swapNeighbourTouchEvents( m_pFirstGameTile, true, true, false ); //Set to true (re-adding it)
}
//----------------------------------------------------
void
GameTileManager::getFirstGameTileAccessPointer(const BaseGameTile* &pOutFirstTile) const
{
	//Two operations move the current tile accessor to most left and lowest (i.e. the first tile)
	pOutFirstTile = pOutFirstTile->getConstantCornerTile( TILE_LEFT );
	pOutFirstTile = pOutFirstTile->getConstantCornerTile( TILE_BOTTOM );
}
//----------------------------------------------------
void
GameTileManager::getFirstGameTileAccessPointer(BaseGameTile* &pOutFirstTile)
{
	//Two operations move the current tile accessor to most left and lowest (i.e. the first tile)
	pOutFirstTile = pOutFirstTile->getCornerTile( TILE_LEFT );
	pOutFirstTile = pOutFirstTile->getCornerTile( TILE_BOTTOM );
}
//----------------------------------------------------
void
GameTileManager::updateFirstGameTileAccessPointer()
{
	//Updates the member variable directly (as we pass it in directly)
	getFirstGameTileAccessPointer( m_pFirstGameTile );
}