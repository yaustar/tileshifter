#include <sstream>
//#include <cmath>
#include <algorithm>

#include "CoinObject.h"

#include "BaseGameTile.h"
#include "CharacterObject.h"

#include "RandomNumberGenerator.h"
#include "GameCoreData.h"
#include "HighscoreManager.h"

#include "FastLabelTTF.h"
#include "EventRequestSoundEffectPlay.h"

#include "EventListener.h"
#include "EventPauseGame.h"

USING_NS_CC;

const float START_NORMALISED_SCALE = 1.25f;
const float MINIMUM_SCALE = 0.3f;

//------------------------------------------
CoinObject::CoinObject(const BaseGameTile* pCharacterTile) :
	m_pCoinTile			( pCharacterTile ),
	m_pSprite			( NULL ),
	m_pScoreLabel		( NULL ),
	m_bLockTouchEvents	( false ),
	m_normalisedScale   ( START_NORMALISED_SCALE )
{
	m_pSprite = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_COIN).c_str() );
	m_pSprite->setPosition( calculateCoinPosWithSanityCheck() );
	this->addChild(m_pSprite);
}
//------------------------------------------
CoinObject*
CoinObject::create(const BaseGameTile* pCharacterTile)
{
	//Copied from 'CREATE_FUNC', modified to cater for additional parameters.
	 CoinObject *pRet = new CoinObject( pCharacterTile ); 
    if (pRet && pRet->init()) 
    { 
        pRet->autorelease(); 
    }
    else 
    { 
        delete pRet; 
        pRet = NULL; 
    }
    
    return pRet;
}
//------------------------------------------
bool
CoinObject::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	//Enable Touch Events On This Layer!
	this->setTouchEnabled( true );

	//Listen for game pause event
	EventListener* pPauseGameListener = EventListener::create( this, callfuncO_selector(CoinObject::pauseGameCallback), TS_EVENT_ID_PAUSE_GAME );
	this->addChild( pPauseGameListener );


	CCSize visibleSize ( CCDirector::sharedDirector()->getVisibleSize() );

	//Assign previous highscore output
	FastLabelTTF* pPrevHighscore = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_30, visibleSize, kCCTextAlignmentLeft, kCCVerticalTextAlignmentTop );
	pPrevHighscore->setPosition( ccp(visibleSize.width*0.51f, visibleSize.height*0.51f) );
	pPrevHighscore->setColor( ccc3(255.0f, 0.0f, 255.0f) );

	std::string prevHighscore("Highscore\n");
	prevHighscore.append( HighscoreManager::sharedHighscoreManager()->getHighestScore() );
	pPrevHighscore->setString( prevHighscore.c_str() );

	this->addChild( pPrevHighscore );


	//Assign score output
	m_pScoreLabel = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_30, visibleSize, kCCTextAlignmentLeft, kCCVerticalTextAlignmentTop );
	
	//Set pos and colour 
	m_pScoreLabel->setPosition( ccp(pPrevHighscore->getPositionX(), pPrevHighscore->getPositionY()-pPrevHighscore->getSize().height) ); //Centre screen size inorder to utilise 'alignemnts above in setup'.
	m_pScoreLabel->setColor( ccc3(0.0f, 0.0f, 255.0f) );
	
	//Set string to the current score and add to this layer!
	updateScoreOuput();
	this->addChild( m_pScoreLabel );

	// Set the update loop
    this->schedule( schedule_selector(CoinObject::updateLoop), 0.f );

	return true;
}
//------------------------------------------
void
CoinObject::onExit()
{
	this->setTouchEnabled( false );
	this->unscheduleAllSelectors();
}
//------------------------------------------
void
CoinObject::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
	if( !m_bLockTouchEvents )
	{
		//This will be called in-alignment with the tiles being moved/updated (so is a good time to move our coin in-alignment).
		//Also sets opacity!
		matchCoinToActiveTile();
	}
}
//------------------------------------------
void 
CoinObject::ccTouchesEnded(CCSet* pTouches, CCEvent* pEvent)
{
	if( !m_bLockTouchEvents )
	{
		m_bLockTouchEvents = true;

		CCMoveTo* pMoveTo = CCMoveTo::create( m_pCoinTile->getTileSlideSpeed(), m_pCoinTile->getEndSlidePosition() );
		CCCallFunc* pMatchCoinToActiveTileCallBack = CCCallFunc::create( this, callfunc_selector(CoinObject::matchCoinToActiveTile) );

		//Push into sequence
		CCSequence* pSequence = CCSequence::createWithTwoActions(pMoveTo, pMatchCoinToActiveTileCallBack); //Once move complete call 'Centre' call back
		m_pSprite->runAction( pSequence );
	}
}
//------------------------------------------
void
CoinObject::updateLoop(float dt)
{
	const float SCALE_CHANGE_PER_SECOND = 0.1f;
	m_normalisedScale -= SCALE_CHANGE_PER_SECOND * dt;
	if(m_normalisedScale <= MINIMUM_SCALE)
	{
		resetCoinToNewTile();
	}
    
	m_pSprite->setScale(getNormalisedScale());
}
//------------------------------------------
bool
CoinObject::testForCollisionWithCharacter(const CharacterObject* pCharacter, bool bIsPlayer)
{
	if( m_pSprite->boundingBox().intersectsRect(pCharacter->getCharacterBound()) )
	{
		if( bIsPlayer )
		{
			GameCoreData::incrementScore(getNormalisedScale());
			updateScoreOuput();

			//Collision requires sound effect
			EventRequestSoundEffectPlay::create( SOUND_EFFECT_COLLECT_COIN );
		}
		else
		{
			//Collision requires sound effect (negative sound effect!)
			EventRequestSoundEffectPlay::create( SOUND_EFFECT_NEG_COLLECT_COIN );
		}
		

        resetCoinToNewTile();
        
		return true;
	}

	return false;
}
//----------------------------------------------------
void
CoinObject::pauseGameCallback(CCObject* pObject)
{
	EventPauseGame* pPauseGameEvent = static_cast<EventPauseGame*>( pObject );

	if( pPauseGameEvent->activatePause() )
	{
		this->pauseSchedulerAndActions();
	}
	else
	{
		this->resumeSchedulerAndActions();
	}
}
//------------------------------------------
cocos2d::CCPoint 
CoinObject::calculateCoinPosWithSanityCheck()
{
	//Offset the coins tile from its current position (provides a sanity check within itself as we must avoid the current position!)
	const CoreTileData& coreData = GameCoreData::getTileData();

	//Find the new coin tile (applied to 'm_pCoinTile')
	findNewCoinTile( ccp(	static_cast<float>(RandomNumberGenerator::getNumber( 0, coreData.gridNodeSize.width-1.0f )),
							static_cast<float>(RandomNumberGenerator::getNumber( 0, coreData.gridNodeSize.height-1.0f ))	) );
	
	return m_pCoinTile->getPosition();
}
//------------------------------------------
void
CoinObject::findNewCoinTile(const cocos2d::CCPoint &offset)
{
	//Number Of Offset Direction
	const int NUM_OFFSET_DIR = 2;

	TileDirection neighbourDir[NUM_OFFSET_DIR]	= { TILE_RIGHT, TILE_TOP };
	TileDirection wrapCornerDir[NUM_OFFSET_DIR] = { TILE_LEFT, TILE_BOTTOM };

	for(int i = 0; i < NUM_OFFSET_DIR; i++)
	{
		const BaseGameTile* pCoinTile = m_pCoinTile->getConstantNeighbourTile( neighbourDir[i] ); //Get the tile one to the right

		if( pCoinTile == NULL )
		{
			pCoinTile = m_pCoinTile->getConstantCornerTile( wrapCornerDir[i] );
		}

		m_pCoinTile = pCoinTile;
	}
}
//------------------------------------------
void
CoinObject::resetCoinToNewTile()
{
    m_pSprite->setPosition( calculateCoinPosWithSanityCheck() );
    m_normalisedScale = START_NORMALISED_SCALE;
}
//------------------------------------------
void
CoinObject::matchCoinToActiveTile()
{
	//this->setTouchEnabled( true ); //Switch touch events back on!
	m_bLockTouchEvents = false;

	if( m_pCoinTile )
	{
		m_pSprite->setPosition( m_pCoinTile->getPosition() );
		m_pSprite->setOpacity( m_pCoinTile->getTilesOpacity() );
	}
}
//------------------------------------------
void
CoinObject::updateScoreOuput()
{
	if( m_pScoreLabel )
	{
		std::stringstream ss;
		ss << "Score\n";
		ss << GameCoreData::getScoreData().score;
		
		m_pScoreLabel->setString( ss.str().c_str() );
	}
}
//------------------------------------------
float
CoinObject::getNormalisedScale() const
{
    return std::min<float>(std::max<float>(m_normalisedScale, MINIMUM_SCALE), 1.f);
}
