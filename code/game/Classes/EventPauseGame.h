#ifndef EVENT_PAUSE_GAME_H
#define EVENT_PAUSE_GAME_H

#include "EventMessage.h"
#include "ExtraDeclarations.h"

#include "GameCoreData.h"

class EventPauseGame : public EventMessage
{
public:
	EventPauseGame( bool bPauseGame ) :
		EventMessage		( TS_EVENT_ID_PAUSE_GAME ),
		m_bActivatePause	( bPauseGame )
	{
		GameCoreData::setGamePaused( bPauseGame );
	}

	CREATE_FUNC_ONE_ARGU( EventPauseGame, bool, bPauseGame );


	//Accessor ----
	bool activatePause() //Or remove it?
	{
		return m_bActivatePause; 
	}

private:

	bool m_bActivatePause;
};
#endif //EVENT_PAUSE_GAME_H