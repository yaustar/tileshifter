#include "SceneManager.h"
#include "SceneFactory.h"

#include "EventManager.h"
#include "SoundManager.h"
#include "HighscoreManager.h"

USING_NS_CC;

//------------------------------------
//General Functions 
//------------------------------------
CCScene* assignScene(SceneType sceneType)
{
	CCScene* pScene = SceneFactory::getNewScene( sceneType );

	EventManager* pEventManager = EventManager::sharedEventManager();
	SoundManager* pSoundManager = SoundManager::sharedSoundManager();
	HighscoreManager* pHighscoreManager = HighscoreManager::sharedHighscoreManager();

	//Retain to avoid automated cleanup, remove from current parent (the previous scene!)
	pEventManager->retain();
	pEventManager->removeFromParent();

	pSoundManager->retain();
	pSoundManager->removeFromParent();

	pHighscoreManager->retain();
	pHighscoreManager->removeFromParent();

	//Inject the event manager into the latest scene (forcing it to always exist for each scene!)
	pScene->addChild( pEventManager );
	pScene->addChild( pSoundManager );
	pScene->addChild( pHighscoreManager );

	//Release the previous retainment, inform the event manager of the scene change
	pEventManager->release();
	pEventManager->sceneTransitionComplete();

	pSoundManager->sceneTransitionComplete( sceneType );
	pSoundManager->release();
	
	pHighscoreManager->sceneTransitionComplete( sceneType );
	pHighscoreManager->release();

	return pScene;
}

//------------------------------------
CCScene* SceneManager::initScene()
{
	//Init 'global' systems that require attachment 
	EventManager::create();
	SoundManager::create();
	HighscoreManager::create();

	//Assign the first scene!
	return assignScene( SCENE_TITLE );
}
//------------------------------------
void SceneManager::swapScene(SceneType sceneType)
{
	if(sceneType != SCENE_EXIT)
	{
		CCScene* pScene = assignScene( sceneType ); //Create the new scene instance through factory (cached old scene isn't lost, CCDirector maintains it still!

		//CCTransitionFlipAngular* pTrans = CCTransitionFlipAngular::create( 0.8f, pScene ); //Swap the scenes with 'Page Turn' effect
		CCTransitionFade* pTrans = CCTransitionFade::create( 1.25f, pScene );
		CCDirector::sharedDirector()->replaceScene(pTrans); //Add the new scene (removing the old one for us)
	}
	else
	{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
			CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
		#else
			CCDirector::sharedDirector()->end();
			#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
				exit(0);
			#endif
		#endif
	}
}
//------------------------------------
//------------------------------------
//------------------------------------
