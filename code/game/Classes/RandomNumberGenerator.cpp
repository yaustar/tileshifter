#include "RandomNumberGenerator.h"

#include <cstdlib>
#include <ctime>

//Private Declaration.
void correctRangeBounds(int startRange, int endRange, int &outStartRange, int &outEndRange);

//------------------------------------------
void RandomNumberGenerator::initSeed()
{
	srand(time(0));
}
//------------------------------------------
int
RandomNumberGenerator::getNumber(int endRange) //Between 0 and max range
{
	return getNumber(0, endRange);
}
//------------------------------------------
int
RandomNumberGenerator::getNumber(int startRange, int endRange) //Between min range and max range
{
	correctRangeBounds(startRange, endRange, startRange, endRange);
	return ((rand() % ((endRange+1) - startRange)) + startRange);
}
//------------------------------------------
void
correctRangeBounds(int startRange, int endRange, int &outStartRange, int &outEndRange)
{
	if(startRange > endRange) //If someone set the ranges backwards
	{
		int rangeCache = startRange;
		startRange = endRange;
		endRange = rangeCache;
	}

	outStartRange = startRange;
	outEndRange = outEndRange;
}