#ifndef RANDOM_NUMBER_GENRATOR_H
#define RANDOM_NUMBER_GENRATOR_H

//------------------------------------------------------------
//Designed to generate numbers between passed values.
//------------------------------------------------------------

namespace RandomNumberGenerator
{
	void initSeed();

	int getNumber(int endRange);
	int getNumber(int startRange, int endRange);
};

#endif //RANDOM_NUMBER_GENRATOR_H