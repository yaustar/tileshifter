#include "FastLabelTTF.h"

USING_NS_CC;

#include "FastLabelTTFCache.h"
#include "FastLabelTTFManager.h"

FastLabelTTF::FastLabelTTF(FastLabelTTFTypes fontType, const cocos2d::CCSize &dimensions, CCTextAlignment horTextAlignment, CCVerticalTextAlignment verTextAlignment) :
	m_textBoxDimensions		( dimensions ),
	m_stringSize			( CCSizeZero ),

	m_horTextAlign			( horTextAlignment ),
	m_verTextAlign			( verTextAlignment ),
	m_pCharacterCache		( FastLabelTTFManager::sharedManager()->getCharacterCache(fontType) )
{
}
//------------------------------------------
FastLabelTTF::~FastLabelTTF()
{
	m_pCharacterCache = NULL;
}
//------------------------------------------
FastLabelTTF*
FastLabelTTF::create(FastLabelTTFTypes fontType, const cocos2d::CCSize &dimensions, CCTextAlignment horTextAlignment, CCVerticalTextAlignment verTextAlignment)
{
	//Copied from 'CREATE_FUNC', modified to cater for additional parameters.
	FastLabelTTF *pRet = new FastLabelTTF( fontType, dimensions, horTextAlignment, verTextAlignment ); 
    if (pRet && pRet->init()) 
    { 
        pRet->autorelease(); 
        return pRet; 
    } 
    else
    { 
        delete pRet; 
        pRet = NULL; 
        return NULL; 
    }
}
//------------------------------------------
bool
FastLabelTTF::init()
{
	 // Super init first
	if ( !CCSprite::init() )
	{
		return false;
	}

	//Default white colour
	setColor( ccc3(255.0f, 255.0f, 255.0f) );

	return true;
}
//------------------------------------------
void
FastLabelTTF::setOpacity(GLubyte opacity)
{
	CCSprite::setOpacity( opacity );

	CCArray* pChildren = this->getChildren();

	if( pChildren )
	{
		for(unsigned int i = 0; i < pChildren->count(); i++)
		{
			CCSprite* pCharSprite = static_cast<CCSprite*>( pChildren->objectAtIndex(i) );
			
			pCharSprite->setOpacity( opacity );
		}
	}
}
//------------------------------------------
void
FastLabelTTF::setString(const char* string)
{
	//Clean up any previous string
	removeAllChildren();

	//Cache the final width and height (as it's being generated)
	float currWidth = 0.0f;

	m_stringSize = CCSizeZero;

	CCSprite* pFirstCharSprite = NULL;

	for(unsigned int i = 0; i < strlen(string); i++)
	{
		if( string[i] == '\n' )
		{
			m_stringSize.height += pFirstCharSprite->boundingBox().size.height;
			currWidth = 0.0f;
		}
		else
		{
			CCTexture2D* pCharTexture = m_pCharacterCache->getCharacterTexture( string[i] );

			if( pCharTexture )
			{
				CCSprite* pCharSprite = CCSprite::createWithTexture( pCharTexture );

				//Apply the colour
				pCharSprite->setColor( getColor() );
				pCharSprite->setOpacity( getOpacity() );

				//Add to this layer
				this->addChild( pCharSprite );

				//Set the position from the previous character
				pCharSprite->setPosition( ccp(currWidth, -m_stringSize.height) + ccp(pCharSprite->boundingBox().size.width*0.5f, 0.0f) );
			
				//Update the total width and heights
				currWidth += pCharSprite->boundingBox().size.width;

				//Apply the largest width so far
				if( currWidth > m_stringSize.width )
				{
					m_stringSize.width = currWidth;
				}

				//Store the first generated sprite!
				if( i == 0 )
				{
					pFirstCharSprite = pCharSprite;
				}
			}
		}
	}

	//if( m_stringSize.height == 0.0f )
	//{
		m_stringSize.height += pFirstCharSprite->boundingBox().size.height;
	//}

	if( pFirstCharSprite )
	{
		//Produce a temporary rectangle around the entire sprite 'string'
		CCRect textRect( pFirstCharSprite->boundingBox().getMinX(), pFirstCharSprite->boundingBox().getMinY(), m_stringSize.width, m_stringSize.height );
	
		//Apply the requested text alignment!
		applyTextAlignment( textRect );
	}
}
//------------------------------------------
void
FastLabelTTF::setColor(const ccColor3B &colour)
{
	CCSprite::setColor( colour );

	CCArray* pChildren = this->getChildren();

	if( pChildren )
	{
		for(unsigned int i = 0; i < pChildren->count(); i++)
		{
			static_cast<CCSprite*>( pChildren->objectAtIndex(i) )->setColor( colour );
		}
	}
}
//------------------------------------------
const CCSize&
FastLabelTTF::getSize()
{
	return m_stringSize;
}
//------------------------------------------
void
FastLabelTTF::applyTextAlignment(cocos2d::CCRect &textRect)
{
	CCArray* pChildren = this->getChildren();

	switch( m_horTextAlign )
	{
	case kCCTextAlignmentLeft:
		for(unsigned int i = 0; i < pChildren->count(); i++)
		{
			CCSprite* pCharSprite = static_cast<CCSprite*>( pChildren->objectAtIndex(i) );
			pCharSprite->setPositionX( textAlignmentLeftOrBottomPosition(pCharSprite->getPositionX(), m_textBoxDimensions.width, textRect.getMinX()) );
		}
		break;

	case kCCTextAlignmentCenter:
		for(unsigned int i = 0; i < pChildren->count(); i++)
		{
			CCSprite* pCharSprite = static_cast<CCSprite*>( pChildren->objectAtIndex(i) );
			pCharSprite->setPositionX( textAlignmentCentrePosition(pCharSprite->getPositionX(), textRect.getMidX()) );
		}
		break;

	case kCCTextAlignmentRight:
		for(unsigned int i = 0; i < pChildren->count(); i++)
		{
			CCSprite* pCharSprite = static_cast<CCSprite*>( pChildren->objectAtIndex(i) );
			pCharSprite->setPositionX( textAlignmentRightOrTopPosition(pCharSprite->getPositionX(), m_textBoxDimensions.width, textRect.getMaxX() ) );
		}
		break;

	default:
		CCAssert( false, "Horizontal Text Alignment (Fast Label TTF.cpp) Failed" );
	}


	switch( m_verTextAlign )
	{
	case kCCVerticalTextAlignmentTop:
		for(unsigned int i = 0; i < pChildren->count(); i++)
		{
			CCSprite* pCharSprite = static_cast<CCSprite*>( pChildren->objectAtIndex(i) );
			pCharSprite->setPositionY( textAlignmentRightOrTopPosition( pCharSprite->getPositionY(), m_textBoxDimensions.height, textRect.getMaxY()) );
		}
		break;

	case kCCVerticalTextAlignmentCenter:
		//Set by default!
		break;

	case kCCVerticalTextAlignmentBottom:
		for(unsigned int i = 0; i < pChildren->count(); i++)
		{
			CCSprite* pCharSprite = static_cast<CCSprite*>( pChildren->objectAtIndex(i) );
			pCharSprite->setPositionY( textAlignmentLeftOrBottomPosition(pCharSprite->getPositionY(), m_textBoxDimensions.height, textRect.getMinY()) );
		}
		break;

	default:
		CCAssert( false, "Vertical Text Alignment (Fast Label TTF.cpp) Failed" );
	}
}
//------------------------------------------
float
FastLabelTTF::textAlignmentLeftOrBottomPosition(float currPos, float textBoxDimension, float textRectMin)
{
	return (currPos - (textBoxDimension*0.5f) - textRectMin);
}
//------------------------------------------
float
FastLabelTTF::textAlignmentCentrePosition(float currPos, float textRectMid)
{
	return currPos - textRectMid;
}
//------------------------------------------
float
FastLabelTTF::textAlignmentRightOrTopPosition(float currPos, float textBoxDimension, float textRectMax)
{
	return currPos - (textRectMax - (textBoxDimension*0.5f));
}
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------