#ifndef SCENE_FACTORY_H
#define SCENE_FACTORY_H

#include "cocos2d.h"
#include "SceneTypes.h"

namespace SceneFactory
{
	cocos2d::CCScene* getNewScene(SceneType sceneType);
}
#endif //SCENE_FACTORY_H