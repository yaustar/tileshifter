#include "TurnGameTile.h"

#include "GameCoreData.h"
#include "RandomNumberGenerator.h"

//Use the 'cocos2d' namespace (only do this within the .cpp!)
USING_NS_CC;

//----------------------------------------------------
TurnGameTile::TurnGameTile(TileType tileID, const CCPoint& pos) :
BaseGameTile(tileID, pos)
{

}
//--------------------------------------------------------------
//Public Virtual Methods
//--------------------------------------------------------------
const CCPoint
TurnGameTile::getEdgeOffset(TileDirection &inOutCurrDir) const
{
	//Turn Game Tile Specific Code -----
	//Converts the current direction to the next direction (as the direction changes!)
	if( inOutCurrDir == ms_invertTileDirection[m_validTileDirList.front()] )
	{
		inOutCurrDir = m_validTileDirList.back();
	}
	else if ( inOutCurrDir == ms_invertTileDirection[m_validTileDirList.back()] )
	{
		inOutCurrDir = m_validTileDirList.front();
	}

	return ms_vectorDir[inOutCurrDir] * getTilesHalfOffset( inOutCurrDir );
}
//----------------------------------------------------
//---------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------