#include "AllGameTile.h"

#include "GameCoreData.h"
#include "RandomNumberGenerator.h"

USING_NS_CC;

//--------------------------------------------------------------
AllGameTile::AllGameTile(TileType tileID, const CCPoint& pos) :
	BaseGameTile( tileID, pos )
{

}
//--------------------------------------------------------------
//Public Virtual Methods
//--------------------------------------------------------------
const CCPoint
AllGameTile::getEdgeOffset(TileDirection &inOutCurrDir) const
{
	return ms_vectorDir[inOutCurrDir] * getTilesHalfOffset( inOutCurrDir );
}
//------------------------------------------------------------------