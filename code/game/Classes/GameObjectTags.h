#ifndef GAME_OBJECT_TAGS_H
#define GAME_OBJECT_TAGS_H

enum GameObjectTags
{
	GAME_OBJ_TAG_TILE_MANAGER,
	GAME_OBJ_TAG_CHARACTER,
	GAME_OBJ_TAG_GHOST,
	GAME_OBJ_TAG_COIN,

	//Auto Gen -----
	GAME_OBJ_TAG_COUNT
};
#endif //GAME_OBJECT_TAGS_H