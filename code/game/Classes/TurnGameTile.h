#ifndef TURN_GAME_TILE_H
#define TURN_GAME_TILE_H

#include "BaseGameTile.h"

class TurnGameTile : public BaseGameTile
{
public:

	TurnGameTile(TileType tileID, const cocos2d::CCPoint& pos);

	//Implement the Layers 'create' function - Calls default constructor (and the base classes init!)
	CREATE_FUNC_TILE(TurnGameTile);

	//Public Virtual Methods - Derived from base! --------------------
	virtual const cocos2d::CCPoint getEdgeOffset(TileDirection &inOutCurrDir) const; 

};
#endif //TURN_GAME_TILE_H