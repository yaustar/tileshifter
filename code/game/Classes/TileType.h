#ifndef TILE_TYPES_H
#define TILE_TYPES_H

//Containts tile types, allowing multiple classes to utilise them without
//the need to know about the entire tile class (allows many classes to forward
//include the tile classes, and later include it in their .cpp file!

//Enum -----------------
enum TileType
{
	TILE_ALL = 0,

	TILE_LEFT_RIGHT,
	TILE_TOP_BOTTOM,

	TILE_RIGHT_BOTTOM,
	TILE_LEFT_TOP,
	TILE_RIGHT_TOP,
	TILE_LEFT_BOTTOM,

	//Auto Generated ----
	TILE_NUM_TYPES
};

enum TileDirection
{
	TILE_LEFT = 0,
	TILE_RIGHT,
	TILE_TOP,
	TILE_BOTTOM,

	//Auto-Generated ----
	TILE_NUM_DIRECTIONS,
};

namespace TileInfo
{
	const TileDirection	g_invertTileDirection[TILE_NUM_DIRECTIONS]	= { TILE_RIGHT, TILE_LEFT, TILE_BOTTOM, TILE_TOP };
}

enum TileMoveType
{
	TILE_MOVE_NULL = 0,
	TILE_MOVE_HOR, //HORIZONTAL
	TILE_MOVE_VER //VERTICAL
};

enum TileMoveTypeDirection
{
	TILE_LOW_DIR = 0,
	TILE_HIGH_DIR,

	//Auto-Generated ----
	TILE_NUM_TYPE_DIRECTIONS
};

#endif //TILE_TYPES_H