#include "GameCoreData.h"

CoreTileData	GameCoreData::ms_coreTileData;
CoreScoreData	GameCoreData::ms_coreScoreData			( 100 );

ResourceNames	GameCoreData::ms_coreResourceNames		( RESOURCE_NAME_COUNT );
bool			GameCoreData::ms_bGamePaused			( true );

#include "TileType.h"

USING_NS_CC;

//---------------------------------------
void
GameCoreData::init()
{
	//Grid Node/Tile Size (Number of tiles along width & height)
	ms_coreTileData.gridNodeSize = CCPoint(5.0f, 5.0f);

	//Decide on a forced screen size (will need to incorporate the above grid size if made bigger, along with adding further asset sizes)
	{
		CCSize screenSize = CCDirector::sharedDirector()->getVisibleSize();
		
		//Set the design resolution (this essentially locks the screen res to a resolution residing around 640x***, but provides scaling options)
		CCSize designRes = generateDesignResolution( screenSize );

		//Phones with smaller screens usually have less processing power, although they can scale the larger assets,
		//using smaller ones should lower their overhead
		//if( screenSize.height >= 640 )
		//{
			assignResourceDirectory( "128/" );
		//}
		//else
		//{
		//	assignResourceDirectory( "64/" );
		//	designRes = designRes * 0.5f; //Halved
		//}


		CCDirector::sharedDirector()->getOpenGLView()->setDesignResolutionSize( designRes.width, designRes.height, kResolutionShowAll );
	}

	//Cache all asset/file names
	assignResourceNames();

	//Tile Size / Tile Size Half
	CCSize localTileSize = CCSprite::create( getResourcesName(RESOURCE_NAME_TILE).c_str() )->getContentSize();
	localTileSize.width /= TILE_NUM_TYPES;
	
	ms_coreTileData.tileSize = localTileSize;
	ms_coreTileData.tileSizeHalf = ms_coreTileData.tileSize * 0.5f;

	//Grid Size
	ms_coreTileData.gridSize = CCSize( ms_coreTileData.tileSize.width * ms_coreTileData.gridNodeSize.width,  ms_coreTileData.tileSize.height * ms_coreTileData.gridNodeSize.height );

	
	//Grid Bound (grid start position & end position)
	const CCSize SCREEN_SIZE = CCDirector::sharedDirector()->getVisibleSize();


	const CCPoint tileStartPos (	(SCREEN_SIZE.width*0.5f - ((ms_coreTileData.gridNodeSize.width * ms_coreTileData.tileSize.width) * 0.5f)) + ms_coreTileData.tileSizeHalf.width,
							SCREEN_SIZE.height*0.5f -((ms_coreTileData.gridNodeSize.height * ms_coreTileData.tileSize.height) * 0.5f) + ms_coreTileData.tileSizeHalf.height);

	ms_coreTileData.gridBound = CCRect(tileStartPos.x, tileStartPos.y, ms_coreTileData.gridSize.width, ms_coreTileData.gridSize.height);

	//Reset the score data (init is called everytime the game scene is entered!)
	resetScore();

	//Set display scale factor, based on the screen size (screen size is affected directly by the above design resolution)
	float scaleFactor = ms_coreTileData.gridSize.height / (SCREEN_SIZE.height - (SCREEN_SIZE.height - ms_coreTileData.gridSize.height));
	CCDirector::sharedDirector()->setContentScaleFactor( scaleFactor );
}
//---------------------------------------
const std::string&
GameCoreData::getResourcesName(CoreResourceNameType resourceType)
{
	return ms_coreResourceNames[resourceType];
}
//---------------------------------------
const CoreTileData&
GameCoreData::getTileData()
{
	return ms_coreTileData;
}
//---------------------------------------
const CoreScoreData&
GameCoreData::getScoreData()
{
	return ms_coreScoreData;
}
//---------------------------------------
void
GameCoreData::incrementScore(float scoreMultiplier)
{
	ms_coreScoreData.score += (int)((float)ms_coreScoreData.SCORE_INCREMENTER * scoreMultiplier);
}
//---------------------------------------
void
GameCoreData::resetScore()
{
	ms_coreScoreData.score = 0;
}
//---------------------------------------
void
GameCoreData::setGamePaused(bool setTo)
{
	ms_bGamePaused = setTo;
}
//---------------------------------------
bool
GameCoreData::isGamePaused()
{
	return ms_bGamePaused;
}
//---------------------------------------
void
GameCoreData::assignResourceDirectory(const char* dirName)
{
	for(ResourceNames::iterator iter = ms_coreResourceNames.begin(); iter != ms_coreResourceNames.end(); iter++)
	{
		(*iter).assign( dirName );
	}
}
//---------------------------------------
void
GameCoreData::assignResourceNames()
{
	ms_coreResourceNames[RESOURCE_NAME_LOGO].append( "Logo.png" );
	ms_coreResourceNames[RESOURCE_NAME_TITLE].append( "Title.png" );

	ms_coreResourceNames[RESOURCE_NAME_TILE].append( "gameTiles.png" );
	ms_coreResourceNames[RESOURCE_NAME_CHARACTER].append( "Character.png" );
	ms_coreResourceNames[RESOURCE_NAME_GHOST].append( "Ghost.png" );
	ms_coreResourceNames[RESOURCE_NAME_COIN].append( "Coin.png" );
	ms_coreResourceNames[RESOURCE_NAME_TIMER_PULSE].append( "TimerPulse.png" );

	ms_coreResourceNames[RESOURCE_NAME_MENU_BUTTON_STAND].append( "MenuButtonStand.png" );
	ms_coreResourceNames[RESOURCE_NAME_MENU_BUTTON_SELECT].append( "MenuButtonSelect.png" );

	ms_coreResourceNames[RESOURCE_NAME_MENU_SELECTOR_LEFT].append( "MenuSelectorBarLeft.png" );
	ms_coreResourceNames[RESOURCE_NAME_MENU_SELECTOR_CENTER].append( "MenuSelectorBarCenter.png" );
	ms_coreResourceNames[RESOURCE_NAME_MENU_SELECTOR_RIGHT].append( "MenuSelectorBarRight.png" );

	ms_coreResourceNames[RESOURCE_NAME_MENU_OPTIONS_STAND].append( "MenuOptionsStand.png" );
	ms_coreResourceNames[RESOURCE_NAME_MENU_OPTIONS_SELECT].append( "MenuOptionsSelect.png" );

	ms_coreResourceNames[RESOURCE_NAME_GO_SEQ_FINAL].append( "GoNumFinal.png" );
	ms_coreResourceNames[RESOURCE_NAME_GO_SEQ_3].append( "GoNum3.png" );
	ms_coreResourceNames[RESOURCE_NAME_GO_SEQ_2].append( "GoNum2.png" );
	ms_coreResourceNames[RESOURCE_NAME_GO_SEQ_1].append( "GoNum1.png" );
}
//---------------------------------------
cocos2d::CCSize
GameCoreData::generateDesignResolution(CCSize physicalScreenRes)
{
	//All aspect ratios creation must reside ABOVE '640', but must also be as close to 640 as possible! 

	//Generate an aspect ratio
	float aspectRatio = 0.0f;

	if( physicalScreenRes.width > physicalScreenRes.height )
	{
		aspectRatio = physicalScreenRes.height / physicalScreenRes.width;
	}
	else
	{
		aspectRatio = physicalScreenRes.width / physicalScreenRes.height;
	}

	//3:2
	if( aspectRatio == 2.0f / 3.0f )
	{
		return CCSize( 640.0f, 960.0f );
	}
	//4:3
	else if( aspectRatio == 3.0f / 4.0f )
	{
		return CCSize( 648.0f, 864.0f );
	}
	//5:3
	else if( aspectRatio == 3.0f / 5.0f )
	{
		return CCSize( 648.0f, 1080.0f );
	}
	//5:4
	else if( aspectRatio == 4.0f / 5.0f )
	{
		return CCSize( 640.0f, 1024.0f );
	}
	//8:5 (or 16:10)
	else if( aspectRatio == 5.0f / 8.0f )
	{
		return CCSize( 640.0f, 1024.0f );
	}
	//16:9
	else if( aspectRatio == 9.0f / 16.0f )
	{
		return CCSize( 648.0f, 1152.0f );
	}
	
	CCLog( "WARNING! Aspect Ratio Was Not Applied! See 'Game Core Data' 'generateDesignResoluton" );

	//Return anything (better than nothing)
	return CCSize( 640.0f, 960.0f );
}
//---------------------------------------