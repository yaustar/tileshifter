#include "AppDelegate.h"

#include "SimpleAudioEngine.h"
#include "SceneManager.h"

//App Init Includes --
#include "RandomNumberGenerator.h"
#include "FastLabelTTFManager.h"
#include "GameCoreData.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
	CC_SAFE_RELEASE_NULL( m_pFastLabelTTFManager );
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    CCDirector* pDirector = CCDirector::sharedDirector();
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();

    pDirector->setOpenGLView(pEGLView);
	
    // turn on display FPS
    pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

	//Init App Systems -----
	//Init Random Num. Gen.
	RandomNumberGenerator::initSeed();

	//Create Singletons -------------------------------------------
	m_pFastLabelTTFManager = FastLabelTTFManager::create();
	CC_SAFE_RETAIN( m_pFastLabelTTFManager );

	//Generate Game Core Data
	GameCoreData::init();

    // create a scene. it's an autorelease object
   // CCScene *pScene = HelloWorld::scene();
	CCScene *pScene = SceneManager::initScene();

    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	CCDirector::sharedDirector()->stopAnimation();

	// if you use SimpleAudioEngine, it must be pause
	CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
