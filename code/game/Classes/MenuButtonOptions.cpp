#include "MenuButtonOptions.h"

#include "GameCoreData.h"

//---------------------------------------
MenuButtonOptions::MenuButtonOptions(cocos2d::CCCallFunc* pPressCallback) :
	MenuButtonStandard	( pPressCallback )
{
}
//---------------------------------------
std::string
MenuButtonOptions::getStandardSpriteName()
{
	return GameCoreData::getResourcesName(RESOURCE_NAME_MENU_OPTIONS_STAND);
}
//---------------------------------------
std::string
MenuButtonOptions::getSelectedSpriteName()
{
	return GameCoreData::getResourcesName(RESOURCE_NAME_MENU_OPTIONS_SELECT);
}
//---------------------------------------
//---------------------------------------
//---------------------------------------