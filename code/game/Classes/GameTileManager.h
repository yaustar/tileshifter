#ifndef GAME_TILE_MANAGER_H
#define GAME_TILE_MANAGER_H

#include "cocos2d.h"

class BaseGameTile;
class GameTileManager : public cocos2d::CCLayer
{
public:

	//Constructor
	GameTileManager();

	//Implement the Layers 'create' function 
	CREATE_FUNC(GameTileManager); 

	virtual bool init(); //Overloaded Init.


	//Public Methods --------------------
	//Requests that the manager decide which tile will contain the 'character'.
	const BaseGameTile* selectCharacterTile();
	const BaseGameTile* selectGhostTile();

	//Get the first tile from the chain of neighbours (Restricted return)
	void getFirstTile(const BaseGameTile* &OutFirstTile) const;

	void pauseGameCallback(CCObject* pObject);

private:

	//Private Methods ---------------
	//Called (via a schduled callback) to activate on the following frame 
	//(allows the touch event to be enabled AFTER the chaarcters touch event,
	//giving the tiles priority of touch event BEFORE the character object
	void delayTileTouchEventActivationCallBack(float delta);

	//Gets the first tile (starting from the passed 'first tile member') (most left and most bottom, in the tile neighbour chains) - CONST
	void getFirstGameTileAccessPointer(const BaseGameTile* &pOutFirstTile) const;

	//Gets the first tile (starting from the passed 'first tile member') (most left and most bottom, in the tile neighbour chains)
	void getFirstGameTileAccessPointer(BaseGameTile* &pOutFirstTile);

	//Updates the 'First Game' member variable to point to the first tile (most left and most bottom, in the tile neighbour chains)
	void updateFirstGameTileAccessPointer();

	//Private Variables ---------------
	BaseGameTile* m_pFirstGameTile;
};
#endif //GAME_TILE_MANAGER_H