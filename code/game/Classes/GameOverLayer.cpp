#include "GameOverLayer.h"

#include "SceneManager.h"
#include "FastLabelTTF.h"

#include "HighscoreManager.h"
#include "GameCoreData.h"
#include <sstream>

#include "MenuButtonStandard.h"

const int TAG_TEXT_FIELD = 98343; //Random value
const int TAG_WARNING_MESS = TAG_TEXT_FIELD+1;

USING_NS_CC; 

//----------------------------------------------------
GameOverLayer::GameOverLayer() :
	m_pBgSprite		( NULL )
{
	
}
//----------------------------------------------------
GameOverLayer::~GameOverLayer()
{

}
//----------------------------------------------------
bool
GameOverLayer::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	//Enable Touch Events On This Layer!
	this->setTouchEnabled( true );

	return true;
}
//----------------------------------------------------
void
GameOverLayer::onExit()
{
	this->setTouchEnabled( false );

	this->unscheduleAllSelectors();

	//Bug Fix - buttons remain in next scene! - Hiding bigger bug? Is this scene being destroyed!!! ??
	this->removeAllChildren();
}
//---------------------------------------
void
GameOverLayer::ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent)
{
	CCTextFieldTTF* pTextField = static_cast<CCTextFieldTTF*>( this->getChildByTag(TAG_TEXT_FIELD) );
	
	//Touch Began may call before text field is initialised
	if( pTextField )
	{
		CCTouch* pCurrTouch	( static_cast<CCTouch*>(pTouches->anyObject()) );

		if( pTextField->boundingBox().containsPoint(pCurrTouch->getLocation()) )
		{
			removeWarningMessage();
			pTextField->attachWithIME();
		}
	}
}
//---------------------------------------
bool
GameOverLayer::onTextFieldInsertText(CCTextFieldTTF* pSender, const char* text, int nLen)
{
	CCTextFieldTTF* pTextField = static_cast<CCTextFieldTTF*>( this->getChildByTag(TAG_TEXT_FIELD) );
	
	//Remove any warnings (the keyboard is active, so the user must be informed by now!)
	removeWarningMessage();

	//Prevent character retry if 3 or more charcters already exist
	if( pTextField->getCharCount() >= 3 )
	{
		if( strcmp(text, "\n") == 0 )
		{
			//Allow, shouldn't affect it! (Removes the keyboard on 'enter' being pressed!)
			return false;
		}
		else
		{
			//Prevent character input (reached the max)!
			return true;
		}
	}

	//else if( pTextField->getCharCount() == 0 ) //Original only applied to first character (now applies to all)
	//{
		char charCache = text[0];

		//Force capitalised letter!
		if( charCache >= 'a' && charCache <= 'z' )
		{
			//Force data pointed to change
			const_cast<char*>(text)[0] = charCache - 32;
		}

	//}
	
	return false;
}
//---------------------------------------------
void
GameOverLayer::activateLayer(cocos2d::CCSprite* pBgSprite)
{
	const GLubyte tintAmount = 80;
	const float fadeTimer = 0.4f;

	//Check for existing background
	if( m_pBgSprite )
	{
		removeChild( m_pBgSprite );
	}

	const CCSize &screenSize = CCDirector::sharedDirector()->getVisibleSize();

	//Assign new background, set it to a central position
	m_pBgSprite = pBgSprite;
	m_pBgSprite->setPosition( screenSize*0.5f );

	//Add child, tint out over time 
	addChild( m_pBgSprite );
	m_pBgSprite->runAction( CCTintTo::create(fadeTimer, tintAmount, tintAmount, tintAmount) );


	//Display the score
	FastLabelTTF* pScoreText = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_100, screenSize, kCCTextAlignmentCenter, kCCVerticalTextAlignmentTop );

	//Set text properties
	pScoreText->setColor( ccc3(255, 255, 255) );
	pScoreText->setPosition( CCSize(screenSize.width*0.5f, screenSize.height*0.25f) );
	
	std::stringstream ss;
	ss << GameCoreData::getScoreData().score;
	pScoreText->setString( ss.str().c_str() );


	//Produce 'Game Over' text
	FastLabelTTF* pGameOverTitleText = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_60, screenSize, kCCTextAlignmentCenter, kCCVerticalTextAlignmentTop );
	
	//Set text properties
	pGameOverTitleText->setColor( ccc3(255, 255, 255) );
	pGameOverTitleText->setPosition( CCSize(screenSize.width*0.5f, screenSize.height*0.35f) );

	if( HighscoreManager::sharedHighscoreManager()->isScoreHigh(GameCoreData::getScoreData().score) )
	{
		pGameOverTitleText->setString( "New High Score!" );

		CCTextFieldTTF* pTextField = CCTextFieldTTF::textFieldWithPlaceHolder("Enter Initials", CCSize(480,100), kCCTextAlignmentCenter, "Arial", 50.0f);
		pTextField->setPosition( ccp(screenSize.width*0.5f, screenSize.height*0.5f) );
		pTextField->setTag( TAG_TEXT_FIELD );
		pTextField->runAction( CCFadeIn::create(fadeTimer) );
		this->addChild(pTextField,4);

		pTextField->attachWithIME();
		pTextField->setDelegate( this ); //Inherited callbacks

		//Enter name button
		MenuButtonStandard* pEnterNameButton = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(GameOverLayer::moveToHighscoresCallback) ));
		pEnterNameButton->setPosition( ccp(screenSize.width * 0.5f, screenSize.height * 0.2f) );
		pEnterNameButton->setString("Submit!");
		this->addChild( pEnterNameButton );
	}
	else
	{
		pGameOverTitleText->setString( "Score" );

		//Add buttons
		MenuButtonStandard* pRetryButton = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(GameOverLayer::retryCallback) ));
		pRetryButton->setPosition( ccp(screenSize.width * 0.5f, screenSize.height * 0.4f) );
		pRetryButton->setString("Retry");
		//pRetryButton->runAction( CCFadeIn::create(fadeTimer) ); //No Opacity Available!
		this->addChild( pRetryButton );

		MenuButtonStandard* pMainMenuButton = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(GameOverLayer::mainMenuCallback) ));
		pMainMenuButton->setPosition( pRetryButton->getPosition() - ccp(0.0f, pMainMenuButton->getSize().height + screenSize.height*0.03f) );
		pMainMenuButton->setString("Main Menu");
		//pMainMenuButton->runAction( CCFadeIn::create(fadeTimer) ); //No Opacity Available!
		this->addChild( pMainMenuButton );
	}

	//Fade into scene!
	pGameOverTitleText->runAction( CCFadeIn::create(fadeTimer) );
	pScoreText->runAction( CCFadeIn::create(fadeTimer) );

	addChild( pGameOverTitleText );
	addChild( pScoreText );
}
//----------------------------------------------------
bool
GameOverLayer::removeWarningMessage()
{
	FastLabelTTF* pWarningMess = static_cast<FastLabelTTF*>( getChildByTag(TAG_WARNING_MESS) );
	
	if( pWarningMess )
	{
		removeChild( pWarningMess );
		return true;
	}

	return false;
}
//----------------------------------------------------
void
GameOverLayer::moveToHighscoresCallback()
{
	CCTextFieldTTF* pTextField = static_cast<CCTextFieldTTF*>( this->getChildByTag(TAG_TEXT_FIELD) );
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
    // IME doesn't work on Mac or Windows so populate the textfield with something
    pTextField->setString("G4H");
#endif

	if( pTextField->getCharCount() >= 3 )
	{
		HighscoreManager::sharedHighscoreManager()->assignHighScore( pTextField->getString(), GameCoreData::getScoreData().score );

		SceneManager::swapScene( SCENE_HIGHSCORES );
	}
	else
	{
		//Not enough characters!
		FastLabelTTF* pWarningMess = static_cast<FastLabelTTF*>( getChildByTag(TAG_WARNING_MESS) );
		
		if( !pWarningMess )
		{
			pWarningMess = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_45, CCSize(0,0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter );
            pWarningMess->setPosition( pTextField->getPosition() - ccp(0.0f, CCDirector::sharedDirector()->getVisibleSize().height*0.15f) );
            pWarningMess->setColor( ccc3(255,0,0) );
            pWarningMess->setTag( TAG_WARNING_MESS );
            pWarningMess->setString( "Not enough characters!" );
            
            addChild( pWarningMess );
        }
	}
}
//----------------------------------------------------
void
GameOverLayer::retryCallback()
{
	SceneManager::swapScene( SCENE_GAME );
}
//----------------------------------------------------
void
GameOverLayer::mainMenuCallback()
{
	SceneManager::swapScene( SCENE_MAIN_MENU );
}
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------
//----------------------------------------------------