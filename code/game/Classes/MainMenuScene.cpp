#include "MainMenuScene.h"

#include "SceneManager.h"
#include "HighscoreManager.h"
#include "GameCoreData.h"

#include "FastLabelTTFTypes.h"
#include "FastLabelTTF.h"

#include "MenuButtonStandard.h"
#include "MenuButtonOptions.h"

USING_NS_CC;

//---------------------------------------
CCScene*
MainMenuScene::createScene()
{
    CCScene *scene = CCScene::create();
    MainMenuScene *layer = MainMenuScene::create();

    scene->addChild(layer);
    return scene;
}
//---------------------------------------
bool
MainMenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
	//Allow touch control
	this->setTouchEnabled(true);

	//Cache the screen size to aid button placement
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();


	//Add "HelloWorld" splash screen"
    CCSprite* pSprite = CCSprite::create("MainMenuScene.png");
    pSprite->setPosition( ccp(visibleSize.width*0.5f, visibleSize.height*0.5f) );
    this->addChild(pSprite, 0);

	//Supply a highscore
	FastLabelTTF* pHighscoreText = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_60, CCSizeZero, kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter );
	pHighscoreText->setPosition( ccp(visibleSize.width*0.5f, visibleSize.height*0.9f) );

	std::string prevHighscore( "Highscore  " );
	prevHighscore.append( HighscoreManager::sharedHighscoreManager()->getHighestScore() );
	pHighscoreText->setString( prevHighscore.c_str() );
	
	addChild( pHighscoreText );

	//Supply a title image
	CCSprite* pLogoSprite = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_TITLE).c_str() );
	pLogoSprite->setPosition( ccp(visibleSize.width * 0.5f, visibleSize.height * 0.7f) );
	this->addChild( pLogoSprite );


	//Generate menu buttons
	MenuButtonStandard* pPlayModeMenu = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(MainMenuScene::menuPlayModeCallback) ));
	pPlayModeMenu->setPosition( ccp(visibleSize.width * 0.5f, visibleSize.height * 0.45f) );
	pPlayModeMenu->setString("Play Mode!");
	this->addChild( pPlayModeMenu );

	MenuButtonStandard* pQuitGameMenu = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(MainMenuScene::menuQuitCallback) ));
	pQuitGameMenu->setPosition( pPlayModeMenu->getPosition() - ccp(0.0f, pQuitGameMenu->getSize().height + visibleSize.height*0.03f) );
	pQuitGameMenu->setString("Quit!");
	this->addChild( pQuitGameMenu );

	MenuButtonOptions* pOptionsMenu = MenuButtonOptions::create( CCCallFunc::create(this, callfunc_selector( MainMenuScene::menuOptionsCallback )) );
	pOptionsMenu->setPosition( ccp(visibleSize.width - (pOptionsMenu->getSize().width*0.5f), pOptionsMenu->getSize().height*0.5f) );
	this->addChild( pOptionsMenu );
	

    return true;
}
//---------------------------------------
void
MainMenuScene::onExit()
{
	this->setTouchEnabled(false);

	//Bug Fix - buttons remain in next scene! - Hiding bigger bug? Is this scene being destroyed!!! ??
	this->removeAllChildren();
}
//---------------------------------------
void
MainMenuScene::menuPlayModeCallback()
{
	SceneManager::swapScene( SCENE_GAME );
}
//---------------------------------------
void
MainMenuScene::menuQuitCallback()
{
	SceneManager::swapScene( SCENE_EXIT );
}
//---------------------------------------
void
MainMenuScene::menuOptionsCallback()
{
	SceneManager::swapScene( SCENE_OPTIONS );
}