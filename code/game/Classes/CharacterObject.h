#ifndef CHARACTER_OBJECT_H
#define CHARACTER_OBJECT_H

#include "cocos2d.h"

#include "TileType.h"

//Forward Includes ------
class BaseGameTile;
//----------------------
class CharacterObject : public cocos2d::CCLayer
{
public:
	CharacterObject(const BaseGameTile* pGameTile, bool bIsGhost);

	static CharacterObject* create(const BaseGameTile* pGameTile, bool bIsGhost);
	virtual bool init(); //Overloaded Init.

	//Override Touch Methods ----------------
    virtual void ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Moves
	virtual void ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Ends

	//Implement the Layers 'create' function 
	//CREATE_FUNC(CharacterObject); 

	//void initGameTile(const BaseGameTile* pGameTile);

	void pauseGameCallback(CCObject* pObject);

	//Accessors ------------------------------------
	const BaseGameTile* getActiveCharacterTile() const; 
	cocos2d::CCRect getCharacterBound() const;

	void onCollisionWithCoin();
	bool testForGhostCollision(CharacterObject* pGhost);

private:
	void moveFromCentre();
	void moveFromEdge();

	void updateTilePrevPosition();
	void applyMoveToCentre(const cocos2d::CCPoint &pos, bool bTurnAround);
	

	cocos2d::CCPoint m_tilesPrevPos;

	const BaseGameTile* m_pGameTile;
	cocos2d::CCSprite* m_pSprite;

	TileDirection m_currDir;

	float m_objectSpeed;
	bool m_bLockTouchEvents;
};
#endif //CHARACTER_OBJECT_H