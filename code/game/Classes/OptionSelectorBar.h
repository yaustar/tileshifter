#ifndef OPTION_SELECTOR_BAR_H
#define OPTION_SELECTOR_BAR_H

#include "cocos2d.h"
#include "ExtraDeclarations.h"

class OptionSelectorBar : public cocos2d::CCLayer
{
public:
	OptionSelectorBar(cocos2d::CCCallFunc* pSwitchedOn, cocos2d::CCCallFunc* pSwitchedOff);

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
	virtual void onExit();
    
    // implement the "static node()" method manually
    CREATE_FUNC_TWO_ARGU(OptionSelectorBar, cocos2d::CCCallFunc*, pSwitchedOn, cocos2d::CCCallFunc*, pSwitchedOff );

	void setSwitch(bool bSwitchIsOn);

private:

	//Private Methods ---
	void leftSelectorPressedCallback(CCObject* pSender);
	void rightSelectorPressedCallback(CCObject* pSender);

	//Private Variables ----
	cocos2d::CCCallFunc* m_pSwitchedOnCallback;
	cocos2d::CCCallFunc* m_pSwitchedOffCallback;
};
#endif //OPTION_SELECTOR_BAR_H