#include "CharacterObject.h"
//#include "LayerGameTiles.h"
#include "BaseGameTile.h"

#include "GameCoreData.h"
#include "RandomNumberGenerator.h"

#include "EventListener.h"
#include "EventPauseGame.h"
#include "EventGameOver.h"

#include "GameObjectTags.h"

//Use the 'cocos2d' namespace (only do this within the .cpp!)
USING_NS_CC; 

//const float OBJECT_SPEED			(0.5f);
const float OBJECT_SPEED_INCREASE	(0.9f); //Smaller = faster!

//----------------------------------------------------
CharacterObject::CharacterObject(const BaseGameTile* pGameTile, bool bIsGhost) :
	m_tilesPrevPos			( pGameTile->getPosition() ),
	m_pGameTile				( pGameTile ),
	m_pSprite				( NULL ),
	m_currDir				( pGameTile->getRandomTileDirection() ),
	m_objectSpeed			( 0.5f ),
	m_bLockTouchEvents		( false )
{
	if( bIsGhost )
	{
		setTag( GAME_OBJ_TAG_GHOST );
	}
	else
	{
		setTag( GAME_OBJ_TAG_CHARACTER );
	}
}
//----------------------------------------------------
CharacterObject*
CharacterObject::create(const BaseGameTile* pGameTiles, bool bIsGhost)
{
	//Copied from 'CREATE_FUNC', modified to cater for additional parameters.
	 CharacterObject *pRet = new CharacterObject(pGameTiles, bIsGhost); 
    if (pRet && pRet->init()) 
    { 
        pRet->autorelease(); 
        return pRet; 
    } 
    else 
    { 
        delete pRet; 
        pRet = NULL; 
        return NULL; 
    }
}
//----------------------------------------------------
bool
CharacterObject::init()
{
	//--------------------------------------------------------
    // Super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

	//Listen for game pause event
	EventListener* pPauseGameListener = EventListener::create( this, callfuncO_selector(CharacterObject::pauseGameCallback), TS_EVENT_ID_PAUSE_GAME );
	this->addChild( pPauseGameListener );

	//Enable Touch Events On This Layer!
	this->setTouchEnabled( true );

	////Create a sprite to represent the character
	if( this->getTag() == GAME_OBJ_TAG_CHARACTER )
	{
		m_pSprite = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_CHARACTER).c_str() );
	}
	else
	{
		//Must be the ghost!
		m_pSprite = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_GHOST).c_str() );
	}

	m_pSprite->setPosition( m_tilesPrevPos );
	this->addChild(m_pSprite);

	//We start at a centre position, activate call back chains here. 
	moveFromCentre();

	return true;
}
//----------------------------------------------------
void
CharacterObject::ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent)
{
	if( !m_bLockTouchEvents )
	{
		m_pSprite->setPosition( m_pSprite->getPosition() + (m_pGameTile->getPosition() - m_tilesPrevPos) );
		updateTilePrevPosition();
	}
}
//----------------------------------------------------
void
CharacterObject::ccTouchesEnded(CCSet* pTouches, CCEvent* pEvent)
{
	if( !m_bLockTouchEvents )
	{
		//Force move to incase the tile in question has been dramatically moved (happens along the edges)
		m_pSprite->setPosition( m_pSprite->getPosition() + (m_pGameTile->getPosition() - m_tilesPrevPos) );
		updateTilePrevPosition();

		m_bLockTouchEvents = true;

		CCMoveTo* pMoveTo = CCMoveTo::create( m_pGameTile->getTileSlideSpeed(), m_pSprite->getPosition() + (m_pGameTile->getEndSlidePosition() - m_tilesPrevPos) );
		CCCallFunc* pUpdatePrevTilePosCallBack = CCCallFunc::create( this, callfunc_selector(CharacterObject::updateTilePrevPosition) );

		//Push into sequence
		CCSequence* pSequence = CCSequence::createWithTwoActions(pMoveTo, pUpdatePrevTilePosCallBack); //Once move complete call 'Centre' call back
		m_pSprite->runAction( pSequence );
	}
}
//----------------------------------------------------
void
CharacterObject::pauseGameCallback(CCObject* pObject)
{
	EventPauseGame* pPauseGameEvent = static_cast<EventPauseGame*>( pObject );

	if( pPauseGameEvent->activatePause() )
	{
		this->pauseSchedulerAndActions();
		m_pSprite->pauseSchedulerAndActions();
	}
	else
	{
		this->resumeSchedulerAndActions();
		m_pSprite->resumeSchedulerAndActions();
	}
}
//----------------------------------------------------
const BaseGameTile*
CharacterObject::getActiveCharacterTile() const
{
	return m_pGameTile;
}
//----------------------------------------------------
cocos2d::CCRect
CharacterObject::getCharacterBound() const
{
	return m_pSprite->boundingBox();
}
//----------------------------------------------------
void
CharacterObject::onCollisionWithCoin()
{
	//Speed utilises a 'duration' not an actual velocity, smaller values are faster!
	m_objectSpeed *= OBJECT_SPEED_INCREASE;
}
//----------------------------------------------------
bool
CharacterObject::testForGhostCollision(CharacterObject* pGhost)
{
	//Calaculate circle collision!
	CCPoint charPoint( m_pSprite->getPosition() );
	CCPoint ghostPoint( pGhost->m_pSprite->getPosition() );

	float dist = charPoint.getDistance( ghostPoint );

	if( dist <= (m_pSprite->boundingBox().size.width*0.5f) + (pGhost->m_pSprite->boundingBox().size.width*0.5f) )
	{
		//Collision has occured
		EventGameOver::create();
		return true;
	}

	return false;
}
//----------------------------------------------------
//Private Methods
//----------------------------------------------------
void
CharacterObject::moveFromCentre() //i.e. move to edge
{	
	CCPoint edgeOffset = m_pGameTile->getEdgeOffset(m_currDir);
	CCMoveBy* pMoveBy = CCMoveBy::create(m_objectSpeed, edgeOffset);

	//Create call back to the 'move from edge', the call back is called on completion of the above movement
	CCCallFunc* pMoveFromEdgeCallBack = CCCallFunc::create(this, callfunc_selector(CharacterObject::moveFromEdge));
	
	//Push above into their respective sequence together
	CCSequence* pSequence = CCSequence::createWithTwoActions(pMoveBy, pMoveFromEdgeCallBack);
	m_pSprite->runAction( pSequence ); //Run the above sequence on the SPRITE! (not this object! As we are 'moving' the sprite!)
}
//----------------------------------------------------
void
CharacterObject::moveFromEdge() //i.e. move to centre
{
	{//SCOPED
		CCPoint nextTilePos;
		CCPoint instantJumpSpriteOffset;

		const BaseGameTile* pNextGameTile = NULL;

		//returns false if the next tile has NO valid path onit from the current position
		if( m_pGameTile->getNextCentrePosition(m_currDir, pNextGameTile, nextTilePos, instantJumpSpriteOffset) ) //(If paths match correctly)
		{
			//If paths are BOTH active (they are both moving along the same plane! So traversal can CONTINUE!)
			//OR Neigher are on the active path (both paths are stationary so can also move into eachother).
			if( (m_pGameTile->isTileMoving() && pNextGameTile->isTileMoving()) || 
				(!m_pGameTile->isTileMoving() && !pNextGameTile->isTileMoving()) )
			{
				//If the tile to be accessed is at the opposite end of the board (i.e. the character is at the bottom and needs to JUMP! to the top)
				CCPoint tilePosDiff = m_pGameTile->getConstPos() - pNextGameTile->getConstPos();
				if( abs((int)tilePosDiff.x) > GameCoreData::getTileData().tileSize.width ||
					abs((int)tilePosDiff.y) > GameCoreData::getTileData().tileSize.height )
				{
					//System needs to jump the character to other side of board!
					m_pSprite->setPosition( pNextGameTile->getConstPos() + instantJumpSpriteOffset );
				}

				//Set member game tile to be the next tile!
				m_pGameTile = pNextGameTile;

				//We have moved into a new tile, update the prev pos, to the current tiles current position)
				m_tilesPrevPos = m_pGameTile->getPosition();

				//Move to centre of new tile
				applyMoveToCentre( nextTilePos, false );

				return; //Skips 're-activating the below scheduler!
			}
			else
			{
				//path cannot be completed! Turn around!
				applyMoveToCentre( m_pGameTile->getPosition(), true );
			}
		}
		else 
		{
			//No valid path available, turn around!
			applyMoveToCentre( m_pGameTile->getPosition(), true );
		}
	}
}
//----------------------------------------------------
void
CharacterObject::updateTilePrevPosition()
{
	m_bLockTouchEvents = false;
	m_tilesPrevPos = m_pGameTile->getPosition();
}
//----------------------------------------------------
void
CharacterObject::applyMoveToCentre(const cocos2d::CCPoint &pos, bool bTurnAround)
{
	//Move to centre of new tile
	CCMoveTo* pMoveTo = CCMoveTo::create( m_objectSpeed, pos );

	//Produce call back to the 'move from centre', ready to be called after we have moved into the centre position
	CCCallFunc* pMoveFromCentreCallBack = CCCallFunc::create(this, callfunc_selector(CharacterObject::moveFromCentre));

	//Apply above into their respective sequence
	CCSequence* pSequence = CCSequence::createWithTwoActions(pMoveTo, pMoveFromCentreCallBack); //Once move complete call 'Centre' call back
	m_pSprite->runAction( pSequence ); //Run sequence onto the SPRITE! (Not this object!)

	if( bTurnAround )
	{
		m_currDir = TileInfo::g_invertTileDirection[m_currDir];
	}
}