#ifndef COIN_OBJECT_H
#define COIN_OBJECT_H

#include "cocos2d.h"

//Forward includes
class CharacterObject;
class BaseGameTile;
class FastLabelTTF;
class CoinObject : public cocos2d::CCLayer
{
public:
	CoinObject(const BaseGameTile* pCharacterTile);

	static CoinObject* create(const BaseGameTile* pCharacterTile);
	virtual bool init(); //Overloaded Init.
	virtual void onExit();

	//Override Touch Methods ----------------
    virtual void ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent); // Touch Moves
	virtual void ccTouchesEnded(cocos2d::CCSet* pTouches, cocos2d::CCEvent* pEvent);

	//Public Methods
	bool testForCollisionWithCharacter(const CharacterObject* pCharacter, bool bIsPlayer);
	void pauseGameCallback(CCObject* pObject);
    void updateLoop(float dt);
    float getNormalisedScale() const;

private:

	//Private Methods -----------------------------
	cocos2d::CCPoint calculateCoinPosWithSanityCheck();
	void findNewCoinTile(const cocos2d::CCPoint &offset);
    void resetCoinToNewTile();

	//Move coin along with it's joining tile
	void matchCoinToActiveTile();

	void updateScoreOuput();

	//Private Variables ---------------------------
	const BaseGameTile* m_pCoinTile; //Pointer to the tile the coin is 'attached' to

	cocos2d::CCSprite* m_pSprite;
	FastLabelTTF* m_pScoreLabel;

	bool m_bLockTouchEvents;
    
    float m_normalisedScale;
};
#endif //COIN_OBJECT_H