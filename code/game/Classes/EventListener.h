#ifndef EVENT_LISTENER_H
#define EVENT_LISTENER_H

#include "cocos2d.h"

#include "EventIdList.h"
#include "EventListenerDeclarations.h"

class EventListener : public cocos2d::CCLayer
{
public:
	EventListener(CCObject* pCallBackObject, cocos2d::SEL_CallFuncO callBackMethod, TileshifterEventId eventID);
	~EventListener();

	static EventListener* create(CCObject* pCallBackObject, cocos2d::SEL_CallFuncO callBackMethod, TileshifterEventId eventID);
	virtual bool init(); //Overloaded Init.

	//Runs the callbacks
	void runListenerCallback(CCObject* pEventObject);

private:

	//Private Callback Variables -----
	cocos2d::SEL_CallFuncO m_callBackMethod;
	CCObject* m_pCallBackObject;

	//Private Variables -----
	EventListenerLinkedNode m_eventListNode;
	TileshifterEventId m_eventId;
};
#endif //EVENT_LISTENER_H