#include "FastLabelTTFFactory.h"


//------------------------------------------
void
FastLabelTTFFactory::GetFontInfo(std::string &outFontName, float &outFontSize, FastLabelTTFTypes fontType )
{
	switch(fontType)
	{
	case FAST_LABEL_TTF_ARIAL_24:
		outFontName.assign("Arial");
		outFontSize = 24.0f;
		break;

	case FAST_LABEL_TTF_ARIAL_30:
		outFontName.assign("Arial");
		outFontSize = 30.0f;
		break;

	case FAST_LABEL_TTF_MARKER_FELT_30:
		outFontName.assign("fonts/Felt.ttf");
		outFontSize = 30.0f;
		break;

	case FAST_LABEL_TTF_MARKER_FELT_45:
		outFontName.assign("fonts/Felt.ttf");
		outFontSize = 45.0f;
		break;

	case FAST_LABEL_TTF_MARKER_FELT_60:
		outFontName.assign("fonts/Felt.ttf");
		outFontSize = 60.0f;
		break;

	case FAST_LABEL_TTF_MARKER_FELT_100:
		outFontName.assign("fonts/Felt.ttf");
		outFontSize = 100.0f;
		break;
    default: break;
	}
}
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------