#include "OptionSelectorBar.h"

#include "SceneManager.h"
#include "GameCoreData.h"

#include "FastLabelTTFTypes.h"
#include "FastLabelTTF.h"

const int BLIP_TAG = 83234; //Random value
const int LEFT_SELECTOR_TAG = BLIP_TAG+1;
const int RIGHT_SELECTOR_TAG = LEFT_SELECTOR_TAG+1;
const int SELECTOR_MENU_TAG = RIGHT_SELECTOR_TAG+1;

USING_NS_CC;

//---------------------------------------
OptionSelectorBar::OptionSelectorBar(cocos2d::CCCallFunc* pSwitchedOn, cocos2d::CCCallFunc* pSwitchedOff) :
	m_pSwitchedOnCallback	( pSwitchedOn ),
	m_pSwitchedOffCallback	( pSwitchedOff )
{
	m_pSwitchedOnCallback->retain();
	m_pSwitchedOffCallback->retain();
}
//---------------------------------------
bool
OptionSelectorBar::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
	//Allow touch control
	this->setTouchEnabled(true);

	//Generate menu buttons
	CCMenuItemImage* pSelectorBarLeft = CCMenuItemImage::create(	GameCoreData::getResourcesName(RESOURCE_NAME_MENU_SELECTOR_LEFT).c_str(),
																	GameCoreData::getResourcesName(RESOURCE_NAME_MENU_SELECTOR_LEFT).c_str(),
																	this,
																	menu_selector(OptionSelectorBar::leftSelectorPressedCallback)
																);

	//Generate menu buttons
	CCMenuItemImage* pSelectorBarRight = CCMenuItemImage::create(	GameCoreData::getResourcesName(RESOURCE_NAME_MENU_SELECTOR_RIGHT).c_str(),
																	GameCoreData::getResourcesName(RESOURCE_NAME_MENU_SELECTOR_RIGHT).c_str(),
																	this,
																	menu_selector(OptionSelectorBar::rightSelectorPressedCallback)
																);


	CCSprite* pSelectorCenter = CCSprite::create( GameCoreData::getResourcesName(RESOURCE_NAME_MENU_SELECTOR_CENTER).c_str() );

	//Assign menu button position
	pSelectorCenter->setPosition( CCPointZero );
	pSelectorBarLeft->setPosition( pSelectorCenter->getPosition() - ccp(pSelectorCenter->getContentSize().width*0.5f, 0.0f) );
	pSelectorBarRight->setPosition( pSelectorCenter->getPosition() + ccp(pSelectorCenter->getContentSize().width*0.5f, 0.0f) );
	pSelectorBarLeft->setTag( LEFT_SELECTOR_TAG );
	pSelectorBarRight->setTag( RIGHT_SELECTOR_TAG );

	//Create menu, it's an autorelease object
    CCMenu* pMenu = CCMenu::create(pSelectorBarLeft, NULL);
    pMenu->setPosition(CCPointZero);
	pMenu->setTag( SELECTOR_MENU_TAG );
    this->addChild(pMenu, 0);

	CCSprite* pSelectorBlip = CCSprite::create( "128/MenuSelector.png" );
	pSelectorBlip->setTag(BLIP_TAG);
	pSelectorBlip->setPosition( pSelectorBarLeft->getPosition() );
	this->addChild( pSelectorBlip, 1 );

	//Add remaining menu items
	pMenu->addChild( pSelectorBarRight );


    return true;
}
//---------------------------------------
void
OptionSelectorBar::onExit()
{
	this->setTouchEnabled( false );

	if( m_pSwitchedOnCallback )
	{
		m_pSwitchedOnCallback->release();
	}

	if( m_pSwitchedOffCallback )
	{
		m_pSwitchedOffCallback->release();
	}
}
//---------------------------------------
void
OptionSelectorBar::setSwitch(bool bSwitchIsOn)
{
	if( bSwitchIsOn )
	{
		leftSelectorPressedCallback(NULL);
	}
	else
	{
		rightSelectorPressedCallback(NULL);
	}
}
//---------------------------------------
void
OptionSelectorBar::leftSelectorPressedCallback(CCObject* pSender)
{
	m_pSwitchedOnCallback->execute();
	
	this->getChildByTag( BLIP_TAG )->setPosition( this->getChildByTag(SELECTOR_MENU_TAG)->getChildByTag(LEFT_SELECTOR_TAG)->getPosition() );
	//static_cast<CCSprite*>( this->getChildByTag(BLIP_TAG) )->setColor( ccc3(0, 255, 0) );
}
//---------------------------------------
void
OptionSelectorBar::rightSelectorPressedCallback(CCObject* pSender)
{
	m_pSwitchedOffCallback->execute();
	
	this->getChildByTag( BLIP_TAG )->setPosition( this->getChildByTag(SELECTOR_MENU_TAG)->getChildByTag(RIGHT_SELECTOR_TAG)->getPosition() );
	//static_cast<CCSprite*>( this->getChildByTag(BLIP_TAG) )->setColor( ccc3(255, 0, 0) );
}
//---------------------------------------