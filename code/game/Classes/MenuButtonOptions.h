#ifndef MENU_BUTTON_OPTIONS_H
#define MENU_BUTTON_OPTIONS_H

#include "MenuButtonStandard.h"

class MenuButtonOptions : public MenuButtonStandard
{
public:
	MenuButtonOptions(cocos2d::CCCallFunc* pPressCallback);

	CREATE_FUNC_ONE_ARGU(MenuButtonOptions, cocos2d::CCCallFunc*, pPressCallback);

protected:
	virtual std::string getStandardSpriteName();
	virtual std::string getSelectedSpriteName();

private:

	//Private Methods ---

	//Private Variables ----
};
#endif //MENU_BUTTON_OPTIONS_H