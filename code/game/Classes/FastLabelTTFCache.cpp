#include "FastLabelTTFCache.h"

USING_NS_CC;

const unsigned int CHAR_CACHE_START_POINT = 32;
const unsigned int CHAR_CACHE_COUNT = 127 - CHAR_CACHE_START_POINT;

//Macros --------------------------
#define CHAR_LOOK_UP(charValue) charValue - CHAR_CACHE_START_POINT


//Construct -----------------------
FastLabelTTFCache::FastLabelTTFCache(const std::string &fontName, float fontSize) :
	m_charTextures	( CHAR_CACHE_COUNT, NULL )
{
	generateCharacterTextures( fontName, fontSize );
}
//------------------------------------------
FastLabelTTFCache::~FastLabelTTFCache()
{
	for(TextureArray::iterator iter = m_charTextures.begin(); iter != m_charTextures.end(); iter++)
	{
		CC_SAFE_RELEASE_NULL( (*iter) );
	}
}
//------------------------------------------
FastLabelTTFCache*
FastLabelTTFCache::create(const std::string &fontName, float fontSize)
{
	//Copied from 'CREATE_FUNC', modified to cater for additional parameters.
	FastLabelTTFCache *pRet = new FastLabelTTFCache( fontName, fontSize ); 
    if (pRet && pRet->init()) 
    { 
        pRet->autorelease(); 
        return pRet; 
    } 
    else
    { 
        delete pRet; 
        pRet = NULL; 
        return NULL; 
    }
}
//------------------------------------------
bool
FastLabelTTFCache::init()
{
	 // Super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	return true;
}
//------------------------------------------
void
FastLabelTTFCache::generateCharacterTextures(const std::string &fontName, float fontSize)
{
	//For each character where we wish to start, to where we wish to end
	for(unsigned int i = CHAR_CACHE_START_POINT; i < CHAR_CACHE_COUNT + CHAR_CACHE_START_POINT; i++)
	{
		//Produce a null terminated string of the current character
		char cacheChar[2] = { static_cast<char>(i), 0 };

		//Use cocos2d-x's TTF generator to produce the single character
		CCLabelTTF* pCharLabel = CCLabelTTF::create( cacheChar, fontName.c_str(), fontSize );

		//Collect and retain the generated texture (we retain to prevent it being unallocated by auto-release system)
		m_charTextures[CHAR_LOOK_UP(i)] = pCharLabel->getTexture();
		CC_SAFE_RETAIN( m_charTextures[CHAR_LOOK_UP(i)] );
	}
}
//------------------------------------------
cocos2d::CCTexture2D*
FastLabelTTFCache::getCharacterTexture(char requestedChar) const
{
	//Convert requested char to a valid array look up ID.
	requestedChar = CHAR_LOOK_UP(requestedChar);
	
	//Test for a request for a non-represented character
	if( requestedChar >= 0 && requestedChar < CHAR_CACHE_COUNT )
	{
		return m_charTextures[requestedChar];
	}

	return NULL;
}
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------
//------------------------------------------