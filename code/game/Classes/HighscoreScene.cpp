#include "HighscoreScene.h"

#include "SceneManager.h"
#include "HighscoreManager.h"

#include "FastLabelTTF.h"
#include "MenuButtonStandard.h"

USING_NS_CC;

//---------------------------------------
CCScene* HighscoreScene::createScene()
{
    CCScene *scene = CCScene::create();
    HighscoreScene *layer = HighscoreScene::create();

    scene->addChild(layer);
    return scene;
}
//---------------------------------------
HighscoreScene::HighscoreScene()
{
}
//---------------------------------------
HighscoreScene::~HighscoreScene()
{
}
//---------------------------------------
bool HighscoreScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }


	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

	//Provide a text output to tell us it's the highscore scene... 
	FastLabelTTF* pHighscoreTitle = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_100, visibleSize, kCCTextAlignmentCenter, kCCVerticalTextAlignmentTop );
	
	pHighscoreTitle->setString( "High Scores" );
	pHighscoreTitle->setColor( ccc3(255, 0, 0) );
	pHighscoreTitle->setPosition( ccp(visibleSize.width*0.5f, visibleSize.height*0.45f) );

	addChild( pHighscoreTitle );


	HighscoreManager* pScoreManager = HighscoreManager::sharedHighscoreManager();

	for(int i = 0; i < pScoreManager->getNumActiveScores(); i++)
	{
		FastLabelTTF* pName = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_45, visibleSize );
		pName->setString( pScoreManager->getHighScoreName(i).c_str() );
		pName->setPosition( ccp(visibleSize.width*0.25, visibleSize.height*0.8f - (pName->getSize().height*static_cast<float>(i))) );
		addChild( pName );

		FastLabelTTF* pScore = FastLabelTTF::create( FAST_LABEL_TTF_MARKER_FELT_45, visibleSize );
		pScore->setString( pScoreManager->getHighScore(i).c_str() );
		pScore->setPosition( ccp(visibleSize.width*0.75, pName->getPositionY()) );
		addChild( pScore );

		//Is this the score the player just recieved! 
		if( i == pScoreManager->getLatestHighscoreId() )
		{
			//Reset score indicator for next time!
			pScoreManager->resetLatestHighscoreId();

			//Flash this score!
			CCRepeatForever* pInfiniteFlashing = CCRepeatForever::create( CCSequence::createWithTwoActions(CCFadeOut::create(0.5f), CCFadeIn::create(0.5f)) );
			pName->runAction( pInfiniteFlashing );

			pInfiniteFlashing = CCRepeatForever::create( CCSequence::createWithTwoActions(CCFadeOut::create(0.5f), CCFadeIn::create(0.5f)) );
			pScore->runAction( pInfiniteFlashing );
		}
	}


	//Add transition buttons
	MenuButtonStandard* pMainMenuButton = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(HighscoreScene::mainMenuCallback) ));
	pMainMenuButton->setPosition( ccp(visibleSize.width*0.5f, pMainMenuButton->getSize().height*0.5f + visibleSize.height*0.01f) );
	pMainMenuButton->setString("Main Menu");
	this->addChild( pMainMenuButton );
	
	MenuButtonStandard* pRetryButton = MenuButtonStandard::create(CCCallFunc::create( this, callfunc_selector(HighscoreScene::retryCallback) ));
	pRetryButton->setPosition( ccp( pMainMenuButton->getPositionX(), pMainMenuButton->getPositionY() + pRetryButton->getSize().height + visibleSize.height*0.03f) );
	pRetryButton->setString("Retry");
	this->addChild( pRetryButton );

    return true;
}
//---------------------------------------
void HighscoreScene::onExit()
{
	//this->setTouchEnabled(false);

	//Bug Fix - buttons remain in next scene! - Hiding bigger bug? Is this scene being destroyed!!! ??
	removeAllChildren();
}

//---------------------------------------
void
HighscoreScene::retryCallback()
{
	SceneManager::swapScene( SCENE_GAME );
}	
//---------------------------------------
void
HighscoreScene::mainMenuCallback()
{
	SceneManager::swapScene( SCENE_MAIN_MENU );
}
//---------------------------------------